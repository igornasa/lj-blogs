﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;
using LJ.Models.User;

namespace LJ.ViewModels.Blog
{
    public class ModelViewPostShort
    {
        public int Id { get; set; }
        public DateTime PostedOn { get; set; }
        public int SecurityStatus { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public int UserImageId { get; set; }
        public ApplicationUserImage UserImage { get; set; }
        public int PostLikesCount { get; set; }
        public int PostDislikesCount { get; set; }
        public int CommentsCount { get; set; }
        public int Rating { get; set; }
        public int TopOrder { get; set; }
                      
    }
}