﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;

namespace LJ.ViewModels
{
    public class CommentViewModel
    {
        public CommentViewModel() { }
        public CommentViewModel(Comment comment)
        {
            Comment = comment;
        }
        public Comment Comment { get; set; }
        public int CommentDislikes { get; set; }
        public int CommentLikes { get; set; }
        public int CommentReplies { get; set; }
        public int StatusId { get; set; }
        public Status StatusValue
        {
            get { return (Status)StatusId; }
            set { StatusId = (int)value; }
        }
        public DateTime DateTime { get; set; }
        public List<CommentViewModel> ChildReplies { get; set; }
        public string Body { get; set; }
        public int Id { get; set; }
        public int UserImageId { get; set; }
        public int? ParentReplyId { get; set; }
        public string UserName { get; set; }
    }

}