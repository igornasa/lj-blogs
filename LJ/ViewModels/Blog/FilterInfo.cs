﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;
using LJ.ViewModels.Blog;
using LJ.Models.User;

namespace LJ.ViewModels.Blog
{
    public class FilterInfo
    {
        string actionName { get; set; }
        Category category { get; set; }
        Tag tag { get; set; }
        int PostsPerPage { get; set; }
    }
}