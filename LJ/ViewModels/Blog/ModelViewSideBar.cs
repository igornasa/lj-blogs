﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;
using LJ.ViewModels.User;

namespace LJ.ViewModels
{
    public class ModelViewSideBar
    {
        public IEnumerable<Category> Categories { get;  set; }
        public IEnumerable<Tag> Tags { get; set; }
        public IEnumerable<Post> LatestPosts { get; set; }
        public UserPanelInfo UserPanelInfo {get; set;}
        public int PageSize {get; set;}
        public Category Category { get; set; }
        public Tag Tag { get; set; }
       
    }
}