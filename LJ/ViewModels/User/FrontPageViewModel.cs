﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;

namespace LJ.ViewModels.User
{
    public class FrontPageViewModel
    {
        public IEnumerable<UserRating> TopUsers { get; set; }
        public IEnumerable<PostRating> TopPosts { get; set; }
    }
}