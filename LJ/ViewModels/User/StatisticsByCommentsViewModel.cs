﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;
using LJ.Models.Blog;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LJ.ViewModels.User
{
    public class StatisticsByCommentsViewModel
    {
        public IEnumerable<Comment> Comments { get; set; }
        public IEnumerable<Post> Posts { get; set; }
        public IEnumerable<PostStatisticsViewModel> PostComments { get; set; }
        public IEnumerable<PostStatisticsViewModel> ReceivedComments { get; set; }
        public IEnumerable<PostStatisticsViewModel> PostedComments { get; set; }
        public IEnumerable<PostStatisticsViewModel> PostLikes { get; set; }
        public IEnumerable<PostStatisticsViewModel> CommentLikes { get; set; }
    }
}