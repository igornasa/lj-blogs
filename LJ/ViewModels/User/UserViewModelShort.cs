﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;
using LJ.Models.Blog;
namespace LJ.ViewModels.User
{
    public class UserViewModelShort
    {
        public const int UsersPerPage = 8;
        public UserViewModelShort()
        { }

        public UserViewModelShort(ApplicationUser user)
        {
            var userInfo = user.ApplicationUserInfo;
            UserName = user.UserName;
            UserDefaultImageId = user.DefaultUserImageId;
            Password = user.password2;
            Email = user.Email;
            UserRealName = userInfo == null || userInfo.IsNotPublic ? "" : 
                           userInfo.FirstName + " " + userInfo.LastName;            
        }

        
        public string UserName { get; set; }
        public  int? UserDefaultImageId { get; set; }
        public string UserRealName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
       
    }
}