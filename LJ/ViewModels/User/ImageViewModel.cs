﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LJ.Models.Blog;
using LJ.Models.User;

namespace LJ.ViewModels.User
{

    
    public class ImageViewModel 
    {     
        public int NewImageId { get; set; }       
        public int Id { get; set; }
    }

    public class ImageCropViewModel : ImageViewModel
    {
        
        [Range(0, int.MaxValue)]
        public int X { get; set; }

        [Range(0, int.MaxValue)]
        public int Y { get; set; }

        [Range(1, int.MaxValue)]
        public int Width { get; set; }

        [Range(1, int.MaxValue)]
        public int Height { get; set; }

    }

     public class ImageCropForDisplyViewModel : ImageCropViewModel
     {

        [Display(Name = "Uploading type")]
        int ImageUploadTypeId { get; set; }

        [Display(Name = "Internet URL")]
        public string Url { get; set; }

        [Display(Name = "Flickr image")]
        public string Flickr { get; set; }

        [Display(Name = "Local file")]
        public HttpPostedFileBase File { get; set; }

         public bool IsUrl { get; set; }
         public bool IsFlickr { get; set; }
         public bool IsFile { get; set; }
     }

     public class UploadedImageViewModel : ImageCropViewModel
     {
        public int ImageUploadTypeId  { get; set; }
        public string Url { get; set; }
        public HttpPostedFileBase File { get; set; }
     }


       


    
}