﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;





namespace LJ.ViewModels.User
{
    public class UserPanelInfo
    {
        public UserPanelInfo() { }

        public string UserName { get; set; }
        public string VisitorUserName { get; set; }
        public int DefaultUserImageId { get; set; }
        public long FriendType { get; set; }
        public string MsgForUserType { get; set; }
        public int PostsPerPage { get; set; }
        public IEnumerable<Group> FriendGroups { get; set; }



        public UserPanelInfo(string userName, long friendType, string msgForUserType, 
                   int userDefaultImageId = 1, string visitorUserName = "")
        {
            UserName = userName;
            VisitorUserName = visitorUserName;
            DefaultUserImageId = userDefaultImageId;
            FriendType = friendType;
            MsgForUserType = msgForUserType;
        }
    }
}