﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;
using LJ.Models.Blog;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LJ.ViewModels.User
{

    public class UserFilter
    {
        public int Count { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }


    public class UserStatistics
    {
        public IEnumerable<UserFilter> Categories { get; set; }
        public IEnumerable<UserFilter> Tags { get; set; }
        public int PostsCount { get; set; }
        public int ResivedComments { get; set; }
        public int PostedComments { get; set; }
        public int PostedReplies { get; set; }
        public int ResivedReplies { get; set; }
    }


    public class UserProfile
    {
        public ApplicationUser User { get; set; }
        public UserStatistics Statistics { get; set; }
    }
  

    public class UserViewModel : UserPanelInfo
    {
        public UserViewModel() { }

        public UserViewModel(ApplicationUser user)
        {
            var userInfo = user.ApplicationUserInfo;
            UserName = user.UserName;
            DefaultUserImageId = user.DefaultUserImageId ?? 1;
            FirstName = userInfo.FirstName;
            LastName = userInfo.LastName;            
            IsNotPublic = userInfo.IsNotPublic;
            Country = userInfo.Country;
            City = userInfo.City;
            About = userInfo.About;              
            UserImages = user.UserImages
                        .OrderBy(i => i.ImageOrderId)
                       .Select(i => new ImageViewModel { Id = i.Id, NewImageId = 0});
            UserImage = new ImageCropForDisplyViewModel();
        }


         [Display(Name = "First Name")]
         public string FirstName { get; set; }
         [Display(Name = "Last Name")]
         public string LastName { get; set; }
         [Display(Name = "Account created")]
         public DateTime Created { get; set; }
         [Display(Name = "Personal info is public")]
         public bool IsNotPublic { get; set; }
         [Display(Name = "Country")]
         public string Country { get; set; }
         [Display(Name = "City")]
         public string City { get; set; }
         [Display(Name = "About")]
         public string About { get; set; }         
         [Display(Name = "Userpic")]
         public IEnumerable<ImageViewModel> UserImages { get; set; }
         public IEnumerable<UploadedImageViewModel> UploadedUserImages { get; set; }
         public ImageCropForDisplyViewModel UserImage { get; set; }

         //Additional info
         public int PostsCount { get; set; } //published posts
         public int CommentsPosted { get; set; }
         public int CommentsReceived { get; set; }
         public IEnumerable<Tag> Tags { get; set; }
         public IEnumerable<Category> Categories { get; set; }

         public ApplicationUserInfo ToApplicationUserInfo(UserViewModel uv)
         {
             var info = new ApplicationUserInfo();
             info.LastName = uv.LastName;
             info.FirstName = uv.FirstName;
             info.City = uv.City;
             info.Country = uv.Country;
             info.IsNotPublic = uv.IsNotPublic;
             return info;
         }  
    }
}