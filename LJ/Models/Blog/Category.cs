﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;


namespace LJ.Models.Blog
{   [Table ("Categories")]
    public class Category
    {
        public  int Id
        { get; set; }

        [Required(ErrorMessage = "Category Name: Field is required")]
        [StringLength(500, ErrorMessage = "Tag Name: Length should not exceed 500 characters")]
        public  string Name
        { get; set; }

       [Required(ErrorMessage = "Category Url: Field is required")]
        [StringLength(500, ErrorMessage = "Tag Url: Length should not exceed 500 characters")]
        public  string Url
        { get; set; }

        public  string Description
        { get; set; }

        [JsonIgnore]
        [InverseProperty("Category")]
        public virtual ICollection<Post> Posts
        { get; set; }
    }
}