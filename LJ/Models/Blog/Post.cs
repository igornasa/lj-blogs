﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using LJ.Models.User;

namespace LJ.Models.Blog
{
    public class Post
    {
        public  int Id
        { get; set; }
         public  int SecurityStatus 
         { get; set; }

        [NotMapped]
        public int CurrentTopOrder { get; set; }
        public int CurrentTopRating { get; set; }

        public string ApplicationUserId // ASP Identity Id
        { get; set; }
        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser 
        { get; set; }

        [Required(ErrorMessage = " is required")]
        [StringLength(200, ErrorMessage = " Length should not exceed 200 characters")]
        public  string Title
        { get; set; }

         [Display(Name = "Short Description")]
        [Required(ErrorMessage = "Short Description: Field is required")]
        public  string ShortDescription
        { get; set; }
       
        public string Content
        { get; set; }

        [Display(Name = "Content")]
        [Required(ErrorMessage = "Content: Field is required")]
        public  string Description
        { get; set; }
      
        public  string Meta
        { get; set; }
      
        public  string Url
        { get; set; }

        public  bool Published
        { get; set; }
      
        public ICollection<Group> Groups { get; set; }

        public  DateTime PostedOn
        { get; set; }

        public  DateTime? Modified
        { get; set; }

        public int UserImageId { get; set; }
        public ApplicationUserImage UserImage { get; set; }

        [Display(Name = "Post category")]
        [Required(ErrorMessage = " is required")]
        public int CategoryId
        { get; set; }

        [ForeignKey("CategoryId")]
        public  Category Category
        { get; set; }
        
        [InverseProperty("Post")]
        public virtual ICollection<PostRating> PostRatings 
        { get; set; }
        
        [InverseProperty("Post")]
        public virtual ICollection<Comment> Comments 
        { get; set; }

        [InverseProperty("Post")]
        public virtual ICollection<Reply> Replies 
        { get; set; }

        [InverseProperty("Post")]
        public virtual ICollection<PostLike> PostLikes 
        { get; set; }

        public virtual ICollection<Tag> Tags
        { get; set; }

         
        
    }
}