﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;




namespace LJ.Models.Blog
{
    public class UserRating
    {   [Key]
        public int Id { get; set; }

        public int UserRatingJobId { get; set; }
        [ForeignKey("UserRatingJobId")]
        public UserRatingJob UserRatingJob { get; set; }

        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser User { get; set; }

        public int Rating { get; set; }
        public int AverageRating { get; set; }

        public DateTime DateTime { get; set; }
        public DateTime FromDateTime { get; set; }
        public DateTime ToDateTime { get; set; }
        public int TopDenseRank { get; set; }

        //Comments
        public int CommentsCount { get; set; }
        public int UsersCommentsCount { get; set; }

        //Likes
        public int LikesCount { get; set; }
        public int DislikesCount { get; set; }

        //Visits
        public long SpentTimeMC { get; set; }
        public int VisitsCount { get; set; }
        public int VisitorsCount { get; set; }
        public int UsersCount { get; set; }
        public int AnonymousCount { get; set;}

        //Friends
        public int AddCount { get; set; }
        public int DeleteCount { get; set; }
        public int WasAddedCount { get; set; }
        public int WasDeletedCount { get; set; }
 
    }
}