﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LJ.Models.User;

namespace LJ.Models.Blog
{
    public class PostLike
    {
        public int Id { get; set; }
      
        public int PostId { get; set; }
        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }
       
        public virtual ApplicationUser User { get; set; }
        public bool Like { get; set; }
        public bool Dislike { get; set; }
        public DateTime DateTime { get; set; }
    }
}