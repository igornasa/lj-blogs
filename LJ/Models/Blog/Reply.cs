﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LJ.Models.User;

namespace LJ.Models.Blog
{
    public class Reply
    {
        public int Id { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }

        public int CommentId { get; set; }
        public Comment Comment { get; set; }

        public int UserImageId { get; set; }
        public ApplicationUserImage UserImage { get; set; }
        
        public DateTime DateTime { get; set; }
        public string UserName { get; set; }

        [Required]
        public string Body { get; set; }

        public int StatusId { get; set; }
        [NotMapped]
        public Status StatusValue
        {
            get { return (Status)StatusId; }
            set { StatusId = (int)value; }
        }
        
        public int? ParentReplyId { get; set; }
        public Reply ParentReply { get; set; }
        public virtual ICollection<Reply> ParentReplies { get; set; }
        public ICollection<ReplyLike> ReplyLikes { get; set; }

        [NotMapped]
        public int LikesCount { get; set; }
        [NotMapped]
        public int DislikesCount { get; set; }
    }
}