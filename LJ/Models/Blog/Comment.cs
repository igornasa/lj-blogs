﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LJ.Models.User;


namespace LJ.Models.Blog
{
    public class Comment
    {
        public int Id { get; set; }

        public int PostId { get; set; }
        [ForeignKey ("PostId")]
        public virtual Post Post { get; set; }
        

        public DateTime DateTime { get; set; }
        public string UserName { get; set; }

        [Required]
        public string Body { get; set; }

        [DefaultValue(0)]
        public int NetLikeCount { get; set; }

        public int StatusId { get; set; }
        [NotMapped]
        public Status StatusValue
        {
            get { return (Status)StatusId; }
            set { StatusId = (int)value; }
        }
       
        public int UserImageId { get; set; }
        public ApplicationUserImage UserImage { get; set; }

        public virtual ICollection<Reply> Replies { get; set; }
        public ICollection<CommentLike> CommentLikes { get; set; }
    }
}