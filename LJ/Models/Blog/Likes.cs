﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LJ.Models.Blog
{
    
    public enum Likes
    {
        Post =1,
        Comment = 2,
        Reply = 4,

        Like = 8,
        Dislike = 16,

        LikePost = (8 + 1),
        LikeComment = (8 + 2),
        LikeReply = (8 + 4),

        DislikePost = (16 + 1),
        DislikeComment = (16 + 2),
        DislikeReply = (16 + 4),

        PostCommentReply = (1 + 2 + 4),
        LikeDislike = ( 8 + 16)
    } 
}