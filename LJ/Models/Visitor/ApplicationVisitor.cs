﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;
using System.Web.Security;
using System.Web.Mvc;
using LJ.Models.Visitor;
using LJ.Models.User;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LJ.Models.Visitor
{
    public class ApplicationVisitor
    {
        [Key]
        [Column(Order = 1)]
        public string SessionId { get; set; }
        [Key]
        [Column(Order = 2)]
        public DateTime SessionStarted { get; set; }
        public DateTime SessionEnded { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public string CountryId { get; set; }
        public string CountryName { get; set; }
        public string Anonymous { get; set; }
        public string IpAddress { get; set; }
        public string UrlReferrer { get; set; }
        public string EnterUrl { get; set; }
        public string UserAgent { get; set; }
    }
}