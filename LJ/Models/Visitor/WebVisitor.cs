﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;
using System.Web.Security;
using System.Web.Mvc;
using System.Net;
using LJ.Models.Visitor;
using LJ.Models.User;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LJ.Models.Visitor
{
  
    public class WebVisitor : ApplicationVisitor
    {
        [NotMapped]
        public string AuthUser { get; set; }
        [NotMapped]
        public GeoLocation GeoLocation { get; set; }

       
        public WebVisitor() { }

        public WebVisitor(HttpContext context)
        {
            if (context != null && context.Request != null && context.Session != null)
            {
                this.SessionId = context.Session.SessionID;
                this.SessionStarted = DateTime.Now;// DateTime.UtcNow;
                this.UserAgent = context.Request.UserAgent ?? String.Empty;
                string ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                this.IpAddress = string.IsNullOrEmpty(ip) ? 
                    context.Request.ServerVariables["REMOTE_ADDR"] : ip.Split(',').Last().Trim();
               
                if (context.Request.IsAuthenticated)
                {
                    this.AuthUser = context.User.Identity.Name;
                    if (!String.IsNullOrEmpty(context.Request.ServerVariables["REMOTE_USER"]))
                        this.AuthUser = context.Request.ServerVariables["REMOTE_USER"];
                    else if (!String.IsNullOrEmpty(context.Request.ServerVariables["AUTH_USER"]))
                        this.AuthUser = context.Request.ServerVariables["AUTH_USER"];
                }
               
                this.Anonymous = context.Request.AnonymousID;

                if (context.Request.UrlReferrer != null)
                {
                    this.UrlReferrer = String.IsNullOrWhiteSpace(context.Request.UrlReferrer.OriginalString) ? 
                        "" : context.Request.UrlReferrer.OriginalString;
                }
                this.EnterUrl = String.IsNullOrWhiteSpace(context.Request.Url.OriginalString) ? 
                        "" : context.Request.Url.OriginalString;
            }
        }

        //================================================
        public static bool IsLocalIpAddress(string host)
        {
            try
            { // get host IP addresses
                IPAddress[] hostIPs = Dns.GetHostAddresses(host);
                // get local IP addresses
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());

                // test if any host IP equals to any local IP or to localhost
                foreach (IPAddress hostIP in hostIPs)
                {
                    // is localhost
                    if (IPAddress.IsLoopback(hostIP)) return true;
                    // is local address
                    foreach (IPAddress localIP in localIPs)
                    {
                        if (hostIP.Equals(localIP)) return true;
                    }
                }
            }
            catch { }
            return false;
        }
    }

   //====================================================
    public static class OnlineVisitorsContainer
    {
        public static readonly ConcurrentDictionary<string, WebVisitor> Visitors = new ConcurrentDictionary<string, WebVisitor>();
    }  
}