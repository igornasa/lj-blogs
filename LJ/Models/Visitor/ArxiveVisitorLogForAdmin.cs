﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LJ.Models.Visitor
{
    public class ArxiveVisitorLogForAdmin
    {
          public long    Id {get; set;}
          public DateTime StartDateTime { get; set; }
          public DateTime EndDateTime { get; set; }
          public string  Controller {get; set;}
          public string  Action {get; set;}
          public int     ActionType { get; set; }
          public long    SpentTimeMC {get; set;}
          public long    CountVisits {get; set;}
          public long    CountUsers {get; set;}
          public long    CountAnonymous {get; set;}
          public long    ExitCount {get; set;}
          public long    BounceCount { get; set; }
    }
}