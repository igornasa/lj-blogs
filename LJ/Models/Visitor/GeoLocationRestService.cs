﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LJ.Models.Visitor
{
    public class GeoLocationRestService
    {
        
        readonly string uriForLocal = "http://api.ipstack.com/check?access_key=708478b7fa02bc7a808dc7c7bdeea82f";
        readonly string uri = "http://api.ipstack.com/{0}?access_key=708478b7fa02bc7a808dc7c7bdeea82f";

        // http://api.ipstack.com/134.201.250.155?access_key=708478b7fa02bc7a808dc7c7bdeea82f
        //http://api.ipstack.com/134.201.250.154?access_key=708478b7fa02bc7a808dc7c7bdeea82f
        public GeoLocation GetGeoLocation(string ip)
        {
            ip = "134.201.250.155";
            using (WebClient webClient = new WebClient())
            {
                var url = string.IsNullOrEmpty(ip)? uriForLocal : String.Format(uri, ip);
                return JsonConvert.DeserializeObject<GeoLocation>(
                        webClient.DownloadString(url)
                );
            }
        }

        public async Task<GeoLocation> GetGeoLocationAsync(string ip)
        {

            using (HttpClient httpClient = new HttpClient())
            {

                return JsonConvert.DeserializeObject<GeoLocation>(
                       await httpClient.GetStringAsync(uri + ip)
                );
            }
        }
    }
}