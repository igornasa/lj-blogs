﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;

namespace LJ.Models.User
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ApplicationUser> Friends { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}