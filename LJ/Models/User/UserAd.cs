﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LJ.Models.User
{
    public class UserAd
    {         
        [Key]
        public string FileName { get; set; }      
        public int Count { get; set; }
    }
}