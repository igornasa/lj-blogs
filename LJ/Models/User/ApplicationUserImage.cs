﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LJ.Models.User
{
    public class ApplicationUserImage
    {
        [InverseProperty("UserImages")] 
        public virtual ICollection<ApplicationUser> Users { get; set; }

        [InverseProperty("DefaultUserImage")] 
        public virtual ICollection<ApplicationUser> User { get; set; }
       
        [Key]
        public int Id { get; set; }
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
        public int ImageOrderId { get; set; }
       
        

        public void SaveImageFromHttp(HttpPostedFileBase image)
        {

            ImageMimeType = image.ContentType;
            ImageData = new byte[image.ContentLength];
            image.InputStream.Read(ImageData, 0, image.ContentLength);
        }



        public void SaveImageFromDisc(string filePath)
        {
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                ImageData = new byte[fileStream.Length];
                fileStream.Read(ImageData, 0, (int)fileStream.Length);
                ImageMimeType = "image/jpeg";
            }
        }
    }
}
