﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LJ.Infrastructure
{
    public static class ValidationModelHelpers
    {    //ASP.NET MVC - Excluding Model Validation Rules 
        //http://www.compiledthoughts.com/2011/02/aspnet-mvc-excluding-model-validation.html

        public static void CleanAllBut(
        this ModelStateDictionary modelState,
        params string[] includes)
        {
            modelState
              .Where(x => !includes
                 .Any(i => String.Compare(i, x.Key, true) == 0))
              .ToList()
              .ForEach(k => modelState.Remove(k));
        }


        public static void CleanSometing(
        this ModelStateDictionary modelState,
        params string[] includes)
        {
            modelState
              .Where(x => includes
                 .Any(i => String.Compare(i, x.Key, true) == 0))
              .ToList()
              .ForEach(k => modelState.Remove(k));
        }
    }
}