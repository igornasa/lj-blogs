﻿$(document).ready(function () {
   
  
    $('.commentMenu').css("visibility", "hidden");
    $(".commentControl").css("visibility", "hidden");
    commentVisibility(".maincomment");
    commentVisibility(".mainreply");
    commentVisibility(".childReplyCont");

    var deleteLinkObj;

    // images select  , onSelected update imageid   
    //====================================
    var imageid;
  

    $('#ddimages').ddslick({
        width : 80,
        onSelected: function (data) {                      
            $("#" + imageid).val(data.selectedData.value.toString());
        }
    });
    var userimages = $("#ddimages");
    
    $("#ddimages").detach();
    

     // Modal window
    //https://ricardocovo.com/2010/09/02/asp-mvc-delete-confirmation-with-ajax-jquery-ui-dialog/
    //http://stackoverflow.com/questions/887029/how-to-implement-confirmation-dialog-in-jquery-ui-dialog


    // Hover show  likes list
    //http://html5beta.com/jquery-2/a-little-guide-about-jqueryui-tooltip-ajax-callback/
    //================================
    $(document).tooltip({
        items: 'a.likecount',
        tooltipClass: 'preview-tip',
        position: { my: "left+5 top", at: "right center" },
        content: function (callback) {
            var $this = $(this);
            var numbertoshow = 20;
            var href = $this.prev().attr("href")
                  .replace("UpdateLikes", "GetLikesList") + "&numbertoshow=" + numbertoshow;
           
            $.post(href, function (data) {                
                var userNamesWhoLikes = "<div><span class=\"likeListheader\">" +
                       data.likeordislike + ":</span><br/>" +
                       (data.count ? data.usernames.join(",<br>") : "No names") +
                       (data.count > numbertoshow ? "<br/>and " + (data.count - numbertoshow) + " more ..." : "") +
                       "</div>";
                
                callback(userNamesWhoLikes);
                if ($this.text() != data.count) $this.text(data.count);
            });
        },
    });





    // Comment, Reply. Post likes dislakes
    //================================
    $(function () {
       
        $("a.linklike").click(function (e) {
            e.preventDefault();

           var $this = $(this);
           $.post($this[0].href, function (data) {
               if (data.count == "-1") alert("You have rated it before");
               else 
                  $this.next().text(data.count);
           });

       });
   });

    //confirm delete
    //==============================
    $(function () {
       
        $("a.confirm-delete").click(function (e) {
           
            e.preventDefault();

            var target = $(this).attr("href");
            var title = $(this).attr("title");
            var content = $(this).attr("alt");
            
            deleteLinkObj = $(this);
            
            var strdialog = "<div id=\"dialog-confirm\" title=\"" + content +
                "\"><p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:12px 12px 20px 0;\"></span>The " +
                content + " will be permanently deleted and cannot be recovered. Are you sure?</p></div>";
               
          //  alert(target +"<br/> " +deleteLinkObj[0].href);
            $(strdialog).dialog({
                draggable: false,
                modal: true,
                resizable: false,
                width: 'auto',
                title: title,
                buttons: {
                   
                    "Confirm": function () {
                        $.post(deleteLinkObj[0].href, function (data) {
                           
                            //delete replies
                            var ids = data.ReplyIdsForDelete;
                            if (ids.length > 0) {
                                for (var i = 0 ; i < ids.length; i++) {
                                    if (ids[i].Deleted) {

                                        $("#gp" + ids[i].Id).remove();
                                    }
                                    else {
                                        $("#cex" + ids[i].Id).children().first().html(data.DeletedReplyText);
                                        $("#date" + ids[i].Id).html(ids[i].DateTime);
                                        $("#cc2" + ids[i].Id).remove();

                                    }
                                }
                            };

                            //delete comment
                            ids = data.CommentIdsForDelete;
                            if (ids.length > 0) {
                                for (var i = 0 ; i < ids.length; i++) {
                                    if (ids[i].Deleted) {

                                        $("#gpcmt" + ids[i].Id).remove();
                                    }
                                    else {
                                        $("#cexcmt" + ids[i].Id).children().first().html(data.DeletedCommentText);
                                        $("#datecmt" + ids[i].Id).html(ids[i].DateTime);
                                        
                                        $("#cc2cmt" + ids[i].Id).remove();

                                    }
                                }
                            };

                            
                        });

                        $(this).dialog("close");
                    },
                  
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });

        });

    });

     //  edit reply
    //==============================   
    $("a.form-edit").click(function (e) {
        e.preventDefault();

        var arr = $(this).attr("href").split("/");
        var id = arr[0];
        var imageId = arr[1];
        var username = arr[2];
        var replytime = $("#date" + id).html();
        var target = $("#cex" + id);
        var oldBody = target.children().first();
        var replyText = oldBody.html();
        var textareaId = "formtdc" + id;
        
        var value = {
            isNew : false,
            replytime: replytime,
            id: id,
            isComment :false,
            userImageId : imageId,
            action: "/Blog/EditReply/",
            replyBody: replyText
        };
        var newControl = $("#tmpFormControl").tmpl(value);
        
       

        var target1 = $("#crp" + id);
        var oldControl = $("#cex" + id);
        var target1 = oldControl.prev();
                            
        oldControl.detach();               
        target1.append(newControl);
       
        tinymce.execCommand('mceAddEditor', false, textareaId);
       

        var ddimages = $('#ddTmpUserImages' + id);
        var tt = $("#tblEditComment").length + "  " + $("#tblEditComment").height(); 
        ddimages.ddslick({
           
            width: 100,
            //height: 100,
            //imagePosition: "right",
            onSelected: function (data) {                      
                $("#userimageid" + id).val(data.selectedData.value.toString());
            }
        });
      
      
     
        //#SelectBox is the id of ddSlick selectbox      
        $('#ddTmpUserImages' + id +' li').each(function (index) {
           var curValue = $(this).find('.dd-option-value').val();                
           if (curValue == imageId) {
               $('#ddTmpUserImages' + id).ddslick('select', { index: $(this).index() });
           }
        });
        
        
       
       // alert("" + $("#tblEditComment").length + "  " + $("#tblEditComment").height());
       
      
    });

//====================
    $(".commentExpCtrl").click(function () {
        var clicks = $(this).data('clicks');
       
        if (!clicks) {
            commentExpand(this, '.maincomment', '.userComment');
            commentExpand(this, '.mainreply', '.parentReply');
            commentExpand(this, '.childReplyCont', '.childReply');
        } else {
            commentCollapse(this, '.maincomment', '.userComment');
            commentCollapse(this, '.mainreply', '.parentReply');
            commentCollapse(this, '.childReplyCont', '.childReply');
        }
        $(this).data("clicks", !clicks);
    });


    $('.shareChild').css("display", "none");
    $('.shareChild').css("margin-left", "-40px");
    shareMenuVisibility(".shareParent");



    $('.postMenuSub').css('visibility', 'hidden');
    $('.postContainer').hover(
        function () {
            $('.postMenuSub').css('visibility', 'visible');
        }, function () {
            $('.postMenuSub').css('visibility', 'hidden');
        }
    );

    // New Reply 
    //===================================
    $('.comReplyParent').click(function () {
       
       
        var clicks = $(this).data('clicks');
        var id = $(this).attr("id");
        var parentid = $('#' + id).closest('.commentExp').attr("id");
        var id = parentid.substring(3);
        
        var textareaid = "tdc" + id;
        var imageid = "img" + id;
        var imgselectid = "imgselect" + id;
        if (!clicks) {           
            $("#" + imgselectid).append($(userimages));         
            $("#" + parentid + ">.collapseComment").show();
            tinymce.execCommand('mceAddEditor', false, textareaid);
           
        }
        else {
            tinymce.execCommand('mceRemoveEditor', false, textareaid);
            $('.collapseComment').hide();
            //$(textareaid).val( tinyMCE.get("ShortDescription").getContent());
        }

        $(this).data("clicks", !clicks);
    });


    // New Comment
    //==================
$('.comNewComment').click(function () {
              
    var clicks = $(this).data('clicks');
    var id = $(this).attr("id");  
    var id = id.substring(3);

    var textareaid = "tdc" + id;
    var imageid = "img" + id;
    var imgselectid = "imgselect" + id;
    $("#tblnewcmt" + id).show();
    if (!clicks) {           
        $("#" + imgselectid).append($(userimages));         
        $("#tblnewcmt" +id).show();
        tinymce.execCommand('mceAddEditor', false, textareaid);
        
           
    }
    else {
        tinymce.execCommand('mceRemoveEditor', false, textareaid);
        $('.collapseComment').hide();
        //$(textareaid).val( tinyMCE.get("ShortDescription").getContent());
    }

    $(this).data("clicks", !clicks);
});




});



//=====================
function commentVisibility(parent) {
    $(parent).hover(
   function () {
       var mcId = $(this).attr("id");
       var id1 = $("#" + mcId + ">.commentControl").attr("id");
       var id2 = $("#" + mcId + ">.commentExp>.replySubMenu>.commentMenu").attr("id");
       $("#" + id1).css("visibility", "visible");
       $("#" + id2).css("visibility", "visible");
   }, function () {
       var mcId = $(this).attr("id");
       var id1 = $("#" + mcId + ">.commentControl").attr("id");
       var id2 = $("#" + mcId + ">.commentExp>.replySubMenu>.commentMenu").attr("id");
       $("#" + id1).css("visibility", "hidden");
       $("#" + id2).css("visibility", "hidden");
   });
}


//===========================
function commentExpand(target, parent, grandparent) {
    var id = $(target).attr("id");
    $("#" + id).html("+");
    $("#" + id).css("font-size", "16px");
    var parentid = $(target).closest(parent).attr('id');
    var expid = $("#" + parentid + "> .commentExp").attr("id");
    var grandparentid = $("#" + parentid).closest(grandparent).attr('id');
    var repliesid = $("#" + grandparentid + "> .commentreplies").attr("id");
    $("#" + expid).css("display", "none");
    $("#" + repliesid).css("display", "none");

    $("#" + id).html("+");
}

//===============================
function commentCollapse(target, parent, grandparent) {
    var id = $(target).attr("id");
    $("#" + id).html("&mdash;");
    $("#" + id).css("font-size", "10px");
    var parentid = $(target).closest(parent).attr('id');
    var expid = $("#" + parentid + "> .commentExp").attr("id");
    var grandparentid = $("#" + parentid).closest(grandparent).attr('id');
    var repliesid = $("#" + grandparentid + "> .commentreplies").attr("id");
    $("#" + expid).css("display", "block");
    $("#" + repliesid).css("display", "block");
}

//===================================
function shareMenuVisibility(parent) {
    $(parent).hover(
        function () {
            var mcId = $(this).attr("id");
            var id = $("#" + mcId + ">div>a").attr("id");
            $("#" + id).css({ 'display': 'block', 'opacity': '0' })
     .animate({ opacity: 1, marginLeft: 0 }, 500);
        }, function () {
            var mcId = $(this).attr("id");
            var id = $("#" + mcId + ">div>a").attr("id");
            $("#" + id).css("display", "none");
            $("#" + id).css("margin-left", "-40px");
        });


    
    // save newReply
    //===========================
    function saveNewReply(e) {
        
        e.preventDefault();
        var frm = $(this);
        var url = $frm.attr('action');
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: frm.serialize(),
            contentType: 'application/json; charset=utf-8',
            error:
                function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + " ---- " +
                        thrownError);
                },
            success: function (data) {
                alert("Id");
                
                
            }
        });



       
        //==========================
    }
}



