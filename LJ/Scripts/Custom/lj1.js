﻿$(function () {
    $('#search-form').submit(function () {
        if ($("#search").val().trim())
            return true;
        return false;
    });
});

var afterShowForm = function (form) {
    tinyMCE.execCommand('mceAddControl', false, "ShortDescription");
    tinyMCE.execCommand('mceAddControl', false, "Description");
};

var onClose = function (form) {
    tinyMCE.execCommand('mceRemoveControl', false, "ShortDescription");
    tinyMCE.execCommand('mceRemoveControl', false, "Description");
};