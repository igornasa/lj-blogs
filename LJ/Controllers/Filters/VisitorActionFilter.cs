﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LJ.Models.Visitor;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using Hangfire;
using Hangfire.Dashboard;
using LJ.Controllers;


//Positional and named parameters for an attribute class are limited to the following attribute parameter types
//https://msdn.microsoft.com/en-us/library/7kk5syz6(VS.90).aspx

namespace LJ.Filters
{
    public class VisitorActionFilter : ActionFilterAttribute, IActionFilter
    {
        public string UserName = "";// { get; set; }
        public string PostId = "";//{ get; set; }
        public int ActionType = 0;//{ get; set; }
       

      void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
      {


         /* var actionName = filterContext.ActionDescriptor.ActionName;
        var actionParams = filterContext.ActionDescriptor.GetParameters
        var actionParamsTypes = actionParams.Cast<ParameterDescriptor>()
                                      .Select(x => x.ParameterType).ToArray();
        var controllerType = filterContext.Controller.GetType();            
        var actionMethodInfo = controllerType.GetMethod(actionName,
                                                        actionParamsTypes, null);            
        var isMethodPost = actionMethodInfo.IsDefiend(typeof(HttpPostAttribute),
                                                      false);*/
          int postId = 0;
          if (!String.IsNullOrEmpty(PostId) && filterContext.ActionParameters.ContainsKey(PostId))
          {
              int? id = filterContext.ActionParameters[PostId] as  Int32?;
              postId = id.GetValueOrDefault();
          }

          

          string userName = "";
          if (!String.IsNullOrEmpty(UserName) && filterContext.ActionParameters.ContainsKey(UserName))
          {
              userName = (filterContext.ActionParameters[UserName] as string) ?? "";
             
          }

          var categoryId = filterContext.ActionParameters.ContainsKey("categoryid")?
               (filterContext.ActionParameters["categoryid"] as Int32?).GetValueOrDefault() : 0;

          var tagId = filterContext.ActionParameters.ContainsKey("tagid") ?
               (filterContext.ActionParameters["tagid"] as Int32?).GetValueOrDefault() : 0;

        VisitorActionLog log = new VisitorActionLog()
        {
            Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
            Action = filterContext.ActionDescriptor.ActionName,
            IpAddress = filterContext.HttpContext.Request.UserHostAddress,
            DateTime = filterContext.HttpContext.Timestamp,
            VisitorUserName = filterContext.HttpContext.User.Identity.Name,
            Anonymous = filterContext.HttpContext.Request.AnonymousID,
            UserName = userName,
            ActionType = ActionType,
            PostId = postId,
            TagId = tagId,
            CategoryId = categoryId,
            SessionId = filterContext.HttpContext.Session.SessionID
        };
 BackgroundJob.Enqueue(() => VisitorController.JobSaveVisitorActionLog(log));
      base.OnActionExecuting(filterContext);
    }
    
    }
}