﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading.Tasks;

using System.Data.Entity;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration.Conventions;
using LJ.Repository.BlogRepository;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.ViewModels;
using LJ.ViewModels.User;
using LJ.ViewModels.Blog;
using System.IO;
using LJ.Filters;
using LJ.Models.Visitor;
using Microsoft.AspNet.Identity.Owin;





namespace LJ.Controllers
{
    [Authorize]
    public class BlogController : Controller
    {
      
        private const int userPostsPerPage = 10;// posts per page for user
        private const int topPostsPerPage = 10; // top posts per page
        private IBlogRepository<Post,int> _blogRepository;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        //==============================================
        public BlogController(IBlogRepository<Post,int> blogRepository)
        {
            _blogRepository = blogRepository;
           

        }

        //==============================================
        public BlogController(IBlogRepository<Post, int> blogRepository, ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            _blogRepository = blogRepository;
            UserManager = userManager;
            SignInManager = signInManager;

        }

        private async Task<ApplicationUser> GetCurrentUserAsync()
        {
            return await UserManager.FindByIdAsync(User.Identity.GetUserId());
        }



        //==============================================
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        //==============================================
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        //==============================================
        public ActionResult Index()
        {
            IEnumerable<Post> posts = _blogRepository.GetAll(1,10);
            return View(posts);
        }

        
       

        
        //==============================================
        public ActionResult GetPostsOLd(int post = 1, int perPage = 10)
        {
            ModelViewPosts mv = new ModelViewPosts();

            mv.Posts = _blogRepository.GetAll(post, perPage);
            mv.TotalPosts = _blogRepository.TotalPosts();
            ViewBag.Title = "Top Posts";
            return View("Posts", mv);
        }

        
       
        //================================================
        [ChildActionOnly]
        public ActionResult Comments(PostViewModel model, string sortOrder)
        {
        return PartialView(model);
        }


      // for View
        //====================================================
        public PartialViewResult ChildReplies(int parentReplyId, int level)
        {
            ViewBag.level = level;
            IEnumerable<CommentViewModel> v = _blogRepository.GetChildReplies(parentReplyId);
                return PartialView("ChildReplies", v);
        }


        
        // Like and dislake post, comment, reply
        //=============================================
        public int LikeDislikeCount(int typeAndlike, int id)
        {       
            return _blogRepository.LikeDislikeCount(typeAndlike, id);                 
        }



        //==============================================
        [AcceptVerbs(HttpVerbs.Post)]
        //[VisitorActionFilter(ActionType = (int)ActionType.SeeBlog)]
        public JsonResult UpdateLikes(int id, string username, int likeordislike, string sortorder="")
        {
            string name = User.Identity.GetUserName();
            int count = _blogRepository.UpdateLikes(id, name, likeordislike);
            return Json(new { count = count }, JsonRequestBehavior.AllowGet);
        }


        //==============================================
        [AcceptVerbs(HttpVerbs.Post)]
       // [VisitorActionFilter(ActionType = (int)ActionType.SeeBlog)]
        public JsonResult GetLikesList(int id, string username, int likeordislike, int numbertoshow)
        {
            var allUserNames = _blogRepository.GetListLikes(id, likeordislike);
            var allUserNamesCount = allUserNames.Count();            
            return Json(
                        new { count = allUserNamesCount,
                              usernames = allUserNames.Take(numbertoshow),
                              likeordislike = Enum.GetName(typeof(Likes), likeordislike & ((int)Likes.LikeDislike))
                        },
                        JsonRequestBehavior.AllowGet);
        }  
    



        // Add new comment repoly
        //======================================
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
      //[VisitorActionFilter(ActionType = (int)ActionType.SeeBlog, PostId = "postid")]
        public ActionResult NewParentReply(string body, string comusername, int postid, int id, string slug, int userimageid)
        {
            var reply = new Reply()
            {
                Id = 0,
                UserImageId = userimageid,
                PostId = postid,
                CommentId = id,
                ParentReplyId = null,
                DateTime = DateTime.Now,
                UserName = comusername,
                Body = body,
            };
            int newId =  _blogRepository.AddNewReply(reply);
            //return Json(new { id = newId }, JsonRequestBehavior.AllowGet);
            return RedirectToAction("GetUserPost", new { id = postid });       
        }

        //=========================================
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)] //string preplyid, string comUserName, string replyBody)
      //  [VisitorActionFilter(ActionType = (int)ActionType.SeeBlog)]
       public ActionResult NewChildReply(string body, string comusername, int id, int userimageid)
        {
            Reply reply = _blogRepository.GetReplyById(id);
            var newReply = new Reply()
            {
                Id = 0,
                UserImageId = userimageid,
                PostId = reply.PostId,
                CommentId = reply.CommentId,
                ParentReplyId =id,
                DateTime = DateTime.Now,
                UserName = comusername,
                Body = body,
            };
            int newId = _blogRepository.AddNewReply(newReply);
            return RedirectToAction("GetUserPost", new { id = reply.PostId });
        }

        //==============================================
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
      //  [VisitorActionFilter(ActionType = (int)ActionType.SeeBlog, PostId = "id")]
        public ActionResult NewComment(string body, string comusername, int id, int userimageid, string slug)
        {            
            Comment newComment = new Comment()
            {
                Id = 0,
                UserImageId = userimageid,
                PostId = id,
                DateTime = DateTime.Now,
                UserName = comusername,
                Body = body,
            };
            int newId = _blogRepository.AddNewComment(newComment);
            return RedirectToAction("GetUserPost", new { id = newComment.PostId });
        }



        // Delete comment repky
       [AcceptVerbs(HttpVerbs.Post)]
      // [VisitorActionFilter]
       public JsonResult DeleteComment(int commentid, string username)
       {
           var deleteInfo = _blogRepository.DeleteComment(commentid, username);
           deleteInfo.DeletedReplyText = "";
           return Json(deleteInfo, JsonRequestBehavior.AllowGet);
       }

       //==============================================
       [AcceptVerbs(HttpVerbs.Post)]
      // [VisitorActionFilter]
       public JsonResult DeleteReply(int replyid, string username)
       {
           var deleteInfo = _blogRepository.DeleteReply(replyid, username);        
           return Json(deleteInfo, JsonRequestBehavior.AllowGet);
       }

       //==============================================
       [HttpPost]
       //[VisitorActionFilter(PostId = "postid")]
       public  ActionResult DeletePost(int postid, int nextid, int previd, string username)
       {
           var deleteInfo = _blogRepository.DeletePost(postid, username);
           if (nextid == 0 && previd == 0)
               return RedirectToAction("GetUserPosts", new { id = username });
           else if (nextid == 0 || previd == 0) 
               return RedirectToAction("GetUserPost", new { id = previd +nextid});
            else 
               return RedirectToAction("GetUserPost", new { id = nextid});
       }



       //==============================================
       [HttpPost]
      // [ValidateAntiForgeryToken]
       [ValidateInput(false)]
      // [VisitorActionFilter]
       public async Task<ActionResult> EditComment(int id, string body, string comusername, int userimageid)
       {
           var user = await GetCurrentUserAsync();
           // if (comusername == user.UserName)

          
           int postId = _blogRepository.EditComment(id, body, comusername, userimageid);

           return RedirectToAction("GetUserPost", new { id = postId });
       }

       //==============================================
       [HttpPost]
       [ValidateAntiForgeryToken]
       [ValidateInput(false)]
      // [VisitorActionFilter]
       public async Task<ActionResult> EditReply(int id, string body, string comusername, int userimageid)
       {
           var user = await GetCurrentUserAsync();
           if (user == null) return RedirectToAction("Login","Account" );
           int postId = 0;
           if (comusername == user.UserName)
           {
               postId = _blogRepository.EditReply(id, body, comusername, userimageid);
           }
           else
           {
               var reply = _blogRepository.GetReplyById(id);
               postId = reply == null ? 0 : reply.PostId;
           }
           return RedirectToAction("GetUserPost", new { id = postId });
       }


        // Images
        //============= 
        [AllowAnonymous]
        public FileContentResult GetImage(int id)
        {
            ApplicationUserImage f = _blogRepository.GetImageBy(id);
            if (f == null || f.ImageData == null) return null;
            return File(f.ImageData, f.ImageMimeType);

        }

        
       
        
        //Get Post
        //===============================================
        //By id old       
        [AllowAnonymous]
        [VisitorActionFilter(ActionType = (int)ActionType.SeeBlog, PostId = "id")]
        public async Task<ActionResult> GetUserPost(int id, 
            int tagid = 0, int categoryid = 0, int sortorder = (int)CommentSortOrders.Earliest)
        {
            var visitor = await GetCurrentUserAsync();

            var visitorUserName = visitor == null ? "" : visitor.UserName;

            var post = _blogRepository.GetPostById(id);
            
            
            PostViewModel model = new PostViewModel();

            model.UserName = post.ApplicationUser.UserName;
            model.DefaultUserImageId = post.ApplicationUser.DefaultUserImage.Id; // user panel img
            model.ImageId = post.UserImageId; //post img
           
            model.Id = post.Id;
            model.Url = post.Url;
            model.Published = post.Published;
            model.PostedOn = post.PostedOn;
            model.Title = post.Title;
            model.Category = post.Category;
            model.SecurityStatus = post.SecurityStatus;
            model.Tags = post.Tags;
            model.Description = post.Description;
            model.Body = post.Content;
            model.PostLikes = _blogRepository.LikeDislikeCount((int)Likes.LikePost, post.Id);
            model.PostDislikes = _blogRepository.LikeDislikeCount((int)Likes.DislikePost, post.Id);
            model.SortOrder = sortorder;
            IEnumerable<int> ids = _blogRepository.GetPostIds(post.ApplicationUser.UserName, categoryid, tagid, visitorUserName);           
            model.PrevPostId = ids.SkipWhile(i => i != post.Id).Skip(1).Select(i => i).FirstOrDefault();
            model.NextPostId = ids.TakeWhile(i => i != post.Id).Select(i => i).LastOrDefault();
            model.LastPostId = ids.First();
            model.FirstPostId = ids.Last();
            model.PostCount = ids.Count();
            model.CommentsCount = post.Replies.Count() + post.Comments.Count();
            model.SecurityStatus = post.SecurityStatus;
            model.Groups = post.Groups;
            model.FilterCategory = categoryid == 0 ? null : _blogRepository.GetCategory(categoryid);
            model.FilterTag = tagid == 0 ? null : _blogRepository.GetTag(tagid);           
            model.UserPanelInfo = GetUserPanelInfo(visitorUserName, model.UserName, model.DefaultUserImageId);
                    
            AddCommentViewModels(model, post, sortorder);

            return View("Post", model);
        }

        //============================================       
        private PostViewModel AddCommentViewModels(PostViewModel model, Post post, int sortOrder)
        {

            IEnumerable<CommentViewModel> commentViewModels = _blogRepository
                .GetPostComments(post)
                .OrderByDescending(d => d.Comment.DateTime)
                .ToList();

            foreach (var commentViewModel in commentViewModels)
            {

                var comment = commentViewModel.Comment;
                comment.NetLikeCount = commentViewModel.CommentLikes - commentViewModel.CommentDislikes;
               
                if (comment.Replies != null) comment.Replies.Clear();
                List<CommentViewModel> replies = _blogRepository.GetReplies(comment);
                foreach (var parentReply in replies)
                {
                    var rep = _blogRepository.GetReplyById(parentReply.Id);
                    rep.DislikesCount = LikeDislikeCount((int)Likes.DislikeReply, rep.Id);
                    rep.LikesCount = LikeDislikeCount((int)Likes.LikeReply, rep.Id);
                    
                    comment.Replies.Add(rep);
                }
            }

            switch (sortOrder)
            {              
                case (int)CommentSortOrders.Best:
                    commentViewModels = commentViewModels.OrderByDescending(x => x.Comment.NetLikeCount).ThenByDescending(x => x.Comment.DateTime).ToList();
                    break;
                case (int)CommentSortOrders.Worst:
                    commentViewModels = commentViewModels.OrderBy(x => x.Comment.NetLikeCount).ThenByDescending(x => x.Comment.DateTime).ToList();
                    break;
                case (int)CommentSortOrders.Earliest:
                    commentViewModels = commentViewModels.OrderByDescending(x => x.Comment.DateTime).ToList();
                    break;
                case (int)CommentSortOrders.Oldest:
                    commentViewModels = commentViewModels.OrderBy(x => x.Comment.DateTime).ToList();
                    break;
                case (int)CommentSortOrders.Heaviest:
                    commentViewModels = commentViewModels.OrderByDescending(x => x.CommentReplies).ThenByDescending(x => x.Comment.DateTime).ToList();
                    break;
                case (int)CommentSortOrders.Lightest:
                    commentViewModels = commentViewModels.OrderBy(x => x.CommentReplies).ThenByDescending(x => x.Comment.DateTime).ToList();
                    break;
                default:
                    commentViewModels = commentViewModels.OrderByDescending(x => x.Comment.DateTime).ToList();
                    break;
            }

            model.Url = post.Url;
            model.Comments = commentViewModels;
            return model;
        }

       //===================================================
        [AllowAnonymous]
        [VisitorActionFilter(ActionType = (int)ActionType.SeeBlog, PostId = "id")]
        public async Task<ActionResult> GetPost(int id, long lineid = (long)FriendType.UserPost,
                                         int tagid = 0, int categoryid = 0, int sortorder = 1)
        {
            var visitor = await GetCurrentUserAsync();
            var visitorUserName = visitor == null ? "" : visitor.UserName;
            var post = _blogRepository.GetPostById(id);
            PostViewModel model = new PostViewModel();

            model.UserName = post.ApplicationUser.UserName;
            model.DefaultUserImageId = post.ApplicationUser.DefaultUserImage.Id; // user panel img
            model.ImageId = post.UserImageId; //post img
            model.Id = post.Id;
            model.Url = post.Url;
            model.Published = post.Published;
            model.PostedOn = post.PostedOn;
            model.Title = post.Title;
            model.Category = post.Category;
            model.Tags = post.Tags;
            model.SecurityStatus = post.SecurityStatus;
            model.Description = post.Description;
            model.Body = post.Content;
            model.PostLikes = _blogRepository.LikeDislikeCount((int)Likes.LikePost, post.Id);
            model.PostDislikes = _blogRepository.LikeDislikeCount((int)Likes.DislikePost, post.Id);
            model.SortOrder = sortorder;
            model.UserPanelInfo = GetUserPanelInfo(visitorUserName, model.UserName, model.DefaultUserImageId);
            model.UserPanelInfo.FriendType |= lineid; 
            _blogRepository.GetPostExtraInfo(model, categoryid, tagid);           
            AddCommentViewModels(model, post, sortorder);
            return View("Post", model);
        }


        // Get Posts
        //===============================================      
        [AllowAnonymous]        
        [VisitorActionFilter(ActionType = (int)ActionType.SeeBlog, UserName = "id")]
        public async Task<ActionResult> GetUserPosts(int pageNo = 1, int pageSize = userPostsPerPage, 
                         string id = "", int tagid= 0, int categoryid = 0)
        {
            var username = id;
            var visitor = await GetCurrentUserAsync();
            var visitorUserName = visitor == null ? "" : visitor.UserName;
            if (pageSize == 0) pageSize = userPostsPerPage;

            //My posts or not my posts
            long queryType = visitorUserName == username ? 
                    (long)FriendType.UserPosts | (long)FriendType.MySelf : (long)FriendType.UserPosts;
            //set filters
            if (tagid != 0) queryType |= (long)FriendType.ByTag;
            if (categoryid != 0) queryType |= (long)FriendType.ByCategory;

            var mv = _blogRepository.GetPosts1(pageNo, pageSize, categoryid, tagid, username, queryType, 0, 0, visitorUserName);
                          
            if (mv.Posts == null)
                return View("_LJErrors", " This blogger was not fond");

            mv.UserPanelInfo = GetUserPanelInfo(visitorUserName, mv.User.UserName, mv.User.DefaultUserImage.Id);
            mv.UserPanelInfo.FriendType |= queryType;
            mv.UserPanelInfo.FriendGroups = mv.User.FriendGroups;
            mv.UserPanelInfo.PostsPerPage = pageSize;
            
            ViewBag.Title = "Latest Posts";
            return View("Posts", mv);
        }
        

        //==============================================
        [AllowAnonymous]
        [VisitorActionFilter(ActionType = (int)ActionType.SeeBlog)]
      //  public async Task<ActionResult> GetFriendsPosts(int pageNo = 1, int pageSize = 10, 
        public ActionResult GetFriendsPosts(int pageNo = 1, int pageSize = userPostsPerPage, 
                         string id = "", int tagid= 0, int categoryid = 0, int groupid = 0)
        {
            var username = id;
           // var visitor = await GetCurrentUserAsync();
            var visitorUserName = User.Identity.GetUserName();
            if (pageSize == 0) pageSize = userPostsPerPage;
           // var visitorUserName = visitor == null ? "" : visitor.UserName;

            //My friends posts or not my friends posts
            long queryType = visitorUserName == username ?
                    (long)FriendType.FriendsPosts | (long)FriendType.MySelf : (long)FriendType.FriendsPosts;
            //set filters
            if (tagid != 0) queryType |= (long)FriendType.ByTag;
            if (categoryid != 0) queryType |= (long)FriendType.ByCategory;

            var mv = _blogRepository.GetPosts1(pageNo, pageSize, categoryid, tagid, username, queryType, groupid, 0, visitorUserName);
                 
            if (mv.Posts == null)
                return View("_LJErrors", " This blogger was not fond");

            mv.UserPanelInfo = GetUserPanelInfo(visitorUserName, mv.User.UserName, mv.User.DefaultUserImage.Id);
            mv.UserPanelInfo.FriendType |= queryType;
            mv.UserPanelInfo.FriendGroups = mv.User.FriendGroups;
            mv.UserPanelInfo.PostsPerPage = pageSize;
            
          
            ViewBag.Title = "Friends Posts";
            return View("Posts", mv);
        }
        

        //Top Posts
        //==============================================
        [AllowAnonymous]
        [VisitorActionFilter(ActionType = (int)ActionType.SeeWebsite)]
        public ActionResult GetRecentPosts(
            int pageNo = 1, int pageSize = topPostsPerPage, int categoryid = 0, int tagid = 0, string username = "")
        {

            long queryType = (long)FriendType.TopPosts;
            if (tagid != 0) queryType |= (long)FriendType.ByTag;
            if (categoryid != 0) queryType |= (long)FriendType.ByCategory;
            if (pageSize == 0) pageSize = topPostsPerPage;
            var mv = _blogRepository.GetPosts1(pageNo, pageSize, categoryid, tagid, username, (long)FriendType.TopPosts, 0, 0);


            string categoryMsg = ((queryType & (long)FriendType.ByCategory) > 0) ? " by category " + mv.Category.Name : "";
            string tagMsg = ((queryType & (long)FriendType.ByTag) > 0) ? " by tag " + mv.Tag.Name : "";

            mv.UserPanelInfo = new UserPanelInfo
            {
                MsgForUserType = "",

                FriendType = queryType
            };

            mv.ModelViewSideBar.UserPanelInfo = mv.UserPanelInfo;
            ViewBag.Title = "Recent posts";

            return View("Posts", mv);
        }

        
        //Top Posts
        //==============================================
        [AllowAnonymous]
        [VisitorActionFilter(ActionType = (int)ActionType.SeeWebsite)]
        public ActionResult GetTopPosts(
            int pageNo = 1, int pageSize = topPostsPerPage, int categoryid = 0, int tagid = 0, string username = "")
        {
            
            long queryType = (long)FriendType.TopPosts;
            if (tagid != 0) queryType |= (long)FriendType.ByTag;
            if (categoryid != 0) queryType |= (long)FriendType.ByCategory;
            if (pageSize == 0) pageSize = topPostsPerPage;
            var mv = _blogRepository.GetTopPosts1(pageNo, pageSize, categoryid, tagid,  (long)FriendType.TopPosts);
            string categoryMsg = ((queryType & (long)FriendType.ByCategory) > 0) ? " by category " + mv.Category.Name : "";
            string tagMsg = ((queryType & (long)FriendType.ByTag) > 0) ? " by tag " + mv.Tag.Name : "";
            mv.UserPanelInfo = new UserPanelInfo {
                MsgForUserType = "",
                FriendType = queryType
            };

            mv.ModelViewSideBar.UserPanelInfo = mv.UserPanelInfo;
            ViewBag.Title = "Top posts";
            return View("TopPosts", mv);
        }



        //==============================================
        [NonAction]
        public UserPanelInfo GetUserPanelInfo(string visitorUserName, string userName, int defaultUserImageId = 1)
        {
            string msgForUserType = "";
            int friendType = 0;
            if (visitorUserName == "")
            {
                friendType = (int)FriendType.Anonymous;
                msgForUserType = "";
            }
            else
            {
                if (visitorUserName == userName)
                {
                    friendType = (int)FriendType.MySelf;
                }
                else
                {
                    friendType = _blogRepository.GetUserType(visitorUserName, userName);
                    if (friendType == 0) { friendType = (int)FriendType.NoConnection; }
                }

                if (!ApplicationUser.MessageForFriendType.TryGetValue(friendType, out msgForUserType))
                {
                    msgForUserType = "";
                }

            }

            var info = new UserPanelInfo(userName, friendType, msgForUserType);
            info.UserName = userName;
            info.VisitorUserName = visitorUserName;
            info.DefaultUserImageId = defaultUserImageId;
            info.FriendType = friendType ;
            info.MsgForUserType = msgForUserType;
            info.FriendGroups = visitorUserName == "" ? null : _blogRepository.GetUser(visitorUserName).FriendGroups;
            return info;
        }



        // Sidebars
        //==============================================
        [ChildActionOnly]
        public PartialViewResult GetSidebars(UserPanelInfo userPanelInfo, int categoryid = 0, int tagid = 0 )
        {
            ModelViewSideBar sb;
            long friendType = userPanelInfo.FriendType;
            sb = _blogRepository.GetSideBarByUser(userPanelInfo.UserName, categoryid, tagid, true);
            sb.UserPanelInfo = userPanelInfo;
            sb.UserPanelInfo.FriendType |= (long)FriendType.UserPosts;
            return PartialView("_Sidebars", sb);
        }
        //==============================================
        [HttpGet]
        [VisitorActionFilter(ActionType = (int)ActionType.EditBlog)]
        public ViewResult EditPost(int id = 0)
        {
            var username = User.Identity.GetUserName();
            if (username == "") return View("Error");
            Post post = id== 0 ? new Post() : _blogRepository.GetPostById(id);
            var mv = new EditPostViewModel();
            mv.Post = post;
            mv.Categories = _blogRepository.GetCategories();
            mv.Tags = _blogRepository.GetTags();
            mv.PostTagsCount = post.Tags == null ? 0 :post.Tags.Count();           
            mv.FriendGroups = _blogRepository.GetUser(username).FriendGroups;
            if (post == null)
                throw new HttpException(404, "Post not found");
            if (post.Published == false && User.Identity.IsAuthenticated == false)
                throw new HttpException(401, "The post is not published");
            return View("EditPost", mv);
        }
        //==============================================
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [VisitorActionFilter(ActionType = (int)ActionType.EditBlog)]
        public async Task<ActionResult> EditPost(Post Post)
        {
            var post = Post;
            var user = await GetCurrentUserAsync();           
            post.ApplicationUser = user;
            if (user == null) RedirectToRoute("Login", "Account");
            int id = _blogRepository.SavePost(post);
            return RedirectToAction("GetUserPost", "Blog", new {id = id});
        }

        //==============================================
    }
}


