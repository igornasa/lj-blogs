﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LJ.Repository.BlogRepository;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.ViewModels.User;
using LJ.ViewModels;
using LJ.Filters;
using LJ.Models.Visitor;

namespace LJ.Controllers
{
    public class HomeController : Controller
    {
         private IBlogRepository<Post,int> _blogRepository;
         private static int UsersPerPage = 20;
         
         public HomeController(IBlogRepository<Post, int> blogRepository)
        {
            _blogRepository = blogRepository;
           

        }
         [VisitorActionFilter(ActionType = (int)ActionType.SeeWebsite)]
        public ActionResult Index()
        {
            return View();
        }

       [VisitorActionFilter(ActionType = (int)ActionType.SeeWebsite)]
        public ActionResult Help()
        {
            return View();
        }

        [VisitorActionFilter(ActionType = (int)ActionType.SeeWebsite)]
        public ActionResult About()
        {
            ViewBag.Message = "About";

            return View();
        }

         [VisitorActionFilter(ActionType = (int)ActionType.SeeWebsite)]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        
    }
}