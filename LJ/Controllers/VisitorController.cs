﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System.Globalization;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Net;
using LJ.Infrastructure;
using LJ.Models.Account;
using LJ.Models.Visitor;
using LJ.Models.User;
using LJ.Models.Blog;
using LJ.ViewModels.Visitor;
using LJ.Repository.VisitorRepository;
using LJ.Repository.BlogRepository;
using LJ.ViewModels.User;
using LJ.Filters;



namespace LJ.Controllers
{
    [AllowAnonymous]
    public class VisitorController : Controller
    {       
        private IVisitorRepository<ApplicationVisitor,int> _repository;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public VisitorController(IVisitorRepository<ApplicationVisitor, int> repository)
        {
            _repository = repository;
        }
        public VisitorController(IVisitorRepository<ApplicationVisitor, int> repository, ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            _repository = repository;
            UserManager = userManager;
            SignInManager = signInManager;
        }

        private async Task<ApplicationUser> GetCurrentUserAsync()
        {
            return await UserManager.FindByIdAsync(User.Identity.GetUserId());
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        
        
       


        //=====================================================      
        public ActionResult WhoIsOnline()
        {
            
                var visitors = OnlineVisitorsContainer.Visitors == null || OnlineVisitorsContainer.Visitors.Count() == 0 ?
                             0 : OnlineVisitorsContainer.Visitors.Count();
                return View(visitors);
            
        }

        
        //==================================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.WebsiteReport)]
        public JsonResult GetOnlineVisitorsMap()
        { 
            if (OnlineVisitorsContainer.Visitors != null)
            {

                var visitors = OnlineVisitorsContainer.Visitors;
                             
                visitors.Values.Where(v => v.AuthUser == null  || v.AuthUser == "").Count();
                var users = visitors.Values.Where(v => v.AuthUser != null && v.AuthUser != "")
                             .GroupBy(v => v.AuthUser, v => v)
                             .Select(h => new
                             {
                                 username = h.Key,                               
                                 city = h.FirstOrDefault().GeoLocation.city,
                                 country_name = h.FirstOrDefault().GeoLocation.country_name,
                                 lat = h.FirstOrDefault().GeoLocation.latitude,
                                 lon = h.FirstOrDefault().GeoLocation.longitude
                             }).OrderBy(h => h.username);

                var ananimouses = visitors.Values.Where(v => v.AuthUser == null  || v.AuthUser == "")
                                  .Select(h => new {
                                      username = "Anonymous",
                                      city = h.GeoLocation.city,
                                      country_name = h.GeoLocation.country_name,
                                      lat = h.GeoLocation.latitude,
                                      lon = h.GeoLocation.longitude
                                  }).OrderBy(h => h.country_name);
                var result = users.Union(ananimouses);

                var data = result.Select((d, index) => new { 
                    name = index, 
                    lat = d.lat, 
                    lon = d.lon }).ToArray();

                var info = result.Select((d, index) => new { 
                    index = index,
                    city = d.city,
                    country_name = d.country_name, 
                    username = d.username }).ToArray();

                return Json(new
                {
                    data = data,
                    info = info,
                    usersCount = users.Count(),
                    ananimousesCount = ananimouses.Count()  
                }
                    , JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { data = new { } }, JsonRequestBehavior.AllowGet); 
        }

        //====================================================

        public ActionResult WhoIsOnlineByCountries()
        {
            var visitors = OnlineVisitorsContainer.Visitors == null || OnlineVisitorsContainer.Visitors.Count() == 0 ?
                         0 : OnlineVisitorsContainer.Visitors.Count();
            return View(visitors);
        }



        //======================================================= 
        [AcceptVerbs(HttpVerbs.Get)]
        [VisitorActionFilter(ActionType = (int)ActionType.WebsiteReport)]
        public JsonResult GetOnlineVisitorsByCountriesMap()
        {
            if (OnlineVisitorsContainer.Visitors != null)
            {

                var visitors = OnlineVisitorsContainer.Visitors;

                var users = visitors.Values
                             .Select(v => new
                             {
                                 AuthUser = (v.AuthUser != null && v.AuthUser != "") ? v.AuthUser : v.Anonymous,
                                 IsUser = (v.AuthUser != null && v.AuthUser != ""),
                                 country_code = v.GeoLocation.country_code,
                                 country_name = v.GeoLocation.country_name
                             })
                            .GroupBy(k => k.country_code, k => k)
                            .Select(h => new
                            {
                                code = h.Key,
                                name = h.FirstOrDefault().country_name,
                                usercount = h.Where(r => r.IsUser)
                                      .GroupBy(u => u.AuthUser, u => u.AuthUser).Select(k => k.Key).Count(),
                                ananimouscount = h.Where( r => !r.IsUser)
                                      .GroupBy( u => u.AuthUser , u => u.AuthUser).Select(k => k.Key).Count(),
                            });

                var result = users.Select(c => new
                {
                    code = c.code,
                    name = c.name,
                    usercount = c.usercount,
                    ananimouscount = c.ananimouscount,
                    value = c.usercount + c.ananimouscount
                }).ToArray();


                return Json(new { data = result }, JsonRequestBehavior.AllowGet);
               
            }
            return Json(new { data = new {} }, JsonRequestBehavior.AllowGet);
        }


        //=====================================================
        public ActionResult GetVisitsStatistics()
        {
            var date = _repository.GetHistoryVisitorsStartDate();
            return View(date);

        }


        //=====================================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.WebsiteReport)]
        public JsonResult GetVisitorsForMonthChart(int month = 0, int year = 0)
        {
           
            var date = DateTime.Now;
            month = month == 0 || month > 12 ? date.Month : month;
            year = year == 0 ? date.Year : year;

            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-1);

            IEnumerable<PostStatisticsViewModel> visits = _repository.GetVisitsForMonth(date1, date2);
            var days = Enumerable.Range(1, DateTime.DaysInMonth(year, month)).ToList();

            var result = days.GroupJoin(visits,
                                         d => d, v => v.Day,
                                         (d, v) => new 
                                         {
                                             Day = d,
                                             Count1 = v.Count() == 0 ? 0 : v.FirstOrDefault().Count1,
                                             Count2 = v.Count() == 0 ? 0 : v.FirstOrDefault().Count2,
                                             Count3 = v.Count() == 0 ? 0 : v.FirstOrDefault().Count3,
                                         }).OrderBy(d => d.Day).ToList();
                                            
              
            return Json(new { data = new {
                                            visits = result.Select(v => v.Count1).ToArray(),
                                            users = result.Select(v => v.Count2).ToArray(),
                                            ananimuses = result.Select(v => v.Count3).ToArray(),
                                            visitors = result.Select(v => v.Count3 + v.Count2).ToArray(),
                                            days = days.ToArray(),
                                            month = month,
                                            year = year,
                                        } 
            }, JsonRequestBehavior.AllowGet);
        }

       
        //====================================================
        public ActionResult GetHistoryVisitorsByCountry()
        {
            var date = _repository.GetHistoryVisitorsStartDate();
            return View(date);
        }


        //======================================================= 
        [AcceptVerbs(HttpVerbs.Get)]
        [VisitorActionFilter(ActionType = (int)ActionType.WebsiteReport)]
        public JsonResult GetHistoryVisitorsMap(int month = 0, int year = 0)
        {
           
            var date = DateTime.Now;
            month = month == 0 || month > 12 ? date.Month : month;
            year = year == 0 ? date.Year : year;

            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-1);

            var visitors =  _repository.GetVisitsForMonthByCountry( date1,  date2);
            var result = visitors.Select(c => new
                {
                    code = c.Id2,//country code
                    name = c.Title, // country name
                    value = c.Count1, //visits
                    visitors = c.Count2 + c.Count3,
                    usercount = c.Count2,
                    ananimouscount = c.Count3,
                    
                }).ToArray();

             return Json(new { data = result , month = month, year = year}, JsonRequestBehavior.AllowGet);
        }
        
        
       
        //=====================================================
        public static void JobVisitors(int month = 0, int year = 0)
        {         
            var date = DateTime.Now;
            month = month == 0 || month > 12 ? date.Month : month;
            year = year == 0 ? date.Year : year;

            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-1);

          IVisitorRepository<ApplicationVisitor, int> repository = new VisitorRepository(new BlogDbContext());
          var e= repository.AddVisitorsToArxiv(date1, date2);
        }


        //=====================================================
        public static void JobSaveVisitorActionLog(VisitorActionLog log)
        {
            var db = new BlogDbContext();
            IVisitorRepository<ApplicationVisitor, int> repository = new VisitorRepository(db);
            var e = repository.SaveVisitorActionLog(log);
        }


        //===================================================
        public static string CRON_STRING_VisitorLogsToArxives = "0 0 * * *";
        public static int JobVisitorLogsToArxives_OffsetTime = 2;

        public static void JobAddVisitorLogsToArxives()
        {
            var  endDate = DateTime.Now.AddHours(-JobVisitorLogsToArxives_OffsetTime);
            var startDate = endDate.AddDays(-1).AddHours(-3);
            var db = new BlogDbContext();
            IVisitorRepository<ApplicationVisitor, int> repository = new VisitorRepository(db);
            var e = repository.AddVisitorsToArxiv(startDate, endDate);
            repository.JobAddVisitorLogsToArxives();
           

        }
        //=============================================
        public static int JOB_INTERVAL_RATING = 12;
        public static string CRON_STRING_Rating = "0 10,22 * * *";
        
        public static void JobRatings()
        {
            var date = DateTime.Now;
            CalculateRatings(date, true);
        }

        //=============================================
         public static void  CalculateRatings(DateTime date, bool isTest, int jobType = (int)JobType.HingFireJob)
        {
          var startDate = date.AddHours(-JOB_INTERVAL_RATING);
          var db = new BlogDbContext();
          IVisitorRepository<ApplicationVisitor, int> repository = new VisitorRepository(db);
          var e = repository.CalculateRatings(startDate, date, jobType);
          if (isTest)
          {
              repository.AddCommentsForJob();
          }

        }

         //=============================================
        public static void  CalculateRatingsForSeed()
        {
            var currentDateTime = DateTime.Now.AddHours(-6);
            var currentHour = currentDateTime.Hour;

            if (currentHour > 23)
                currentHour = 23;
            else if (currentHour > 11)
                currentHour = 11;
            else
            {
                currentDateTime = currentDateTime.AddDays(-1);
                currentHour = 23;
            }

            var date1 = new DateTime(currentDateTime.Year, currentDateTime.Month, 1, currentHour, 0, 0);
            
            var days = 10;
            var times = days * (24 / JOB_INTERVAL_RATING);

            for (int i = times; i >= 0; i--)
            {
                var d1 = currentDateTime.AddHours(-(JOB_INTERVAL_RATING * i));

                LJ.Controllers.VisitorController.CalculateRatings(d1, false, (int)JobType.CustomJob);
            }

        }

        //=============================================
         [VisitorActionFilter(ActionType = (int)ActionType.AdminReport)]
        public ActionResult GetAllVisitorsList()
        {
            var visitors = _repository.GetAllVisitorsList();
            return View(visitors);
        }


         //=============================================
         [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public ActionResult GetVisitedPagesForUser(string username)
        {
            var actions = _repository.GetVisitorActionsForUser(username);
            return PartialView(actions);
        }

        //----------------------------------------------------
         [VisitorActionFilter(ActionType = (int)ActionType.AdminReport)]
            public ActionResult GetVisitedPagesForAdmin()
            {
                var actions = _repository.GetVisitorActionsForAdmin();
                return View(actions);

            }



         //=============================================
        [VisitorActionFilter(ActionType = (int)ActionType.AdminReport)]
            public ActionResult GetExitPagesForAdmin()
            {
                var actions = _repository.GetExitPagesForAdmin();
                return View(actions);
            }



        //=============================================
           [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
            public ActionResult GetExitPagesForUser(string id)
            {
                var username = id;
                var actions = _repository.GetExitPagesForUser(username);
                return View(actions);

            }


         //=============================================
        [VisitorActionFilter(ActionType = (int)ActionType.AdminReport)]
            public ActionResult GetBouncePagesForAdmin()
            {
                var actions = _repository.GetBouncePagesForAdmin();
                return View(actions);
            }



        //=============================================
            [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
            public ActionResult GetBouncePagesForUser(string id)
            {
                var username = id;
                var actions = _repository.GetBouncePagesForUser(username);
                return View(actions);

            }
       
       
        
    }
}