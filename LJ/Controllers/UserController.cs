﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System.Globalization;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using LJ.ViewModels.User;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.Models.Visitor;
using System.Net;
using LJ.Repository.UserRepository;
using LJ.Infrastructure;
using LJ.Models.Account;
using LJ.Filters;

namespace LJ.Controllers
{
    [AllowAnonymous]
    public class UserController : Controller
    {

        private IUserRepository<ApplicationUser,int> _repository;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public UserController(IUserRepository<ApplicationUser, int> repository)
        {
            _repository = repository;
           

        }
        public UserController(IUserRepository<ApplicationUser, int> repository, ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            _repository = repository;
            UserManager = userManager;
            SignInManager = signInManager;

        }

        private async Task<ApplicationUser> GetCurrentUserAsync()
        {
            return await UserManager.FindByIdAsync(User.Identity.GetUserId());
        }


        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        


        // from http://www.codeproject.com/Articles/379980/Fancy-ASP-NET-MVC-Image-Uploader
        //===========================
        public ActionResult Index(int id)
        {
            return PartialView(id);

        }


        [HttpGet]
        public ActionResult UploadImage()
        {
            return View();
        }



        [HttpPost]
        public ActionResult UploadImage(ImageCropForDisplyViewModel model)
        {
            if (ModelState.IsValid)
            {
                Bitmap original = null;
                var name = "newimagefile";
                var errorField = string.Empty;

                if (model.IsUrl)
                {
                    errorField = "Url";
                    name = GetUrlFileName(model.Url);
                    original = GetImageFromUrl(model.Url);
                }
                else if (model.IsFlickr)
                {
                    errorField = "Flickr";
                    name = GetUrlFileName(model.Flickr);
                    original = GetImageFromUrl(model.Flickr);
                }
                else 
                {
                    errorField = "File";
                    name = Path.GetFileNameWithoutExtension(model.File.FileName);
                    original = Bitmap.FromStream(model.File.InputStream) as Bitmap;
                }

                if (original != null)
                {
                    var img = CreateImage(original, model.X, model.Y, model.Width, model.Height);
                    var imgmodel = new ApplicationUserImage
                        {
                            ImageMimeType = model.File.ContentType,
                            ImageData = ImageToByte(img)
                        };
                    int i =_repository.SaveImage(imgmodel);
                    return RedirectToAction("Index", new { id = i });
                }
                else
                    ModelState.AddModelError(errorField, "Your upload did not seem valid. Please try again using only correct images!");
            }

            return View(model);
        }

        //============================
         [NonAction]
        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }



        //=============================
        [NonAction]
         public string GetUrlFileName(string url)
        {
            var parts = url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var last = parts[parts.Length - 1];
            return Path.GetFileNameWithoutExtension(last);
        }



        //===========================
        [NonAction]
        Bitmap GetImageFromUrl(string url)
        {
            var buffer = 1024;
            Bitmap image = null;

            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                return image;

            using (var ms = new MemoryStream())
            {
                var req = WebRequest.Create(url);

                using (var resp = req.GetResponse())
                {
                    using (var stream = resp.GetResponseStream())
                    {
                        var bytes = new byte[buffer];
                        var n = 0;

                        while ((n = stream.Read(bytes, 0, buffer)) != 0)
                            ms.Write(bytes, 0, n);
                    }
                }

                image = Bitmap.FromStream(ms) as Bitmap;
            }

            return image;
        }



        //===================================
        Bitmap CreateImage(Bitmap original, int x, int y, int width, int height)
        {
            var img = new Bitmap(width, height);

            using (var g = Graphics.FromImage(img))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(original, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
            }

            return img;
        }



        //=====================================
        [AllowAnonymous]
        public FileContentResult GetImage(int id)
        {
            ApplicationUserImage f = _repository.GetImageBy(id);
            if (f == null || f.ImageData == null) return null;
            return File(f.ImageData, f.ImageMimeType);

        }

        // User Profile
        //===========================
        [HttpGet]          
        [VisitorActionFilter(ActionType = (int)ActionType.SeeProfile, UserName = "id")]
        public ActionResult GetProfile(string id)
        {  //id ==username
            var profile = _repository.GetProfileBy(id);
            if (profile == null) return View("Error", "No this blogger");
      
            return View(profile);
        }

        
        //====================================
        [HttpGet]
        [VisitorActionFilter(ActionType = (int)ActionType.EditProfile)]
        public ActionResult EditProfile(string id)
        {
            var username = id;
            UserViewModel user;
            if (username == "") user = new UserViewModel();
            else
            {
                var userFromDb = _repository.GetUserBy(username);
                if (userFromDb == null)
                    return View("Error", "No this blogger");
                else
                    user = new UserViewModel(userFromDb);
            }
            return View(user);
        }


        //==================================================
       [HttpPost]
       [ValidateAntiForgeryToken]
       [ValidateInput(false)]
        public ActionResult SaveSectionUserpics(UserViewModel user, int DefaultIdOrder,
           IEnumerable<ImageViewModel> UserImages, 
           IEnumerable<ImageViewModel> UploadedUserImages)
        {
            string username = user.UserName;
            int result = 1;
            List<ApplicationUserImage> imagesToAdd = new List<ApplicationUserImage>();
            var errorField = string.Empty;
            
            if (ModelState.IsValid)
            {           
                //If have images
                if (user.UserImages != null )
                {
                    // set order number for all images
                    var images = user.UserImages
                        .Select((i, index) => new { OrderId = index + 1, Image = i }).ToList();  
                
                    //if have new images
                    if (user.UploadedUserImages != null) {
                        // set orderid for uploaded images (new) images
                        var newImages = user.UploadedUserImages
                             .Join(images.Where(a => a.Image.Id == 0),
                                           i1 => i1.NewImageId,
                                           i2 => i2.Image.NewImageId,
                                          (i1, i2) => new { OrderId = i2.OrderId, NewImage = i1 }).ToList();

                        //create new Images;
                        foreach (var img in newImages)
                        {
                            Bitmap originalImage = null;
                            var newImage = (UploadedImageViewModel)img.NewImage;
                            var uploadType = newImage.ImageUploadTypeId & (int)ImageUploadType.IsFileFlickrUrl;
                            string contentType;
                         
                            switch (uploadType)
                            {
                                case (int)ImageUploadType.Url:
                                    errorField = "Url";
                                    originalImage = GetImageFromUrl(newImage.Url);
                                   
                                    break;
                                case (int)ImageUploadType.Flickr:
                                    errorField = "Flickr";
                                    originalImage = GetImageFromUrl(newImage.Url);
                                    
                                    break;
                                case (int)ImageUploadType.File:
                                    errorField = "File";
                                    originalImage = Bitmap.FromStream(newImage.File.InputStream) as Bitmap;
                                    contentType = newImage.File.ContentType;
                                    break;
                                default:
                                    break;
                            }

                            var imgFromBitMap = CreateImage(originalImage, newImage.X, newImage.Y, newImage.Width, newImage.Height);
                            var imageToAdd = new ApplicationUserImage
                            {
                                Id = img.NewImage.Id,
                                ImageMimeType = "png",
                                ImageData = ImageToByte(imgFromBitMap),
                                ImageOrderId = img.OrderId
                            };
                            imagesToAdd.Add(imageToAdd);
                    }                    
                  }

                 var updatedImages = images
                        .Where(i => i.Image.Id > 0)
                        .Select(k => new ApplicationUserImage { Id = k.Image.Id, ImageOrderId = k.OrderId });
                 result = _repository.SaveSectionUserpics(imagesToAdd, updatedImages, user, DefaultIdOrder);
                   
            }

                if (result == 0)
                    return View("Error", "something wrong at db");
                else
                {
                    
                    return RedirectToAction("EditProfile", new { id = username });
                }
            }
           
          else
          {
              ModelState.AddModelError(errorField, "Your upload did not seem valid. Please try again using only correct images!");
                  return View(user);
          }           
      }





        //======================================================
        [HttpPost]
    //   [ValidateAntiForgeryToken]
       [ValidateInput(false)]
       public ActionResult SaveSectionAbout(string UserName, string About)
        {
            var result = _repository.SaveSectionAbout(UserName, About);
            if (result > 0)
            return RedirectToAction("EditProfile", new { id = UserName });
            else
            {
               
                return RedirectToAction("EditProfile", new { id = UserName });
            }    
        }    
    
        //================================================
        [HttpPost]
       [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SaveSectionInfo( UserViewModel user)
        {
            var info = user.ToApplicationUserInfo(user); 

            int id = user.DefaultUserImageId;
            var result = _repository.SaveSectionInfo(info, user.UserName, id);
            if (result > 0)
                return RedirectToAction("EditProfile", new { id = user.UserName });
            else
            {
               
                return RedirectToAction("EditProfile", new { username = user.UserName });
            }
        }    
    

        //============================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.ManageProfile)]
        public ActionResult DeleteFriend(string username)
        {
            string name = User.Identity.GetUserName();
            
          // var user = await GetCurrentUserAsync();
           // if (user == null)  user="Mar//return 0;

            var result = _repository.DeleteFriend(username, name);//user.UserName);
            return Json(new { result = result }, JsonRequestBehavior.AllowGet);
            
        }


        //======================================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.ManageProfile)]
        public ActionResult AddFriend(string username)
        {
            string name = User.Identity.GetUserName();
            var result = _repository.AddFriend(username, name);
            return Json(new { result = result }, JsonRequestBehavior.AllowGet);

        }


        //======================================================        
         [VisitorActionFilter(ActionType = (int)ActionType.WebsiteReport)]
        public ActionResult GetCurrentTopUsers(int id = 1, int perpage = UserViewModelShort.UsersPerPage)
        {
            var pageNumber = id;
          
            var result = _repository.GetUsersByCurrentTopRating(perpage, pageNumber);

            ViewBag.Message = "Today Top Blogs";
            ViewBag.pageNumber = pageNumber;
            ViewBag.perpage = perpage;
            ViewBag.usersCount = result.TopUsersCount;

            return View("GetCurrentTopUsers", result.UserRatings);
        }



         //=======================================================
         [VisitorActionFilter(ActionType = (int)ActionType.WebsiteReport)]
        public ActionResult GetAverageTopUsers(int id = 1, int perpage = UserViewModelShort.UsersPerPage)
        {
            var pageNumber = id;
            var result = _repository.GetUsersByAverageTopRating(perpage, pageNumber);
          

            ViewBag.Message = "Top Blogs";
            ViewBag.pageNumber = pageNumber;
            ViewBag.perpage = perpage;
            ViewBag.usersCount = result.TopUsersCount;

            return View("GetAverageTopUsers", result.UserRatings);
        }



        //=======================================================
        public ActionResult GetUserPanel(string visitorUserName, string userName, int defaultUserImageId = 1)
        {
            
            string msgForUserType = "";
            int friendType = 0;


            if (visitorUserName == "")
            {
                friendType = (int)FriendType.Anonymous;
                msgForUserType = "";
            }
            else
            {
                if (visitorUserName == userName)
                {
                    friendType = (int)FriendType.MySelf;
                }
                else
                {
                    friendType = _repository.GetUserType(visitorUserName, userName);
                    if (friendType == 0) { friendType = (int)FriendType.NoConnection; }

                }

                if (!ApplicationUser.MessageForFriendType.TryGetValue(friendType, out msgForUserType))
                {
                    msgForUserType = "";
                }
            }

            var info = new UserPanelInfo(userName, friendType, msgForUserType);
            info.UserName = userName;
            info.VisitorUserName = visitorUserName;
            info.DefaultUserImageId = defaultUserImageId;
            info.FriendType = friendType;
            info.MsgForUserType = msgForUserType;
            return PartialView("_UserPanel", info);
        }



        //========================================================
        [ChildActionOnly]
        public ActionResult GetUsersForSideBar()
        {
            var users = _repository.GetUsers(16, 1).ToList();            
            return PartialView("_GetUsers", users);
        }



        //========================================================
        [ChildActionOnly]
        public ActionResult GetUsersWithLogins()
        {
            var users = _repository.GetUsers(16, 1).ToList();
            ViewBag.AdminId = _repository.GetRoleId("Admin");
            return PartialView("_GetUsersLogins", users);
        }



        //==================================================
        [VisitorActionFilter(ActionType = (int)ActionType.SeeWebsite)]
        public ActionResult SignInDemo(string id)
        {
            var username = id;
            var user = _repository.GetUserByName(username);
            if (user == null)
                return RedirectToAction("Login", "Account");

            var login = new LoginViewModel{
                Email= user.Email, 
                Password= user.password2,
                RememberMe=false
            };
            return PartialView(login);
            
        }


       //=====================================================
        [VisitorActionFilter(ActionType = (int)ActionType.EditProfile)]
        public ActionResult ManageFriends(string id)
        {
            var username = id;

            var vm = _repository.GetManageFriends(username);
            if (vm.User == null)
                return RedirectToAction("Login", "Account");
            return View( vm);
            
        }


       //=====================================================
        [VisitorActionFilter(ActionType = (int)ActionType.EditProfile)]
        public ActionResult ManageFriendsGroups(string id)
        {
            var username = id;
            ApplicationUser user = _repository.GetManageFriendsGroups(username);
            if (user == null)
                return RedirectToAction("Login", "Account");
            return View(user);

        }



        //=====================================================
         [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
         public ActionResult GetFriendsStatistics(string id, int month = 0, int year = 0)
        {
            var username = id;
            
            StatisticsByFriendsViewModel user = _repository.GetFriendsStatistics(username);
            if (user == null)
                return RedirectToAction("Login", "Account");
            return View(user);
        }
 
        //======================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public ActionResult GetFriendsChart(string id,int month = 0, int year = 0)
        {
            var username = id;
            var date = DateTime.Now;
            month = month == 0 || month >12 ? date.Month : month;
            year = year == 0 ? date.Year : year;

            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-1);
            StatisticsByFriendsViewModel user = _repository.GetFriendsStatistics(username);

            if (user == null)
                return RedirectToAction("Login", "Account");

            var days = Enumerable.Range(1, DateTime.DaysInMonth(year, month)).ToList();

            //---FriendsWithArxiv
            var friendsWithArxiv = user.User.FriendsWithArxiv.Where(u => u.Date >= date1 && u.Date <= date2).ToList();
            var usersByDays = days.GroupJoin(friendsWithArxiv,
                                       d => d, u => u.Date.Day, 
                                       (d, u) => new {

                                           Day = d,  

                                           AddCount = u
                                              .Where(h => h.Date.Day == d && h.Action == (short)FriendAction.Added).Count(),

                                           DeleteCount = u
                                               .Where(h => h.Date.Day == d && h.Action == (short)FriendAction.Deleted).Count(),

                                           AddNames = u
                                              .Where(h => h.Date.Day == d && h.Action == (short)FriendAction.Added)
                                              .GroupBy(z => z.Friend.UserName, z => z.Friend.UserName)
                                              .Select(k => k.Key + (k.Count() > 1 ? "(" + k.Count() + ")" : ""))
                                              .OrderBy(w => w)
                                              .ToArray(),

                                           DeleteNames = u
                                              .Where(h => h.Date.Day == d && h.Action == (short)FriendAction.Deleted)
                                               .GroupBy(z => z.Friend.UserName, z => z.Friend.UserName)
                                               .Select(k => k.Key + (k.Count() > 1 ?  "(" + k.Count() +")": ""))
                                               .OrderBy(w => w)
                                              .ToArray()

                                       })
                                       .OrderBy( k => k.Day).ToList();
            
            
            var names2 = new string[2] { "You have added to your friends list", "You have deleted from your friends list" };
            var series2 = names2.Select((n, index) => new
            {
                id = "series1-" + (index == 0 ? "Add" :"Delete"),
                name = n,
                data = index == 0 ? usersByDays.Select(k => k.AddCount).ToArray() :
                                            usersByDays.Select(k => -k.DeleteCount).ToArray() 
            }).ToArray();
                            
            var arrnames2 = usersByDays
                          .Select(k => new
                          {
                              Add = k.AddCount == 0 ? "" : String.Join(", ", k.AddNames.ToArray()),
                              Delete = k.DeleteCount == 0 ? "" : String.Join(", ", k.DeleteNames.ToArray())
                          }).ToArray();
          
            //---FriendsOfArxiv
            var friendsOfArxiv = user.FriendsOfArxiv.Where(u => u.Date >= date1 && u.Date <= date2).ToList();

            var friendsOfArxivByDays = days.GroupJoin(friendsOfArxiv,
                                       d => d, u => u.Date.Day,
                                       (d, u) => new
                                       {
                                           Day = d,
                                           AddCount = u
                                              .Where(h => h.Date.Day == d && h.Action == (short)FriendAction.Added).Count(),
                                           DeleteCount = u
                                               .Where(h => h.Date.Day == d && h.Action == (short)FriendAction.Deleted).Count(),

                                           AddNames = u
                                              .Where(h => h.Date.Day == d && h.Action == (short)FriendAction.Added)
                                              .GroupBy(z => z.User.UserName, z => z.User.UserName)
                                              .Select(k => k.Key + (k.Count() > 1 ? "(" + k.Count() + ")" : ""))
                                              .OrderBy(w => w)
                                              .ToArray(),

                                           DeleteNames = u
                                              .Where(h => h.Date.Day == d && h.Action == (short)FriendAction.Deleted)
                                               .GroupBy(z => z.User.UserName, z => z.User.UserName)
                                               .Select(k => k.Key + (k.Count() > 1 ? "(" + k.Count() + ")" : ""))
                                               .OrderBy(w => w)
                                              .ToArray()

                                       })
                                       .OrderBy(k => k.Day).ToList();

            var names1 = new string[2] { "You have been added to friends lists", "You have been deleted from friends lists" };
            var series1 = names1.Select((n, index) => new
            {
                id = "series2-" + (index == 0 ? "Add" : "Delete"),
                name = n,
                data = index == 0 ? friendsOfArxivByDays.Select(k => k.AddCount).ToArray() :
                                            usersByDays.Select(k => -k.DeleteCount).ToArray()
            }).ToArray();

            var arrnames1 = friendsOfArxivByDays
                          .Select(k => new
                          {
                              Add = k.AddCount == 0 ? "" : String.Join(", ", k.AddNames.ToArray()),
                              Delete = k.DeleteCount == 0 ? "" : String.Join(", ", k.DeleteNames.ToArray())
                          }).ToArray();
            var chart1 = new
            {
                chart = new { type = "column" },
                title = new { text = "Statistics by users actions" },

                xAxis = new
                {
                    gridLineWidth = 1,
                    categories = days.Select(a => a.ToString()).ToArray(),
                    title = new { text = "Days in  " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date2.Month) }
                },

                yAxis = new
                {
                    title = new { text = "Users" },
                    gridLineWidth = 1,
                    tickInterval = 1
                },

                series = series1

            };

            var chart2 = new
            {
                chart = new { type = "column" },
                title = new { text = "Statistics by your actions" },

                xAxis = new
                {
                    gridLineWidth = 1,
                    categories = days,
                    title = new { text = "Days in  " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(date2.Month) }
                },

                yAxis = new
                {
                    title = new { text = "Users" },
                    gridLineWidth = 1,
                    tickInterval = 1
                },

                series = series2

            };

            return Json(new
            {
                chart1 = chart1,
                chart2 = chart2,

                names1 = arrnames1,
                names2 = arrnames2,

                month = month,
                year = year,

            }, JsonRequestBehavior.AllowGet);
        }



        //=========================================
        public ActionResult GetInfoForFrontPage(int usercount, int postcount)
        {

            var users = _repository.GetInfoForFrontPage(usercount, postcount);
            return PartialView(users);
        }
     


        //========================================
      // [VisitorActionFilter(ActionType = (int)ActionType.SeeWebsite)]
        public ActionResult  GetUsersAlfabet()
        {
            var alfabet = _repository.GetUsersAlfabet();
            return View(alfabet);
        }


        //============================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.SeeWebsite)]
        public JsonResult GetUsersByFirstLetter(string id, int perPage = 100, int pageNumber = 1)
        {
            var letter = id;
            var result = _repository.GetUsersByFirstLetter(letter, perPage, pageNumber).ToList();
            var users = result.Select((t, index) => new { 
                    Index = index +1,
                    UserName = t.UserName,
                    DefaultUserImageId = t.DefaultUserImageId,
                    FirstName = t.ApplicationUserInfo.FirstName?? "",
                    LastName =t.ApplicationUserInfo.LastName?? "",
                    Country = t.ApplicationUserInfo.Country?? "",
                    City = t.ApplicationUserInfo.City?? ""
                
                }).ToList();
            return Json(new
            {
                users = users
            },
            JsonRequestBehavior.AllowGet);
        }

        //============================================
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public ActionResult GetUserRatingStatistics(string id)
        {
            var username = id;
            var date = DateTime.Now;
            StatisticsByFriendsViewModel user = _repository.GetFriendsStatistics(username);
            ViewBag.StartDateRange = _repository.GetAllUsersRatingsLastJob();
            if (user == null)
                return RedirectToAction("Login", "Account");
            return View(user);
        }


        //========================================
         [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public ActionResult GetCommentsStatistics(string id)
        {
            var username = id;
            var date = DateTime.Now;
            StatisticsByFriendsViewModel user = _repository.GetFriendsStatistics(username);
            
            if (user == null)
                return RedirectToAction("Login", "Account");
            ViewBag.StartDateRange = user.User.Created;
            return View(user);
        }



        //========================================
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public ActionResult GetPostsStatistics(string id)
        {
            var username = id;
            var date = DateTime.Now;
            StatisticsByFriendsViewModel user = _repository.GetFriendsStatistics(username);
            if (user == null)
                return RedirectToAction("Login", "Account");
            user.StartDateRange = _repository.GetPostCommentsStartDate(user.User.Id);
            return View(user);
        }

         //========================================
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public ActionResult GetVisitorStatisticsForUser(string id)
        {
            var username = id;
            var date = DateTime.Now;
            StatisticsByFriendsViewModel user = _repository.GetFriendsStatistics(username);
            if (user == null)
                return RedirectToAction("Login", "Account");
            return View(user);
        }
       
         //======================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public ActionResult GetCommentsChart(string id,int month = 0, int year = 0)
        {
            var username = id;
            var date = DateTime.Now;
            month = month == 0 || month >12 ? date.Month : month;
            year = year == 0 ? date.Year : year;
            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-1);
            StatisticsByCommentsViewModel vm = _repository.GetCommentsStatistics(username, date1, date2);
            var days = Enumerable.Range(1, DateTime.DaysInMonth(year, month)).ToList();

            // for posted comments
            var reseivedComments = days.GroupJoin(vm.ReceivedComments,
                                     i1 => i1, i2 => i2.Day,
                                     (i1, i2) => new
                                     {
                                         Day = i1,
                                         CommentsCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().Count1,
                                         PostsCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().Count2,
                                         UsersCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().Count3,
                                         YourCommentsCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().Count4,
                                     })
                                    .OrderBy(d => d.Day).ToList();

            var chartdata11 = reseivedComments.Select(c => c.CommentsCount).ToArray(); 
            var chartdata12 = reseivedComments.Select(c => c.PostsCount).ToArray(); 
            var chartdata13 = reseivedComments.Select(c => c.UsersCount).ToArray(); 

            var chartdata21 = reseivedComments.Select(c => c.YourCommentsCount).ToArray(); // your comments
            var chartdata22 = reseivedComments.Select(c => c.CommentsCount - c.YourCommentsCount).ToArray(); // other users comments 

            var chartTotal21 = chartdata21.Sum(c => c);
            var chartTotal22 = chartdata22.Sum(c => c);

            // for posted comments
            var postedComments = days.GroupJoin(vm.PostedComments,
                                     i1 => i1, i2 => i2.Day,
                                     (i1, i2) => new
                                     {
                                         Day = i1,
                                         CommentsCount = i2.Count() == 0 ? 0 : i2.Sum(ss => ss.Count2),
                                         YourPostsCommentCount = i2.Count() == 0 ? 0 : i2.Sum(ss => ss.Count1 - ss.Count2)
                                     })
                                    .OrderBy(d => d.Day).ToList();

            var chartdata41 = postedComments.Select(c => c.CommentsCount).ToArray(); 
            var chartdata42 = postedComments.Select(c => c.YourPostsCommentCount).ToArray(); 
            var chartTotal41 = chartdata41.Sum(c => c);
            var chartTotal42 = chartdata42.Sum(c => c);

            return Json(new
            {
                chartdata11 = chartdata11,
                chartdata12 = chartdata12,
                chartdata13 = chartdata13,

                chartdata21 = chartdata21,
                chartdata22 = chartdata22,
                chartTotal21 = chartTotal21,
                chartTotal22 = chartTotal22,

                chartdata41 = chartdata41,
                chartdata42 = chartdata42,
                chartTotal41 = chartTotal41,
                chartTotal42 = chartTotal42,

                days = days,
                month = month,
                year = year,
            },
            JsonRequestBehavior.AllowGet);
        }



        //=====================================================        
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public ActionResult GetPostsChart(string id, int month = 0, int year = 0)
        {
            var username = id;
            var date = DateTime.Now;
            month = month == 0 || month > 12 ? date.Month : month;
            year = year == 0 ? date.Year : year;

            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-1);

            // ApplicationUser user = null;//_repository.GetCommentsStatistics(username);
            // if (user == null)
            //    return RedirectToAction("Login", "Account");

            StatisticsByCommentsViewModel vm = _repository.GetPostsStatistics(username, date1, date2);
            var comments = vm.Comments;
            var posts = vm.Posts;

            var days = Enumerable.Range(1, DateTime.DaysInMonth(year, month)).ToList();

            // for diagram comments
            var userCommentsByPostId = (comments
                   .GroupBy(cc => cc.PostId, cc => cc.PostId)
                   .Select(gr => new
                   {
                       PostId = gr.Key,
                       y = gr.Count()
                   }))
                   .Join(posts, p1 => p1.PostId, p2 => p2.Id, (p1, p2) => new { name = p2.Title, y = p1.y })
                   .OrderBy(o => o.name).ToArray();

            return Json(new
            {               
                month = month,
                year = year,
                chart2data = userCommentsByPostId,
                chart3data = vm.PostLikes.Where(p => p.Count1 > 0).Select(p => new { name = p.Title, y = p.Count1 }).ToArray(),
                chart4data = vm.PostLikes.Where(p => p.Count2 > 0).Select(p => new { name = p.Title, y = p.Count2 }).ToArray(),
            },
            JsonRequestBehavior.AllowGet);

        }




        //=====================================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public JsonResult GetUsersRatingsChart(string username, int month = 0, int year = 0)
        {
            var date = DateTime.Now;
            month = month == 0 || month > 12 ? date.Month : month;
            year = year == 0 ? date.Year : year;

            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-2);
            var tttt = new DateTime(1970, 1, 1);
       
            IEnumerable<PostStatisticsViewModel> monthResult = 
                _repository.GetUsersRatingsMonthChart(date1, date2, username).OrderBy(d => d.DateTime).ToList();

            var lastOrder = monthResult.Max(u => u.Count6) ;
            var topRatingRanges = monthResult.Select(b => 
                new long[] 
                { 
                         
                       (long)((b.DateTime - tttt).TotalMilliseconds), b.Count4, b.Count3
                }).ToArray();

            var userRatings = monthResult.Select(b =>
                new long[] 
                {    
                       (long)((b.DateTime - tttt).TotalMilliseconds), b.Count1
                }).ToArray();

            var topOrderRanges = monthResult.Select(b =>
               new long[] 
                {    
                          (long)((b.DateTime - tttt).TotalMilliseconds), 
                          b.Count5 == 0 ? -(lastOrder ) : 0, 
                          b.Count6 == 0 ? -lastOrder : -b.Count6 +1 
                }).ToArray();

            var userTopOrders = monthResult.Select(b =>
               new long[] 
                {    
                       (long)((b.DateTime - tttt).TotalMilliseconds), b.Count2 == 0 ? -lastOrder : -b.Count2 +1
                }).ToArray();

            return Json(
                new {
            
                lastOrder = lastOrder,
                topOrderRanges = topOrderRanges,
                userTopOrders = userTopOrders,

                topRatingRanges = topRatingRanges,
                userRatings = userRatings,
              
                month = month, 
                year = year }, 
                JsonRequestBehavior.AllowGet);
        }


        //=====================================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public JsonResult GetPostRatingsChart(int id,  int month = 0, int year = 0)
        {

            var date = DateTime.Now;
            month = month == 0 || month > 12 ? date.Month : month;
            year = year == 0 ? date.Year : year;

            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-2);

            var tttt = new DateTime(1970, 1, 1);


            RatingsViewModel vm = _repository.GetPostRatingsMonthChart(date1, date2, id);
            var post = vm.Post;
            var   monthResult = vm.RatingsInfo.OrderBy(d => d.DateTime).ToList();

            var lastOrder = monthResult.Max(u => u.Count6);
            var topRatingRanges = monthResult.Select(b =>
                new long[] 
                { 
                         
                       (long)((b.DateTime - tttt).TotalMilliseconds), b.Count4, b.Count3
                }).ToArray();

            var userRatings = monthResult.Select(b =>
                new long[] 
                {    
                       (long)((b.DateTime - tttt).TotalMilliseconds), b.Count1
                }).ToArray();

            var topOrderRanges = monthResult.Select(b =>
               new long[] 
                {    
                          (long)((b.DateTime - tttt).TotalMilliseconds), 
                          b.Count5 == 0 ? -(lastOrder ) : 0, 
                          b.Count6 == 0 ? -lastOrder : -b.Count6 +1 
                }).ToArray();

            var userTopOrders = monthResult.Select(b =>
               new long[] 
                {    
                       (long)((b.DateTime - tttt).TotalMilliseconds), b.Count2 == 0 ? -lastOrder : -b.Count2 +1
                }).ToArray();

            return Json(
                new
                {

                    lastOrder = lastOrder,
                    topOrderRanges = topOrderRanges,
                    userTopOrders = userTopOrders,

                    topRatingRanges = topRatingRanges,
                    userRatings = userRatings,

                    username = post.ApplicationUser.UserName,
                    imageid = post.UserImageId,
                    postid = post.Id,
                    title = post.Title,

                    month = month,
                    year = year
                },
                JsonRequestBehavior.AllowGet);
        }


        //===================================================
        [VisitorActionFilter(ActionType = (int)ActionType.WebsiteReport)]
        public ActionResult GetUsersRatingsRanges()
        {
            DateTime startDate = _repository.GetAllUsersRatingsLastJob();
           // startDate.AddMonths(-1);
            return View(startDate);
        }




        //===================================================
        [VisitorActionFilter(ActionType = (int)ActionType.WebsiteReport)]
        public ActionResult GetPostsRatingsRanges()
        {

            DateTime startDate = _repository.GetAllUsersRatingsLastJob();
            return View(startDate);
        }


        //===================================================
        [VisitorActionFilter(ActionType = (int)ActionType.WebsiteReport)]
        public ActionResult GetPostRatings(int id)
        {
            
            ViewBag.StartDateRange = _repository.GetAllUsersRatingsLastJob();
            return View("GetPostRatingStatistics", id);
        }



        //===================================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public JsonResult GetAllUsersRatingsChart(string selectedDate, int month = 0, int year = 0)
        {

            var date = DateTime.Now;
            month = month == 0 || month > 12 ? date.Month : month;
            year = year == 0 ? date.Year : year;

            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-2);

            var tttt = new DateTime(1970, 1, 1);
            

            IEnumerable<PostStatisticsViewModel> monthResult =
                _repository.GetAllUsersRatingsMonthChart(date1, date2).OrderBy(d => d.DateTime).ToList();

            var lastOrder = monthResult.Count()== 0 ? 0 : monthResult.Max(u => u.Count6);
            var topRatingRanges = monthResult.Select(b =>
                new long[] 
                {   
                       (long)((b.DateTime - tttt).TotalMilliseconds), b.Count4, b.Count3
                }).ToArray();

            var topOrderRanges = monthResult.Select(b =>
               new long[] 
                {    
                          (long)((b.DateTime - tttt).TotalMilliseconds), 
                          b.Count5 == 0 ? -(lastOrder ) : 0, 
                          b.Count6 == 0 ? -lastOrder : -b.Count6 +1 
                }).ToArray();

            return Json(
                new
                {

                    lastOrder = lastOrder,
                    topOrderRanges = topOrderRanges,
                    topRatingRanges = topRatingRanges,
                    minTopRating = monthResult.Select(b =>
               new long[] 
                {    
                          (long)((b.DateTime - tttt).TotalMilliseconds), b.Count3
                          
                }).ToArray(),

                  
                    month = month,
                    year = year
                },
                JsonRequestBehavior.AllowGet);
        }


        //===================================================
      [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.UserReport)]
        public JsonResult GetAllPostsRatingsMonthChart(int month = 0, int year = 0)
        {

            var date = DateTime.Now;
            month = month == 0 || month > 12 ? date.Month : month;
            year = year == 0 ? date.Year : year;

            var date1 = new DateTime(year, month, 1, 0, 0, 0);
            var date2 = date1.AddMonths(1).AddMilliseconds(-2);

            var tttt = new DateTime(1970, 1, 1);


            IEnumerable<PostStatisticsViewModel> monthResult =
                _repository.GetAllPostsRatingsMonthChart(date1, date2).OrderBy(d => d.DateTime).ToList();

            var lastOrder = monthResult.Max(u => u.Count6);
            var topRatingRanges = monthResult.Select(b =>
                new long[] 
                {   
                       (long)((b.DateTime - tttt).TotalMilliseconds), b.Count4, b.Count3
                }).ToArray();

            var topOrderRanges = monthResult.Select(b =>
               new long[] 
                {    
                          (long)((b.DateTime - tttt).TotalMilliseconds), 
                          b.Count5 == 0 ? -(lastOrder ) : 0, 
                          b.Count6 == 0 ? -lastOrder : -b.Count6 +1 
                }).ToArray();

            return Json(
                new
                {

                    lastOrder = lastOrder,
                    topOrderRanges = topOrderRanges,
                    topRatingRanges = topRatingRanges,
                    minTopRating = monthResult.Select(b =>
               new long[] 
                {    
                          (long)((b.DateTime - tttt).TotalMilliseconds), b.Count3
                          
                }).ToArray(),


                    month = month,
                    year = year
                },
                JsonRequestBehavior.AllowGet);
        }



        //=====================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.ManageProfile)]
        public ActionResult GetGroupForListBoxes(string username, int groupid)
        {
            var user = _repository.GetGroupForListBoxes(username, groupid);
            var friends = user.FriendsWith.Select(u => new { UserName = u.UserName });
            var group = user.FriendGroups
                .Where(g => g.Id == groupid).FirstOrDefault();
            var groupfriends = group.Friends
                .Select(u => new { UserName = u.UserName }).OrderBy(u => u.UserName).ToArray();

            var friendsExceptGroup = friends.Except(groupfriends).OrderBy(u => u.UserName).ToArray(); 
            return Json(
              new
              {
                  FriendsCount = friendsExceptGroup.Count(),
                  GroupFriendsCount = groupfriends.Count(),
                  Friends = friendsExceptGroup,
                  GroupFriends = groupfriends,
                  GroupId = group.Id,
                  GroupName = group.Name

              },
              JsonRequestBehavior.AllowGet);
        }



        //=================================================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.ManageProfile)]
        public ActionResult GetFriendsForListBoxes(string username)
        {
             var user = _repository.GetFriendsForListBoxes(username);
             var friends = user.FriendsWith.Select(u => new { UserName = u.UserName }).OrderBy(u => u.UserName).ToArray();

          
                 return Json(
                 new
                 {
                     FriendsCount = friends.Count(),
                     GroupFriendsCount = 0,
                     Friends = friends,
                     GroupFriends = new { },
                     GroupId = -1,
                     GroupName = "Deleted friends"

                 },
                 JsonRequestBehavior.AllowGet);           
        }


        //=======================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.ManageProfile)]
        public ActionResult GetNewFriendsGroupForListBoxes(string username)
        {
            var user = _repository.GetFriendsForListBoxes(username);
            var friends = user.FriendsWith.Select(u => new { UserName = u.UserName }).OrderBy(u => u.UserName).ToArray();

             return Json(
                 new
                 {
                     FriendsCount = friends.Count(),
                     GroupFriendsCount = 0,
                     Friends = friends,
                     GroupFriends = new { },
                     GroupId = 0,
                     GroupName = ""

                 },
                 JsonRequestBehavior.AllowGet);
        }

        //=======================================
        [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.ManageProfile)]
        public ActionResult DeleteGroup(string username, int groupid)
        {
            var id = _repository.DeleteGroup(username, groupid);
            return Json(
                new
                {
                    id = id
                },
                JsonRequestBehavior.AllowGet);
        }

        //=======================================
     //   [AcceptVerbs(HttpVerbs.Post)]
        [VisitorActionFilter(ActionType = (int)ActionType.ManageProfile)]
        public ActionResult SaveFriendsOrGroup(int groupid, string name, string username, string[] names)
        {

            if (groupid == -1) _repository.DeleteFriends(username, names);
            else
              _repository.AddOrEditGroup(username, name, groupid, names); 

           return RedirectToAction("ManageFriendsGroups", "User", new {id = username});
        }

        //=======================================
    }
}