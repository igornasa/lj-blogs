﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Visitor;
using LJ.ViewModels.User;
using LJ.ViewModels.Blog;
using LJ.ViewModels.Visitor;

namespace LJ.Repository.VisitorRepository
{
    public interface IVisitorRepository<T, TId> where T : class
    {
        void SaveWebVisitor(WebVisitor wv);
        IEnumerable<PostStatisticsViewModel> GetVisitsForMonth(DateTime d1, DateTime d2);
        DateTime GetHistoryVisitorsStartDate();
        IEnumerable<PostStatisticsViewModel> GetVisitsForMonthByCountry(DateTime d1, DateTime d2);

        int AddVisitorsToArxiv(DateTime d1, DateTime d2);
        int CalculateRatings(DateTime d1, DateTime d2, int jobType);
        IEnumerable<ApplicationVisitor> GetAllVisitorsList();

        //Jobs
        void AddCommentsForJob();
        Int64 SaveVisitorActionLog(VisitorActionLog log);
        void JobAddVisitorLogsToArxives();

       //Reports
        IEnumerable<VisitorActionsViewModel> GetVisitorActionsForUser(string username);
        IEnumerable<VisitorActionsViewModel> GetExitPagesForUser(string username);
        IEnumerable<VisitorActionsViewModel> GetBouncePagesForUser(string username);

        IEnumerable<VisitorActionsViewModel> GetVisitorActionsForAdmin();
        IEnumerable<VisitorActionsViewModel> GetExitPagesForAdmin();
        IEnumerable<VisitorActionsViewModel> GetBouncePagesForAdmin();

        
        
    }
}

