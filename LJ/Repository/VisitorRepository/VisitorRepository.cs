﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Visitor;
using LJ.Models.User;
using LJ.Models.Blog;
using LJ.Repository.BlogRepository;
using LJ.ViewModels.User;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Reflection;
using LJ.ViewModels.Visitor;

namespace LJ.Repository.VisitorRepository
{
    public class VisitorRepository : IVisitorRepository<ApplicationVisitor, int>
    {
        private BlogDbContext _context;

        public VisitorRepository(BlogDbContext context)
        {
            _context = context;
        }
      
        //================ Reports =================================
        public DateTime GetHistoryVisitorsStartDate()
        {
            
            return _context.ArxivVisitors.Count() == 0 ? DateTime.Now.AddMilliseconds(-3) : _context.ArxivVisitors.Min(d => d.Date);
        }


        //===========================================================
        public IEnumerable<PostStatisticsViewModel> GetVisitsForMonthByCountry(DateTime d1, DateTime d2)
        {
            
            IEnumerable<PostStatisticsViewModel> vm = _context.Visitors
                         .Where(v => v.SessionStarted >= d1 && v.SessionStarted <= d2)
                         .GroupBy(s => s.CountryId, s => s)
                         .Select(f => new PostStatisticsViewModel
                         {
                             Id2 = f.Key,
                             Title = f.FirstOrDefault().CountryName,
                             Count1 = f.Count(), //visits
                             Count2 = f.Where(ff => ff.UserId != null) //users
                                                      .GroupBy(kk => kk.UserId, kk => kk.UserId)
                                                      .Select(pp => pp.Key).Count(),
                             Count3 = f.Where(ff => ff.UserId == null) // ananimuses
                                                      .GroupBy(kk => kk.Anonymous, kk => kk.Anonymous)
                                                      .Select(pp => pp.Key).Count(),
                         });
            return vm;

        }


        
        //==============================================================
        public IEnumerable<PostStatisticsViewModel> GetVisitsForMonth(DateTime d1, DateTime d2)
        {

            IEnumerable<PostStatisticsViewModel> vm = _context.Visitors
                         .Where(v => v.SessionStarted >= d1 && v.SessionStarted <= d2)
                         .GroupBy(s => s.SessionStarted.Day, s => s)
                         .Select(f => new PostStatisticsViewModel
                         {
                             Day = f.Key,
                             Count1 = f.Count(), //visits
                             Count2 = f.Where(ff => ff.UserId != null) //users
                                                      .GroupBy(kk => kk.UserId, kk => kk.UserId)
                                                      .Select(pp => pp.Key).Count(),
                             Count3 = f.Where(ff => ff.UserId == null) // ananimuses
                                                      .GroupBy(kk => kk.Anonymous, kk => kk.Anonymous)
                                                      .Select(pp => pp.Key).Count(),
                         });
            return vm;

        }



      
        //==============================================================
        public IEnumerable<ApplicationVisitor> GetAllVisitorsList()
        {
            var av = _context.Visitors.Include(h => h.User)
                .OrderBy(u => u.CountryId)
                .ThenBy(g => g.IpAddress)
                .ThenBy(g => g.User.UserName)
                .ThenBy(d => d.SessionStarted).ToList();
            return av;

        }



       
        //==============================================================
        public IEnumerable<VisitorActionsViewModel> GetVisitorActionsForUser(string username)
        {
            var user = _context.Users.Where(u => u.UserName == username).FirstOrDefault();

            var actions1 = _context.VisitorActionLogs
                     .Where(u => u.UserName == username && u.VisitorUserName != username)
                    .GroupBy(g => new { Key1 = g.Action, Key2 = g.PostId,  })
                    .Select(f => new VisitorActionsViewModel
                    {                       
                        Action = f.Key.Key1,
                        Id = f.Key.Key2,
                        SpentTimeMC = f.Sum( ff => ff.SpentTimeMC),                      
                        Count1 = f.Count(),
                        
                        Count2 = f.Where(u => u.VisitorUserName != "")
                                    .GroupBy(ff => ff.VisitorUserName)
                                    .Select(fff => fff.Key).Count(),
                                              
                        Count3 = f.GroupBy(a => a.Anonymous)
                                    .Select(s => s.Where(u => u.VisitorUserName != "").Count())
                                    .Where(x => x == 0).Count(),
                        Title1 = ""
                    });


            var actions2 = actions1.Concat(_context.ArxiveVisitorLogsForUser.Where(b => username == b.UserName) //union all
                                      .Select(b => new VisitorActionsViewModel
                                      {                                         
                                          Action = b.Action,
                                          Id = (int)b.PostId,
                                          SpentTimeMC = b.SpentTimeMC,
                                          Count1 = b.CountVisits, 
                                          Count2 = b.CountUsers, 
                                          Count3 = b.CountAnonymous,
                                          Title1 = ""
                                      })
                              )
                              .GroupBy(s => new { Key1 = s.Id, Key2 = s.Action })
                              .Select(b => new VisitorActionsViewModel
                              {
                                  
                                  Action = b.Key.Key2,
                                  Id = b.Key.Key1,
                                  SpentTimeMC = b.Sum(bb => bb.SpentTimeMC), //VisitsCount
                                  Count1 = b.Sum(bb => bb.Count1), //VisitsCount
                                  Count2 = b.Sum(bb => bb.Count2),
                                  Count3 = b.Sum(bb => bb.Count3),
                                  Title1 =""
                              });

            var actionsFull = actions2.GroupJoin(_context.Posts,
                                i1 => i1.Id, i2 => i2.Id,
                                (i1,i2) => new VisitorActionsViewModel 
                                {                                  
                                    Action = i1.Action,
                                    Id = i1.Id,
                                    SpentTimeMC = i1.SpentTimeMC,
                                    Count1 = i1.Count1,
                                    Count2 = i1.Count2,
                                    Count3 = i1.Count3,
                                    Title1 = i2.Count() == 0 ? "" : i2.FirstOrDefault().Title,
                                }
                )
                .OrderByDescending(c => (c.SpentTimeMC) / (c.Count2 + c.Count3)).ThenBy(c => c.Action).ThenBy(c => c.Title1);
            return actionsFull;

        }



        //====================================================================
        public IEnumerable<VisitorActionsViewModel> GetVisitorActionsForUserOLD(string username)
        {
            var user = _context.Users.Where(u => u.UserName == username).FirstOrDefault();

            var actions = _context.VisitorActionLogs
                     .Where(u => u.UserName == username && u.VisitorUserName != username)
                    .GroupBy(g => new { Key1 = g.Action, Key2 = g.PostId, Key3 = g.DateTime.Day })
                    .Select(f => new
                    {
                        Day = f.Key.Key3,
                        Action = f.Key.Key1,
                        PostId = f.Key.Key2,
                        SpentTimeMC = f.Sum(ff => ff.SpentTimeMC),
                        VisitsCount = f.Count(),
                        Users = f.Where(u => u.VisitorUserName != "")
                                        .GroupBy(ff => ff.VisitorUserName)
                                        .Select(fff => fff.Key),
                        UsersCount = f.Where(u => u.VisitorUserName != "")
                                        .GroupBy(ff => ff.VisitorUserName)
                                        .Select(fff => fff.Key).Count(),
                        Anonymouses = f.GroupBy(a => a.Anonymous)
                                        .Select(s => new { Anonymous = s.Key, UsersCount = s.Where(u => u.VisitorUserName != "").Count() })
                                        .Where(x => x.UsersCount == 0).Select(t => t.Anonymous),
                        AnonymousCount = f.GroupBy(a => a.Anonymous)
                                        .Select(s => s.Where(u => u.VisitorUserName != "").Count())
                                        .Where(x => x == 0).Count()

                    }).ToList();

            var actionsFull = actions.GroupJoin(_context.Posts,
                                i1 => i1.PostId, i2 => i2.Id,
                                (i1, i2) => new VisitorActionsViewModel
                                {
                                    Day = i1.Day,
                                    Action = i1.Action,
                                    SpentTimeMC = i1.SpentTimeMC,
                                    Id = i1.PostId,
                                    Title1 = i2.Count() == 0 ? "" : i2.FirstOrDefault().Title,
                                    Count1 = i1.VisitsCount,
                                    Names1 = i1.Users,
                                    Names2 = i1.Anonymouses,
                                    Count2 = i1.UsersCount,
                                    Count3 = i1.AnonymousCount
                                }

                ).ToList();
            return actionsFull;

        }


        //====================================================================
        public IEnumerable<VisitorActionsViewModel> GetVisitorActionsForAdmin()
        {
            var actions = _context.VisitorActionLogs

                    .GroupBy(g => new { Key1 = g.Controller, Key2 = g.Action })
                    .Select(f => new VisitorActionsViewModel
                    {

                        Controller = f.Key.Key1,
                        Action = f.Key.Key2,
                        ActionType = f.FirstOrDefault().ActionType,
                        SpentTimeMC = f.Sum(ff => ff.SpentTimeMC),
                        Count1 = f.Count(), // VisitsCount

                        Count2 = f.Where(u => u.VisitorUserName != "")
                              .GroupBy(ff => ff.VisitorUserName)
                              .Select(fff => fff.Key).Count(), //UsersCount

                        Count3 = f.GroupBy(a => a.Anonymous)
                                    .Select(s => s.Where(u => u.VisitorUserName != "").Count())
                                     .Where(x => x == 0).Count() // Anonymous count

                    });

            var actionsFull = actions.Concat(_context.ArxiveVisitorLogsForAdmin.Where(h => h.SpentTimeMC > 0) //union all
                             .Select(b => new VisitorActionsViewModel
                                      {
                                          Controller = b.Controller,
                                          Action = b.Action,
                                          ActionType = b.ActionType,
                                          SpentTimeMC = b.SpentTimeMC,
                                          Count1 = b.CountVisits, 
                                          Count2 = b.CountUsers, 
                                          Count3 = b.CountAnonymous, 
                                      })
                              )
                              .GroupBy(s => new { Key1 = s.Controller, Key2 = s.Action })
                              .Select(b => new VisitorActionsViewModel
                                      {
                                          Controller = b.Key.Key1,
                                          Action = b.Key.Key2,
                                          ActionType = b.FirstOrDefault().ActionType,
                                          SpentTimeMC = b.Sum(bb => bb.SpentTimeMC), 
                                          Count1 = b.Sum(bb => bb.Count1), 
                                          Count2 = b.Sum(bb => bb.Count2),
                                          Count3 = b.Sum(bb => bb.Count3), 
                                      })
                              .OrderByDescending(c => (c.SpentTimeMC) / (c.Count2 + c.Count3))
                              .ThenBy(c => c.Controller)
                              .ThenBy(c => c.ActionType);                            
            return actionsFull;
        }




        //====================================================================
        public IEnumerable<VisitorActionsViewModel> GetExitPagesForUser(string username)
        {
            var actions = (_context.VisitorActionLogs
                    .Where(u => u.UserName == username && u.VisitorUserName != username)
                    .GroupBy(f => f.SessionId)
                    .Select(s => s.OrderByDescending(ss => ss.DateTime).FirstOrDefault()) //last action in session for username
                    .GroupBy(g => new { Key1 = g.Controller, Key2 = g.Action, Key3 = g.PostId })
                    .Select(f => new
                    {
                        Controller = f.Key.Key1,
                        Action = f.Key.Key2,

                        PostId = f.Key.Key3,
                        Id = f.Key.Key3,
                        Count = f.Count(), // VisitsCount        
                    })

                    .GroupJoin(_context.VisitorActionLogs
                                   .Where(u => u.UserName == username && u.VisitorUserName != username),
                     i1 => new { Key1 = i1.Controller, Key2 = i1.Action, Key3 = i1.PostId, Key4 = username },
                     i2 => new { Key1 = i2.Controller, Key2 = i2.Action, Key3 = i2.PostId, Key4 = i2.UserName },
                     (i1, i2) => new VisitorActionsViewModel
                     {
                         Controller = i1.Controller,
                         Action = i1.Action,
                         Id = i1.PostId,
                          Count1 = i2.Count(),
                         Count2 = i1.Count,
                         Title1 = ""
                        
                     }
                    ))
                    .Concat(_context.ArxiveVisitorLogsForUser.Where(b => username == b.UserName && b.ExitCount > 0)
                                    .Select(i => new VisitorActionsViewModel
                                    {
                                        Controller = i.Controller,
                                        Action = i.Action,
                                        Id = i.PostId,
                                        Count1 = i.CountVisits,
                                        Count2 = i.ExitCount,
                                        Title1 = "",                                      
                                    })
                              )
                   .GroupBy(f => new { Key1 = f.Controller, Key2 = f.Action, Key3 = f.Id })
                   .Select(i => new VisitorActionsViewModel
                           {
                                        Controller = i.Key.Key1,
                                        Action = i.Key.Key2,
                                        Id = i.Key.Key3,
                                        Count1 = i.Sum(ii => ii.Count1),
                                        Count2 = i.Sum(ii => ii.Count2),
                                        Title1 = ""                                      
                            });

            var actionsFull =  actions.GroupJoin(_context.Posts, i1 => i1.Id, i2 => i2.Id,
                               (i1, i2) => new VisitorActionsViewModel
                               {
                                   Controller = i1.Controller,
                                   Action = i1.Action,
                                   Id = i1.Id,
                                   Count1 = i1.Count1,
                                   Count2 = i1.Count2,
                                   Title1 = i2.Count() == 0 ? "" : i2.FirstOrDefault().Title
                               })
                   
                              .OrderByDescending(c => (c.Count2 * 100) / c.Count1)
                              .ThenBy(c => c.Controller)
                              .ThenBy(c => c.Action);
            return actionsFull;
        }
        

        //====================================================================
        public IEnumerable<VisitorActionsViewModel> GetExitPagesForAdmin()
        {
            var actions = _context.VisitorActionLogs

                    .GroupBy(g => new { Key1 = g.Controller, Key2 = g.Action })
                    .Select(f => new VisitorActionsViewModel
                    {

                        Controller = f.Key.Key1,
                        Action = f.Key.Key2,
                        ActionType = f.FirstOrDefault().ActionType,
                        Count1 = f.Count(), // VisitsCount
                        Count2 = f.Where(u => u.SpentTimeMC == 0).Count() 
                    })
                    .Where(u => u.Count2 != 0)

                    .Concat(_context.ArxiveVisitorLogsForAdmin.Where(h =>h.ExitCount > 0) //union all
                             .Select(b => new VisitorActionsViewModel
                                      {
                                          Controller = b.Controller,
                                          Action = b.Action,
                                          ActionType = b.ActionType,
                                          Count1 = b.CountVisits, //VisitsCount
                                          Count2 = b.ExitCount, //exit count                                          
                                      })
                              )
                              .GroupBy(s => new {Key1 = s.Controller, Key2 =s.Action})
                              .Select(b => new VisitorActionsViewModel
                                      {
                                          Controller = b.Key.Key1,
                                          Action = b.Key.Key2,
                                          ActionType = b.FirstOrDefault().ActionType,
                                          Count1 = b.Sum(bb => bb.Count1), //VisitsCount
                                          Count2 = b.Sum(bb =>bb.Count2), //exit count                                          
                                      })

                              .OrderByDescending(c => (c.Count2 *100) / c.Count1)
                              .ThenBy(c => c.Controller)
                              .ThenBy(c => c.Action);
            return actions;
        }


        //====================================================================
        public IEnumerable<VisitorActionsViewModel> GetBouncePagesForUser(string username)
        {
            var sessionsWithBouncePage = _context.VisitorActionLogs
                          .Where(u => u.UserName == username && u.VisitorUserName != username)
                          .GroupBy(d => d.SessionId)
                          .Select(o => new { SessionId = o.Key, Count = o.Count(), ActionLog = o.FirstOrDefault() })
                          .Where(d => d.Count == 1)
                          .Select(h => new 
                          { 
                              SessionId = h.SessionId, 
                              Action = h.ActionLog.Action, 
                              PostId = h.ActionLog.PostId,
                              Controller = h.ActionLog.Controller 
                          })
                          .GroupBy(r => new { Key1 = r.Action, Key2 = r.Controller, Key3 = r.PostId })
                          .Select(t => new 
                          { 
                              Action = t.Key.Key1, 
                              Controller = t.Key.Key2, 
                              PostId = t.Key.Key3,
                              Count = t.Count() 
                          });

            var bouncePages = sessionsWithBouncePage
                                  .GroupJoin(_context.VisitorActionLogs.Where(u => u.VisitorUserName != username),
                                  i1 => new { Key1 = i1.Action, Key2 = i1.Controller },
                                  i2 => new { Key1 = i2.Action, Key2 = i2.Controller },
                                  (i1, i2) => new VisitorActionsViewModel
                                  {
                                      Controller = i1.Controller,
                                      Action = i1.Action,
                                      Id = i1.PostId,
                                      Count1 = i2.Count(), //VisitsCount
                                      Count2 = i1.Count, //bounce count
                                      Title1 = "",                                     
                                  }
                                  )
                                   .Concat(_context.ArxiveVisitorLogsForUser.Where(b => b.BounceCount > 0 && username == b.UserName)
                                   .Select(i => new VisitorActionsViewModel
                                    {
                                        Controller = i.Controller,
                                        Action = i.Action,
                                        Id = i.PostId,
                                        Count1 = i.CountVisits,
                                        Count2 = i.BounceCount,
                                        Title1 = "",
                                    })
                              )
                    .GroupBy(f => new { Key1 = f.Controller, Key2 = f.Action, Key3 = f.Id })
                    .Select(i => new VisitorActionsViewModel
                    {
                        Controller = i.Key.Key1,
                        Action = i.Key.Key2,
                        Id = i.Key.Key3,
                        Count1 = i.Sum(ii => ii.Count1),
                        Count2 = i.Sum(ii => ii.Count2),
                        Title1 = ""
                    });

            var bouncePagesFull =    bouncePages.GroupJoin(_context.Posts, i1 => i1.Id, i2 => i2.Id,
                               (i1, i2) => new VisitorActionsViewModel
                               {
                                   Controller = i1.Controller,
                                   Action = i1.Action,
                                   Id = i1.Id,
                                   Count1 = i1.Count1,
                                   Count2 = i1.Count2,
                                   Title1  = i2.Count() == 0 ? "" : i2.FirstOrDefault().Title
                               })
                   
                    .OrderByDescending(c => (c.Count2 * 100) / c.Count1)
                    .ThenBy(c => c.Controller)
                    .ThenBy(c => c.Action);
            return bouncePagesFull;

        }


        //====================================================================
        public IEnumerable<VisitorActionsViewModel> GetBouncePagesForAdmin()
        {

            var sessionsWithBouncePage = _context.VisitorActionLogs
                          .GroupBy(d => d.SessionId)
                          .Select(o => new { SessionId = o.Key, Count = o.Count() , ActionLog = o.FirstOrDefault() })
                          .Where (d => d.Count == 1)
                          .Select(h => new {
                              SessionId = h.SessionId, 
                              Action = h.ActionLog.Action,
                              ActionType = h.ActionLog.ActionType,
                              Controller = h.ActionLog.Controller 
                          })
                          .GroupBy (r =>   new {Key1 = r.Action, Key2 =r.Controller})
                          .Select (t => new { 
                              Action = t.Key.Key1, 
                              ActionType = t.FirstOrDefault().ActionType,
                              Controller = t.Key.Key2,
                              Count = t.Count()
                          });

            var bouncePages = sessionsWithBouncePage.GroupJoin(_context.VisitorActionLogs,
                                  i1 => new {Key1 = i1.Action, Key2 =i1.Controller},
                                  i2 => new {Key1 = i2.Action, Key2 =i2.Controller},
                                  (i1, i2) => new VisitorActionsViewModel
                                  {
                                      Action = i1.Action,
                                      Controller = i1.Controller,
                                      ActionType = i1.ActionType,
                                      Count2 = i1.Count, //bounce count
                                      Count1 = i2.Count(), //VisitsCount
                                  })
                                 .Concat(_context.ArxiveVisitorLogsForAdmin.Where(h =>h.BounceCount > 0) //union all
                                      .Select(b => new VisitorActionsViewModel
                                      {
                                          Action = b.Action,
                                          Controller = b.Controller,
                                          ActionType = b.ActionType,
                                          Count2 = b.BounceCount, //bounce count
                                          Count1 = b.CountVisits, //VisitsCount
                                      })
                                      ).GroupBy(s => new {Key1 = s.Controller, Key2 =s.Action})
                                       .Select(b => new VisitorActionsViewModel
                                      {
                                          Action = b.Key.Key2,
                                          Controller = b.Key.Key1,
                                          ActionType = b.FirstOrDefault().ActionType,
                                          Count2 = b.Sum(bb =>bb.Count2), //bounce count
                                          Count1 = b.Sum(bb =>bb.Count1), //VisitsCount
                                      })
                                .OrderByDescending(c => (c.Count2 * 100) / c.Count1)
                                .ThenBy(c => c.Controller)
                                .ThenBy(c => c.Action);
           return bouncePages;

        }


        
        
        //=============================================
        public void JobAddVisitorLogsToArxives()
        {
            // sent to arxive only when session ended  logoff or 
          //  var usersArxive = _context.VisitorActionLogs.Where(d => d.DateTime <= date2 && d.DateTime >= date1 && d.SpentTimeMC == 0);
            if (_context.UserRatingJobs.Count() == 0) return;
            var lastRatingJob = _context.UserRatingJobs.Max(d => d.DateTime);

            //for users 
            var usersArxiveTemp1 = _context.VisitorActionLogs
                .Where(d => d.DateTime <= lastRatingJob && d.UserName != "" && d.UserName != d.VisitorUserName)
                .GroupBy(g => new { Key1 = g.SessionId, key2 = g.Anonymous })
                .Select(f => new
                {
                  Visits = f.GroupBy(v => new { Key1 = v.Controller , Key2 = v.Action, Key3 = v.PostId, Key4 = v.UserName})
                                .Select (vv => new 
                                {
                                    Controller = vv.Key.Key1,
                                    Action = vv.Key.Key2,
                                    PostId =  vv.Key.Key3,
                                    UserName = vv.Key.Key4,
                                    CountVisits = vv.Count(),
                                    SpentTimeMC = vv.Sum(vvv => vvv.SpentTimeMC),

                                    CountUsers = vv.Where(u => u.VisitorUserName != "")
                                        .GroupBy(vvv => vvv.VisitorUserName)
                                        .Select(vvvv => vvvv.Key).Count(),

                                    CountAnonymous = vv.Where(u => u.VisitorUserName == "" )
                                    .Count() == 0 ? 0 : 1,
                                }), // visits
                                   
                    ExitLog = f.OrderByDescending(t =>t.DateTime).FirstOrDefault(), // bounce action
                    BounceCount = f.Count() == 1? 1 : 0// exit actons
                });

            var usersArxiveTemp2 = from u in usersArxiveTemp1
                                   from uu in u.Visits
                                   select new                                    
                                   {
                                       Controller = uu.Controller,
                                       Action = uu.Action,
                                       PostId = uu.PostId,
                                       UserName = uu.UserName,
                                       CountVisits = uu.CountVisits,
                                       SpentTimeMC = uu.SpentTimeMC,
                                       CountUsers = uu.CountUsers,
                                       BounceCount = u.BounceCount,
                                       ExitCount = uu.Controller == u.ExitLog.Controller
                                                          && uu.Action == u.ExitLog.Action
                                                                 && uu.PostId == u.ExitLog.PostId ? 1 : 0,
                                       CountAnonymous = uu.CountUsers > 0 ? 0 : uu.CountAnonymous,                                           
                                   };

            var usersArxiveTemp3 = usersArxiveTemp2.GroupBy(k => new
            {
                Key1 = k.Controller,
                Key2 = k.Action,
                Key3 = k.PostId,
                Key4 = k.UserName
            }).AsEnumerable()
                .Select(u => new ArxiveVisitorLogForUser
                {
                    Controller = u.Key.Key1,
                    StartDateTime = lastRatingJob,
                    EndDateTime = lastRatingJob,
                    Action = u.Key.Key2,
                    PostId = u.Key.Key3,
                    UserName = u.Key.Key4,
                    SpentTimeMC = u.Sum(uu => uu.SpentTimeMC),
                    CountVisits = u.Sum(uu => uu.CountVisits),
                    CountUsers = u.Sum(uu => uu.CountUsers),
                    CountAnonymous = u.Sum(uu => uu.CountAnonymous),
                    ExitCount = u.Sum(uu => uu.ExitCount),
                    BounceCount = u.Sum(uu => (uu.ExitCount + uu.BounceCount) == 2 ? 1 : 0)
                }).ToList();


            //Admin
            var websiteArxiveTemp1 = _context.VisitorActionLogs
               .Where(d => d.DateTime <= lastRatingJob)
               .GroupBy(g => new { Key1 = g.SessionId, key2 = g.Anonymous })
               .Select(f => new
               {
                   Visits = f.GroupBy(v => new { Key1 = v.Controller, Key2 = v.Action })
                                 .Select(vv => new
                                 {
                                     Controller = vv.Key.Key1,
                                     Action = vv.Key.Key2,
                                     CountVisits = vv.Count(),
                                     SpentTimeMC = vv.Sum(vvv => vvv.SpentTimeMC),
                                     CountUsers = vv.Where(u => u.VisitorUserName != "")
                                         .GroupBy(vvv => vvv.VisitorUserName)
                                         .Select(vvvv => vvvv.Key).Count(),

                                     CountAnonymous = vv.Where(u => u.VisitorUserName == "")
                                     .Count() == 0 ? 0 : 1,
                                     ActionType = vv.FirstOrDefault().ActionType
                                 }), // visits

                   ExitLog = f.OrderByDescending(t => t.DateTime).FirstOrDefault(), // bounce action                  
                   BounceCount = f.Count() == 1 ? 1 : 0// exit actions
               });


            var websiteArxiveTemp2 = 
                                   from u in websiteArxiveTemp1
                                   from uu in u.Visits
                                   select new
                                   {
                                       Controller = uu.Controller,
                                       Action = uu.Action,
                                       ActionType = uu.ActionType,
                                       CountVisits = uu.CountVisits,
                                       SpentTimeMC = uu.SpentTimeMC,
                                       CountUsers = uu.CountUsers,
                                       BounceCount = u.BounceCount,
                                       ExitCount = uu.Controller == u.ExitLog.Controller
                                                          && uu.Action == u.ExitLog.Action ? 1 : 0,
                                       CountAnonymous = uu.CountUsers > 0 ? 0 : uu.CountAnonymous,

                                   };

            var websiteArxiveTemp3 = websiteArxiveTemp2.GroupBy(k => new
            {
                Key1 = k.Controller,
                Key2 = k.Action
            })
            .AsEnumerable()
            .Select(u => new ArxiveVisitorLogForAdmin
               {
                   Controller = u.Key.Key1,
                   Action = u.Key.Key2,
                   StartDateTime = lastRatingJob,
                   EndDateTime = lastRatingJob,
                   ActionType = u.FirstOrDefault().ActionType,
                   SpentTimeMC = u.Sum(uu => uu.SpentTimeMC),
                   CountVisits = u.Sum(uu => uu.CountVisits),
                   CountUsers = u.Sum(uu => uu.CountUsers),
                   CountAnonymous = u.Sum(uu => uu.CountAnonymous),
                   ExitCount = u.Sum(uu => uu.ExitCount),
                   BounceCount = u.Sum(uu => (uu.ExitCount + uu.BounceCount) == 2 ? 1 : 0)
               }).ToList();

            _context.ArxiveVisitorLogsForUser.AddRange(usersArxiveTemp3);
            _context.ArxiveVisitorLogsForAdmin.AddRange(websiteArxiveTemp3);
            var actionLogsForDelete = _context.VisitorActionLogs
                .Where(d => d.DateTime <= lastRatingJob).ToList();

            _context.VisitorActionLogs.RemoveRange(actionLogsForDelete);
            _context.SaveChanges();
        }



        
        //===============================================================
        public int CalculateRatings(DateTime d1, DateTime d2, int jobType)
        {
            var currentTime = d2;// DateTime.Now;

            //rating job id
            var userRatingJob = new UserRatingJob() { DateTime = currentTime, JobType = jobType };
            _context.UserRatingJobs.Add(userRatingJob);
            
            //comments
            var postsComments = ((_context.Comments
                         .Where(v => v.DateTime >= d1 && v.DateTime <= d2)
                         .GroupBy(s => s.PostId, s => s)
                         .Select(f => new
                         {
                             PostId = f.Key,
                             CommentsCount = f.Count(), 
                         })
                      )
                     .Concat //don't remove dublicates
                     (_context.Replies
                         .Where(v => v.DateTime >= d1 && v.DateTime <= d2)
                         .GroupBy(s => s.PostId, s => s)
                         .Select(f => new
                         {
                             PostId = f.Key,
                             CommentsCount = f.Count(), 
                         })
                      ))
                      .GroupBy(w => w.PostId, w => w.CommentsCount)
                      .Select(ww => new {PostId = ww.Key, CommentsCount = ww.Sum(www =>www) })
                      .ToList();

            //likes
            var postsLikes = _context.PostLikes
                         .Where(v => v.DateTime >= d1 && v.DateTime <= d2)
                         .GroupBy(p => p.PostId, p => p)
                         .Select(g => new
                         {
                             PostId = g.Key,
                             LikesCount = g.Sum(ff => ff.Like ? 1 : 0) - g.Sum(ff => ff.Dislike ? 1 : 0)
                         })
                       .GroupBy(w => w.PostId, w => w.LikesCount)
                       .Select(ww => new {PostId = ww.Key, LikesCount = ww.Sum(www =>www) })
                       .ToList();
            //visitors
            // need to add 

            var postVisitors = _context.VisitorActionLogs
                                .Where(v => v.DateTime >= d1 && v.DateTime <= d2 && v.PostId > 0)
                                .GroupBy(p => p.PostId)
                                .Select(g => new
                                         {
                                             PostId = g.Key,   
                                             SpentTimeMC = g.Sum(ff => ff.SpentTimeMC),
                                             VisitsCount = g.GroupBy(gg => new{ Key1 =gg.SessionId ,Key2 = gg.Anonymous}).Count(),
                                             AnonymousCount = g.Where(gg => gg.VisitorUserName == "").GroupBy(gg => gg.VisitorUserName).Count(),
                                             UsersCount = g.Where(gg => gg.VisitorUserName != "").GroupBy(gg => gg.VisitorUserName).Count()
                                         }).ToList();
                                
            //Posts with current Ratings
            var postsIds = postsComments.Select(p => p.PostId) //comments
                                        .Union(postsLikes.Select(p => p.PostId)) //likes
                                        .Union(postVisitors.Select(p => p.PostId)).ToList(); //visitors

  
            var postRatings = postsIds.GroupJoin(postsComments, i1 => i1, i2 => i2.PostId,
                                                 (i1, i2) => new
                                                 {
                                                     PostId = i1,
                                                     CommentsCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().CommentsCount
                                                 })
                                                 .GroupJoin(postsLikes, ii1 => ii1.PostId, ii2 => ii2.PostId,
                                                  (ii1, ii2) => new
                                                  {
                                                      PostId = ii1.PostId,
                                                      CommentsCount = ii1.CommentsCount,
                                                      LikesCount = ii2.Count() == 0 ? 0 : ii2.FirstOrDefault().LikesCount
                                                  })
                                                  .GroupJoin(postVisitors, iii1 => iii1.PostId, iii2 => iii2.PostId,
                                                  (iii1, iii2) => new
                                                  {
                                                      PostId = iii1.PostId,
                                                      CommentsCount = iii1.CommentsCount,
                                                      LikesCount = iii1.LikesCount,
                                                     // SpentTimeMC = iii2.Count() == 0 ? 0 : iii2.FirstOrDefault().SpentTimeMC,
                                                      SpentTimeMC = iii2.Count() == 0 ? 0 : 2,//(long)(iii2.FirstOrDefault().SpentTimeMC / 60000000),
                                                     
                                                      VisitsCount = iii2.Count() == 0 ? 0 : iii2.FirstOrDefault().VisitsCount,
                                                      AnonymousCount = iii2.Count() == 0 ? 0 : iii2.FirstOrDefault().AnonymousCount,
                                                      UsersCount = iii2.Count() == 0 ? 0 : iii2.FirstOrDefault().UsersCount
                                                  })
                                                  .Join(_context.Posts, y1 => y1.PostId, y2 => y2.Id,
                                                       (y1, y2) => new
                                                       {
                                                           PostId = y1.PostId,
                                                           ApplicationUserId = y2.ApplicationUserId,
                                                          Rating = (int)(y1.SpentTimeMC / 1000) + //minutes
                                                                      y1.VisitsCount +                    
                                                                    y1.CommentsCount + 
                                                                    y1.LikesCount +
                                                                    y1.VisitsCount, 
                                                          
                                                           CommentsCount = y1.CommentsCount,
                                                           LikesCount = y1.LikesCount,
                                                           SpentTimeMC = y1.SpentTimeMC,
                                                           VisitsCount = y1.VisitsCount,
                                                           AnonymousCount = y1.AnonymousCount,
                                                           UsersCount = y1.UsersCount
                                                       })
                                                   .GroupBy(r=> r.Rating, r => r)
                                                   .Where(p => p.Key > 0)
                                                   .OrderByDescending(d => d.Key)
                                                  .SelectMany((d, index) => d.Select (dd => new PostRating
                                                  {
                                                      TopDenseRank = index + 1,
                                                      DateTime = currentTime, 
                                                      FromDateTime = d1,
                                                      ToDateTime = d2,
                                                      Rating = d.Key,
                                                      PostId = dd.PostId,
                                                      ApplicationUserId = dd.ApplicationUserId,
                                                      CommentsCount = dd.CommentsCount,
                                                      LikesCount = dd.LikesCount,
                                                      SpentTimeMC = dd.SpentTimeMC,
                                                      VisitsCount = dd.VisitsCount,
                                                      AnonymousCount = dd.AnonymousCount,
                                                      UsersCount = dd.UsersCount,
                                                      PostRatingJob = userRatingJob
                                                  }))
                                                  .OrderBy(f => f.TopDenseRank).ThenByDescending(f => f.PostId)
                                                 .ToList();

            //by posts
            var userPostsRatings = postRatings
                            .GroupBy(p => p.ApplicationUserId, p => p)
                            .Select(h => new
                                {
                                    ApplicationUserId = h.Key,
                                    Rating = h.Sum(hh => hh.Rating),
                                    CommentsCount = h.Sum(hh => hh.CommentsCount),
                                    LikesCount = h.Sum(hh => hh.LikesCount),
                                    SpentTimeMC = h.Sum(hh => hh.SpentTimeMC),
                                    VisitsCount = h.Sum(hh => hh.VisitsCount),
                                    AnonymousCount = h.Sum(hh => hh.AnonymousCount),
                                    UsersCount = h.Sum(hh => hh.UsersCount),
                                   
                                })
                                .OrderBy(u => u.Rating)
                                .Select((h, index) => new 
                                {
                                    DateTime = currentTime,
                                    ApplicationUserId = h.ApplicationUserId,
                                    Rating = h.Rating,
                                    CommentsCount = h.CommentsCount,
                                    LikesCount = h.LikesCount,
                                    SpentTimeMC = h.SpentTimeMC,
                                    VisitsCount = h.VisitsCount,
                                    AnonymousCount = h.AnonymousCount,
                                    UsersCount = h.UsersCount,
                                    TopOrder = index
                                }).ToList();

            //by friends
            var friends = _context.FriendsWithArxiv
                   .Where(v => v.Date >= d1 && v.Date <= d2);

            var userFriendRatings = (friends
                   .GroupBy(v => v.UserId, v => v)
                   .Select(k => new
                   {
                       ApplicationUserId = k.Key,
                       AddCount = k.Count() == 0 ? 0 : k.Where(kk => kk.Action == (int)FriendAction.Added).Count(),
                       DeleteCount = k.Count() == 0 ? 0 : k.Where(kk => kk.Action == (int)FriendAction.Deleted).Count(),
                       WasAddedCount = 0,
                       WasDeletedCount = 0
                   }))

                   .Concat //don't remove dublicates
                  (friends
                   .GroupBy(v => v.FriendId, v => v)
                   .Select(k => new
                   {
                       ApplicationUserId = k.Key,
                       AddCount = 0,
                       DeleteCount = 0,
                       WasAddedCount = k.Count() == 0 ? 0 : k.Where(kk => kk.Action == (int)FriendAction.Added).Count(),
                       WasDeletedCount = k.Count() == 0 ? 0 : k.Where(kk => kk.Action == (int)FriendAction.Deleted).Count()
                   }))

                   .GroupBy(u => u.ApplicationUserId, u => u)
                   .Select(h => new
                   {
                       ApplicationUserId = h.Key,
                       AddCount = h.Sum(hh => hh.AddCount),
                       DeleteCount = h.Sum(hh => hh.DeleteCount),
                       WasAddedCount = h.Sum(hh => hh.WasAddedCount),
                       WasDeletedCount = h.Sum(hh => hh.WasDeletedCount),
                   }).ToList();


            var usersIds = 
                (userPostsRatings.Select(u => u.ApplicationUserId).ToList())
                .Union
                (
                      userPostsRatings.Select(u => u.ApplicationUserId).ToList()
                ).ToList();
           
            // rating job
            var usersRatings = usersIds.GroupJoin(userFriendRatings, i1 => i1, i2 => i2.ApplicationUserId,
                    (i1, i2) => new
                    {
                        ApplicationUserId = i1,
                        AddCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().AddCount,
                        DeleteCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().DeleteCount,
                        WasAddedCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().WasAddedCount,
                        WasDeletedCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().WasDeletedCount,
                    })
                    .GroupJoin(userPostsRatings, i1 => i1.ApplicationUserId, i2 => i2.ApplicationUserId,
                    (i1, i2) => new
                    {
                        ApplicationUserId = i1.ApplicationUserId,
                        AddCount = i1.AddCount,
                        DeleteCount = i1.DeleteCount,
                        WasAddedCount = i1.WasAddedCount,
                        WasDeletedCount = i1.WasDeletedCount,
                        CommentsCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().CommentsCount,
                        LikesCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().LikesCount,
                        SpentTimeMC = i2.Count() == 0 ? 0 : i2.FirstOrDefault().SpentTimeMC,
                        VisitsCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().VisitsCount,
                        AnonymousCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().AnonymousCount,
                        UsersCount = i2.Count() == 0 ? 0 : i2.FirstOrDefault().UsersCount,
                        Rating =  (int)(i1.AddCount + i1.WasAddedCount - i1.DeleteCount - i1.WasAddedCount +
                                 (i2.Count() == 0 ? 0 : i2.FirstOrDefault().CommentsCount) +
                                 (i2.Count() == 0 ? 0 : i2.FirstOrDefault().LikesCount) +
                                 (i2.Count() == 0 ? 0 : i2.FirstOrDefault().VisitsCount) +
                                 (i2.Count() == 0 ? 0 : (i2.FirstOrDefault().SpentTimeMC/6000))) 
                                   

                    })
                    .GroupBy(r => r.Rating, r => r)
                                                   .Where(k => k.Key > 0)
                                                   .OrderByDescending(d => d.Key)
                                                  .SelectMany((d, index) => d.Select(dd => new UserRating
                                                  {
                                                      TopDenseRank = index + 1,
                                                      DateTime = currentTime,
                                                      FromDateTime = d1,
                                                      ToDateTime = d2,
                                                      Rating = d.Key,
                                                      ApplicationUserId = dd.ApplicationUserId,
                                                      CommentsCount = dd.CommentsCount,
                                                      LikesCount = dd.LikesCount,

                                                      SpentTimeMC = dd.SpentTimeMC,
                                                      VisitsCount = (int)dd.VisitsCount,
                                                      AnonymousCount = (int)dd.AnonymousCount,
                                                      UsersCount = (int)dd.UsersCount,


                                                      AddCount = dd.AddCount,
                                                      WasAddedCount = dd.WasAddedCount,
                                                      DeleteCount = dd.DeleteCount,
                                                      WasDeletedCount = dd.WasDeletedCount,
                                                      UserRatingJob = userRatingJob
                                                  })).ToList();

            userRatingJob.UsersCount = usersRatings.Count();
            userRatingJob.PostsCount = postRatings.Count();
             
            _context.PostRatings.AddRange(postRatings);
            _context.UserRatings.AddRange(usersRatings);
            _context.SaveChanges();  
            return 1;
        }




      //Save Visitor Action
      //============================================================
        public Int64 SaveVisitorActionLog(VisitorActionLog log)
        {
            if (log.PostId != 0)
            {
                var user = _context.Posts
                    .Where(p => p.Id == log.PostId)
                    .Include(h => h.ApplicationUser)
                    .FirstOrDefault();
                log.UserName = user == null ? "" : user.ApplicationUser.UserName;
            }
           
            var lastVisitorLog = _context.VisitorActionLogs
                          .Where(l =>  l.DateTime <= log.DateTime &&
                              l.SessionId == log.SessionId &&
                              l.Anonymous == log.Anonymous)
                          .OrderByDescending(t => t.DateTime).FirstOrDefault();
            if (lastVisitorLog != null)
            { 
              //TimeSpan spentTime = log.DateTime.Subtract(lastVisitorLog.DateTime);
              var spentTime = log.DateTime.Subtract(lastVisitorLog.DateTime).Ticks;
              lastVisitorLog.SpentTimeMC = spentTime;
              _context.Entry(lastVisitorLog).Property(e => e.SpentTimeMC).IsModified = true;
            }

            _context.VisitorActionLogs.Add(log);
            _context.SaveChanges();

            var id = log.Id;
            return id;

        }

        // Save Visitor start and and Session time
        //===========================================================
        public void SaveWebVisitor(WebVisitor wv)
        {
            if (wv != null)
            {
                string username = wv == null ? "" : wv.AuthUser;
                ApplicationUser user = username == "" ? null : _context.Users.Where(u => u.UserName == username).FirstOrDefault();
                ApplicationVisitor visitor = (ApplicationVisitor)wv;
                visitor.User = user;
                visitor.CountryId = wv.GeoLocation.country_code;
                visitor.CountryName = wv.GeoLocation.country_name;
                visitor.SessionEnded = DateTime.Now;
                _context.Visitors.Add(visitor);
                _context.SaveChanges();
            }
        }



        //===========================================================
        public int AddVisitorsToArxiv(DateTime d1, DateTime d2)
        {
            var av = _context.Visitors
                         .Where(v => v.SessionStarted >= d1 && v.SessionStarted <= d2)
                         .GroupBy(s => s.SessionStarted.Month, s => s)
                         .Select(f => new
                         {
                             Date = f.FirstOrDefault().SessionStarted,
                             VisitorCount = f.Count(),
                             UsersCount = f.Where(ff => ff.UserId != null)
                                                     .GroupBy(kk => kk.UserId, kk => kk.UserId)
                                                     .Select(pp => pp.Key).Count(),
                         }).FirstOrDefault();

            if (av == null) return 0;

            var arxivVisitor = new ArxivVisitor { 
                            Date = av.Date, 
                            VisitorsCount = av.VisitorCount, 
                            UsersCount = av.UsersCount 
            };
            _context.ArxivVisitors.Add(arxivVisitor);

            _context.SaveChanges();
            return arxivVisitor.Id;
        }
       


        //Jobs for imitation 
        //============================================================
        public void AddCommentsForJob()
        {
            var users = _context.Users.Include(u => u.UserImages).OrderBy(p => p.Id).ToList();

            var posts = _context.Posts
                .Where(p => p.SecurityStatus == (int)SecurityStatuses.Public)
                .Include(u => u.ApplicationUser);

            var usersCount = users.Count();
            var postsCount = posts.Count();
            Random random = new Random();

            foreach (var p in posts.ToList())
            {
                var postUserName = p.ApplicationUser.UserName;
                var postImageId = p.UserImageId;
                var userLeftComment = users.Skip(random.Next(0, 15)).FirstOrDefault();
                var userImages = userLeftComment.UserImages;
                var imageIds = userImages.Select(img => img.Id).ToArray();
                var isPostLike = random.Next(1, 4) != 2;

                Comment comment = new Comment
                {
                    PostId = p.Id,
                    UserImageId = imageIds[random.Next(0, userImages.Count() - 1)],
                    Body = "comment left by " + userLeftComment.UserName,
                    DateTime = DateTime.Now,
                    UserName = userLeftComment.UserName
                };

                _context.Comments.Add(comment);
            }

            var comments = _context.Comments.ToList();
            var commentsCount = comments.Count();
            for (int i = 1; i < random.Next(1, 3); i++)
            {
                var userReply = users.Skip(random.Next(0, 15)).FirstOrDefault();
                var cmt = comments.Skip(random.Next(0, commentsCount - 1)).FirstOrDefault();

                Reply reply = new Reply
                {
                    PostId = cmt.PostId,
                    CommentId = cmt.Id,
                    ParentReplyId = null,
                    DateTime = DateTime.Now,
                    UserName = userReply.UserName,
                    UserImageId = (int)userReply.DefaultUserImageId,
                    Body = "comment left by " + userReply.UserName,
                };
                _context.Replies.Add(reply);
            }
            _context.SaveChanges();

        }

    }
}