﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Reflection;
using LJ.Infrastructure;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.ViewModels;
using LJ.Repository.BlogRepository;
using LJ.ViewModels.User;
using System.Data.Entity.SqlServer;



namespace LJ.Repository.UserRepository
{
    public class UserRepository : IUserRepository<ApplicationUser, int>
    {
        private BlogDbContext _context;

        public UserRepository(BlogDbContext context)
        {
            _context = context;
        }

        // Profile
        //=============================================
        public ApplicationUser GetUserBy(string username)
        {
            ApplicationUser user = _context.Users
                .Where(u => u.UserName == username)
                .Include(k => k.DefaultUserImage)
                .Include(k => k.UserImages)
                .Include(k => k.ApplicationUserInfo)
                .Include(k => k.FriendsFor)
                .Include(k => k.FriendsWith)
                .Include(k => k.FriendGroups)
                .FirstOrDefault();
            return user;
        }


        //===============================================
        public UserProfile GetProfileBy(string username)
        {
            UserProfile up = new UserProfile();
            up.User = _context.Users
                .Where(u => u.UserName == username)
                .Include(k => k.DefaultUserImage)
                .Include(k => k.UserImages)
                .Include(k => k.ApplicationUserInfo)
                .Include(k => k.FriendsFor)
                .Include(k => k.FriendsWith)
                .Include(k =>k.FriendGroups)
                .FirstOrDefault();


            up.Statistics = GetUserStatistics(up.User);

            return up;
        }


        //==============================================
        public UserStatistics GetUserStatistics(ApplicationUser user)
        {
            UserStatistics us = new UserStatistics();

            IQueryable<Post> posts = _context.Posts
                      .Where(p => p.ApplicationUserId == user.Id && p.Published);

            us.Tags = (from p in posts.Include(pp => pp.Tags)
                       from t in p.Tags
                       select t)
                      .GroupBy(tt => tt, tt => tt)
                      .Select(k => new UserFilter { Id = k.Key.Id, Name = k.Key.Name, Count = k.Count() })
                      .OrderBy(kk => kk.Name);

            us.Categories = (posts
                             .Select(p => p.Category)
                             .GroupBy(c => c.Id, c => c)
                             .Select(cc => new { Id = cc.Key, Count = cc.Count() }))
                             .Join(_context.Categories, k1 => k1.Id, k2 => k2.Id,
                                      (k1, k2) => new UserFilter { Id = k2.Id, Name = k2.Name, Count = k1.Count })
                             .OrderBy(k => k.Name);

            var resived = posts
                    .Include(p => p.Comments)
                    .Include(p => p.Replies)
                    .Select(k => new { Comments = k.Comments.Count(), Replies = k.Replies.Count() })
                    .FirstOrDefault();

            us.ResivedComments = resived == null? 0 : resived.Comments;
            us.ResivedReplies = resived == null ? 0 : resived.Replies; 

            us.PostedComments = _context.Comments
                                    .Where(c => c.UserName == user.UserName && !posts.Any(pp => pp.Id == c.PostId))
                                    .Count();

            us.PostedReplies = _context.Replies
                                    .Where(c => c.UserName == user.UserName && !posts.Any(pp => pp.Id == c.PostId))
                                    .Count();

            us.PostsCount = posts.Count();
            return us;
        }


       
        //Images
        //===============================
        public int SaveImage(ApplicationUserImage img)
        {
            _context.UserImages.Add(img);
            _context.SaveChanges();
            return img.Id;
        
        }

        //===========================================
        public ApplicationUserImage GetImageBy(int id)
        {
            return (id != 0) ? _context.UserImages.Find(id) : null;
        }

        //=============================================
        public ApplicationUserImage GetDefaultImageBy(string username)
        {

            ApplicationUser user = _context.Users.Where(u => u.UserName.Equals(username)).Include("DefaultUserImage").FirstOrDefault();
            if (user != null && user.DefaultUserImage != null)
                return user.DefaultUserImage;
            else return null;
        }

        //=======================================
        public int SaveSectionUserpics(IEnumerable<ApplicationUserImage> newImages, 
                                       IEnumerable<ApplicationUserImage> images,
                                       UserViewModel userFromView, int defaultOrderId)
        {

            ApplicationUser originalUser = _context.Users.Where(i => i.UserName == userFromView.UserName)
            .Include(p => p.UserImages)
            .Include(p => p.DefaultUserImage)
            .Include(p => p.ApplicationUserInfo)
            .FirstOrDefault();

            if (originalUser == null) return 0;
            
            var original = originalUser.UserImages.ToList();
            
            //new defaultimageid
            
            var newDefaultImage = images.Where(i => i.ImageOrderId == defaultOrderId).FirstOrDefault() ??
                                             newImages.Where(i => i.ImageOrderId == defaultOrderId).FirstOrDefault();
           
            if (originalUser.DefaultUserImageId != newDefaultImage.Id)
            {
                originalUser.DefaultUserImage = newDefaultImage.Id == 0 ? newDefaultImage :
                                                newDefaultImage.Id == 1 ? _context.UserImages.Find(1) :
                                                           original.Where(i => i.Id == newDefaultImage.Id).FirstOrDefault();
                _context.Entry(originalUser).Property(p => p.DefaultUserImageId).IsModified = true;
            }

            //Update orderId in old images
            var forUpdate = original
                        .Join(images, i1 => i1.Id, i2 => i2.Id,
                           (i1, i2) => new { Original = i1, Updated = i2 })
                           .Where(k => k.Original.ImageOrderId != k.Updated.ImageOrderId)
                           .ToList();

            foreach (var i in forUpdate)
            {
                i.Original.ImageOrderId = i.Updated.ImageOrderId;
                _context.Entry(i.Original).Property(p => p.ImageOrderId).IsModified = true;
            }
           
            // Set up for Delete Images
            var forDelete = original
                             .GroupJoin(images.Where(i => i.Id != 0), // remove new img
                                i1 => i1.Id, i2 => i2.Id,
                                (i1, i2) => new { Original = i1, Deleted = i2.Count() })
                                 .Where(k => k.Deleted == 0)
                                 .ToList();

            var idsForDelete = forDelete.Select(a => a.Original.Id).ToList();

            // For image to delete set default image for posts 
            var postsForUpdate = _context.Posts
                                 .Where(p => idsForDelete.Any(c => c == p.UserImageId))
                                 .ToList();
            foreach (var c in postsForUpdate)
            {
                c.UserImage = originalUser.DefaultUserImage;
                _context.Entry(c).State = EntityState.Modified;
            }

            // For image to delete set default image for comments 
            var commentsForUpdate = _context.Comments
                                 .Where(p => idsForDelete.Any(c => c == p.UserImageId))
                                 .ToList();
            foreach (var c in commentsForUpdate)
            {
                c.UserImage = originalUser.DefaultUserImage;
                _context.Entry(c).State = EntityState.Modified;
            }
            
            // set default image for replies 
            var repliesForUpdate = _context.Replies
                                 .Where(p => idsForDelete.Any(c => c == p.UserImageId))
                                 .ToList();
            foreach (var c in repliesForUpdate)
            {
                c.UserImage = originalUser.DefaultUserImage;
                _context.Entry(c).Property(p => p.UserImageId).IsModified = true;
            }
               
            // Delete images
            foreach (var i in forDelete)
            {
                _context.Entry(i.Original).State = EntityState.Deleted;
            }

            //Add new images
            foreach (var i in newImages)
            {
                originalUser.UserImages.Add(i);
            }
            
            _context.SaveChanges();

            return (int)originalUser.DefaultUserImageId;       
        }
        


        //==============================================================
        public int SaveSectionAbout(string id, string About)
        {
            var userName = id;
            var user =_context.Users
                .Where(u => u.UserName == userName)
                .Include(a => a.ApplicationUserInfo)
                .FirstOrDefault();

            if (user == null) return 0;
            var originalUserInfo = user.ApplicationUserInfo;
            originalUserInfo.About = About;

            _context.Entry(originalUserInfo).Property(p => p.About).IsModified = true;
            _context.SaveChanges();
            return 1;
        }

        //=============================================================

        public int SaveSectionInfo(ApplicationUserInfo info, string UserName, int DefaultUserImageId)
        {
            var user = _context.Users
                .Where(u => u.UserName == UserName)
                .Include(a => a.ApplicationUserInfo)
                .FirstOrDefault();

            if (user == null) return 0;
            var originalUserInfo = user.ApplicationUserInfo;

            originalUserInfo.FirstName = info.FirstName;
            originalUserInfo.LastName = info.LastName;
            originalUserInfo.City = info.City;
            originalUserInfo.Country = info.Country;
            originalUserInfo.IsNotPublic = info.IsNotPublic;
            _context.Entry(originalUserInfo).State = EntityState.Modified;

           var originalUserImage =  _context.UserImages.Find(DefaultUserImageId);
           user.DefaultUserImage = originalUserImage;
            _context.Entry(user).State = EntityState.Modified;

            _context.SaveChanges();
            return 1;
        }

        //=============================================================
           public int DeleteFriend (string userNameToDelete, string username)
            {
                int result = 0;
                var originalUserToDelete = _context.Users
                    .Where(i => i.UserName == userNameToDelete)
                    .FirstOrDefault();

               if (originalUserToDelete == null) return result;

               var originalUser =_context.Users.Where(u => u.UserName == username)
                   .Include(p => p.FriendsWith)
                   .Include(p => p.FriendGroups)
                   .Include(p => p.FriendsWithArxiv)
                   .FirstOrDefault();

               foreach (var gr in originalUser.FriendGroups)
               {
                   gr.Friends.Remove(originalUserToDelete);
               }

               originalUser.FriendsWith.Remove(originalUserToDelete);
               FriendArxiv fa = new FriendArxiv { Friend = originalUserToDelete, User = originalUser, Date = DateTime.Now, Action = (short)FriendAction.Deleted };
               _context.FriendsWithArxiv.Add(fa);
               originalUser.FriendsWithArxiv.Add(fa);
               
               _context.SaveChanges();
               return 1;
            }


         //=======================================================
           public int AddFriend(string userNameToAdd, string username)
           {
               int result = 0;
              
               var originalUserToAdd = _context.Users
                   .Where(i => i.UserName == userNameToAdd)
                   .FirstOrDefault();
               if (originalUserToAdd == null) return result;

               var originalUser = _context.Users.Where(u => u.UserName == username)
                   .Include(p => p.FriendsWith)
                   .Include(p => p.FriendsWithArxiv)
                   .FirstOrDefault();
               FriendArxiv fa = new FriendArxiv { Friend = originalUserToAdd, User = originalUser, Date = DateTime.Now, Action = (short)FriendAction.Added };
               _context.FriendsWithArxiv.Add(fa);

               originalUser.FriendsWith.Add(originalUserToAdd);
               originalUser.FriendsWithArxiv.Add(fa);
               _context.SaveChanges();
               return 1;
           }
        
           

        //=======================================================
           public int GetUserType(string visitorUserName, string jornalUserName)
           {
               var friendType = _context.Users
                   .Where(u => u.UserName == jornalUserName)
                   .Include(p => p.FriendsFor)
                   .Include(p => p.FriendsWith)
                   .Select (u => (u.FriendsFor.Any(k => k.UserName == visitorUserName) ? (int)FriendType.Friend : 0) +
                                 (u.FriendsWith.Any(k => k.UserName == visitorUserName) ? (int)FriendType.FriendOf : 0))
                    .FirstOrDefault();
               return friendType;
           }


        //=====================================================
           public int GetUsersCount()
           {
               return _context.Users.Count();                   
           }


        //====================================================
          public UserAd GetAdByUserName(string username = "Pavarotti")
           {               
              return _context.UserAds
                             .Where(i => i.FileName == username)
                             .FirstOrDefault();           
           }


        //====================================================
           public ApplicationUser GetUserByName(string username)
           {
               return _context.Users
                              .Where(u => u.UserName == username)
                              .FirstOrDefault();           
           }


        //======================================================
           public StatisticsByFriendsViewModel GetManageFriends(string username)
           {
               var vm = new StatisticsByFriendsViewModel();
               var user = _context.Users
                   .Where(u => u.UserName == username)
                   .Include(p => p.FriendsWithArxiv)
                   .Include(p => p.FriendsWithArxiv.Select(k => k.Friend))
                   .Include(p => p.FriendGroups)
                   .FirstOrDefault();

               vm.User = user;
               if (user == null) return vm;

               vm.FriendsOfArxiv = _context.FriendsWithArxiv
                   .Where(f => f.FriendId == user.Id)
                   .Include(f => f.User);

               return vm;
           }


        //=======================================================================
           public ApplicationUser GetManageFriendsGroups(string username)
           {              
               var user = _context.Users
                   .Where(u => u.UserName == username)
                   .Include(p => p.FriendsWith)
                   .Include(p => p.FriendsFor)
                   .Include(p => p.FriendGroups.Select(k => k.Friends))
                   .Include(p => p.FriendGroups)
                   .FirstOrDefault();
               return user;
           }


        //===========================================
           public StatisticsByFriendsViewModel GetFriendsStatistics(string username )
           {
               var vm = new StatisticsByFriendsViewModel();
              
               var user = _context.Users
                   .Where(u => u.UserName == username)
                   .Include(p => p.FriendsWithArxiv)
                   .Include(p => p.FriendsWithArxiv.Select(k => k.Friend))
                   .FirstOrDefault();

               vm.User = user;
               if (user == null) return vm;

               vm.FriendsOfArxiv = _context.FriendsWithArxiv
                   .Where(f => f.FriendId == user.Id)
                   .Include(f => f.User);

               vm.StartDateRange = user.FriendsWithArxiv.Min( f => f.Date);

               return vm;

           }
        

           //===========================================
           public StatisticsByCommentsViewModel GetCommentsStatistics(string username, DateTime startDate, DateTime endDate)
           {
               var vm = new StatisticsByCommentsViewModel();
               var user = _context.Users
                  .Where(u => u.UserName == username).FirstOrDefault();

                var postIds = _context.Posts
                    .Where(p => p.ApplicationUserId == user.Id).Select(p => p.Id).ToList();
              
                //reseivedComments
                vm.ReceivedComments = (_context.Comments
                              .Where(c => postIds.Any(cc => cc == c.PostId) && c.DateTime <= endDate && c.DateTime >= startDate)
                              .Select(c => new { PostId = c.PostId, Day = c.DateTime.Day, UserName = c.UserName }))
                              .Union
                              (
                                 _context.Replies
                              .Where(c => postIds.Any(cc => cc == c.PostId) && c.DateTime <= endDate && c.DateTime >= startDate)
                              .Select(r => new  { PostId = r.PostId, Day = r.DateTime.Day, UserName = r.UserName })
                              )
                               .GroupBy(w => w.Day, w => w)
                        .Select(c => new PostStatisticsViewModel
                        {
                            Day = c.Key,
                            Count1 = c.Count(),//all comments
                            Count2 = c.GroupBy(k => k.PostId, k => k.PostId).Count(),//postsCount
                            Count3 = c.GroupBy(k => k.UserName, k => k.UserName).Count(),//usersCount
                            Count4 = c.Where(k => k.UserName == username).Count(),//yourCommentsCount
                        }).ToList();

               //Posted  commens
               vm.PostedComments = (_context.Comments
                        .Where(c => c.UserName == username && c.DateTime <= endDate && c.DateTime >= startDate)
                        .Select(c => new  { 
                                     PostId = c.PostId,                                
                                     Day = c.DateTime.Day,
                                     IsYourPost = postIds.Any(p => p == c.PostId)
                        }))
                        .Union
                        (
                            _context.Replies
                        .Where(c => c.UserName == username && c.DateTime <= endDate && c.DateTime >= startDate)
                        .Select(c => new  { 
                                            PostId = c.PostId, 
                                            Day = c.DateTime.Day,
                                            IsYourPost = postIds.Any(p => p == c.PostId)
                        }))
                        .GroupBy(w => w.Day, w => w)
                        .Select(c => new PostStatisticsViewModel { 
                                          
                                            Day = c.Key,
                                            Count1 = c.Count(),//all comments
                                            Count2 = c.Count() == 0 ? 0 :  c.Where(cc => cc.IsYourPost).Count(), // comments in your posts
                                            Count3 = c.GroupBy(p => p.PostId, p =>p.PostId).Count(), //postsCount
                                            Count4 = c.Where(k => k.IsYourPost).Count(), //yourpostsCount                  
                        }).ToList() ;
             
               return vm;

           }


        //======================================================
           public StatisticsByCommentsViewModel GetPostsStatistics(string username, DateTime startDate, DateTime endDate)
           {
               var vm = new StatisticsByCommentsViewModel();
               var user = _context.Users
                  .Where(u => u.UserName == username).FirstOrDefault();

               vm.Posts = _context.Posts
                  .Where(p => p.ApplicationUserId == user.Id);

               var postIds = vm.Posts.Select(p => p.Id).ToList();

               vm.PostLikes = (_context.PostLikes
                           .Where(l => postIds.Any(p => p == l.PostId))
                           .GroupBy(h => h.PostId, h => h)
                           .Select(r => new
                           {
                               Id = r.Key,
                               Count1 = r.Count() == 0 ? 0 : r.Where(gr => gr.Like).Count(),
                               Count2 = r.Count() == 0 ? 0 : r.Where(gr => gr.Dislike).Count(),
                           }))
                           .Join(_context.Posts, i1 => i1.Id, i2 => i2.Id,
                                 (i1, i2) => new PostStatisticsViewModel { Title = i2.Title, Count1 = i1.Count1, Count2 = i1.Count2, Id = i1.Id })
                           .OrderBy(k => k.Title).ToList();

               IEnumerable<Post> postnames = _context.Posts
                   .Where(p => p.ApplicationUserId == user.Id).Select(k => new Post { Id = k.Id, Title = k.Title });

               IQueryable<Comment> comments = _context.Comments
                              .Where(c => postIds.Any(cc => cc == c.PostId) && c.DateTime <= endDate && c.DateTime >= startDate);
               var comments1 = comments.ToList();

               IQueryable<Reply> replies = _context.Replies
                              .Where(c => postIds.Any(cc => cc == c.PostId) && c.DateTime <= endDate && c.DateTime >= startDate);
               var replies1 = replies.ToList();

               vm.Comments =
                   comments1.Select(c => new Comment { PostId = c.PostId, DateTime = c.DateTime, UserName = c.UserName })
                   .Union(replies1.Select(r => new Comment { PostId = r.PostId, DateTime = r.DateTime, UserName = r.UserName })).ToList();

               return vm;
           }


           //===========================================
           public IEnumerable<string> GetUsersAlfabet()
           {
               var usersAlfabet = _context.Users.Select(u => u.UserName.Substring(0, 1).ToUpper())
                   .Distinct()
                   .OrderBy(u => u);

               return usersAlfabet;
           }


           //========================
           public IEnumerable<ApplicationUser> GetUsersByFirstLetter(string letter, int perPage, int pageNumber)
           {
               IQueryable<ApplicationUser> users = _context.Users;
               if (letter.Count() == 1) 
               {
                   users = users.Where(u => u.UserName.Substring(0, 1).ToUpper() == letter.ToUpper());
               }
               users = users.OrderBy(u => u.UserName)
                   .Skip(perPage * (pageNumber - 1))
                  .Take(perPage)
                   .Include("ApplicationUserInfo");
               return users;
           }


           
           //========================
           public IEnumerable<ApplicationUser> GetUsers(int perPage, int pageNumber)
           {
               var users = _context.Users
                   .OrderBy(u => u.UserName)
                   .Skip(perPage * (pageNumber - 1))
                   .Take(perPage)
                   .Include("ApplicationUserInfo");
               return users;
           }

           public string GetRoleId(string Name)
           {
               var adminId = _context.Roles.Where(p => p.Name =="Admin").FirstOrDefault().Id;
               return adminId;
           }


          //==============================================================================
           public TopUsersViewModel GetUsersByAverageTopRating(int perPage, int pageNumber)
           {//GetAverageTopUsers

               var ratingJobs = _context.UserRatingJobs
                               .Where(p => p.PostsCount > 0);
                               
               var firstRatingJobs = ratingJobs.FirstOrDefault();
               var lastRatingJobs = ratingJobs.OrderByDescending(p => p.DateTime).FirstOrDefault();
               
               if (lastRatingJobs == null) return new TopUsersViewModel();
               var lastRatingJobsDate = lastRatingJobs.DateTime;
               var JobsCount = lastRatingJobs.Id - firstRatingJobs.Id + 1;

               var ratings1 = _context.UserRatings
                             .GroupBy(r => r.ApplicationUserId, r => r)
                             .Select(u => new
                             {
                                 ApplicationUserId = u.Key,
                                 Today = u.Where(f => f.UserRatingJobId == lastRatingJobs.Id).FirstOrDefault(),
                                 AverageRating = (decimal)(u.Sum(rr => rr.Rating)) / JobsCount 
                             }).ToList();

               var ratings = ratings1.GroupBy(r =>  Math.Round((decimal)r.AverageRating, 2), r => r)
                             .OrderByDescending(d => d.Key)
                             .SelectMany((d, index) => d.Select(dd => new 
                                                  {
                                                      ApplicationUserId = dd.ApplicationUserId,
                                                      TopDenseRank = index + 1,
                                                      TodayRating = dd.Today == null? 0 : dd.Today.Rating,
                                                      TodayOrder = dd.Today == null ? 0 : dd.Today.TopDenseRank,
                                                      AverageRating = d.Key                                                     
                                                  }))
                             .OrderBy(f => f.TopDenseRank)
                             .Skip(perPage * (pageNumber - 1))
                             .Take(perPage)                            
                             .ToList();

               var userRatings = ratings.Join(_context.Users, i1 => i1.ApplicationUserId, i2 => i2.Id,
                   (i1, i2) => new UserRatingViewModel
                   {
                       User = i2,
                       AverageRating = i1.AverageRating,
                       AverageTopOrder = i1.TopDenseRank,
                       Rating = i1.TodayRating,
                       TopOrder = i1.TodayOrder
                       
                   }).ToList();
              
               var vm = new TopUsersViewModel();
               vm.UserRatings = userRatings;
               vm.TopUsersCount = ratings1.Count(); 
                   
               return vm;
           }


        //======================================================
           public FrontPageViewModel GetInfoForFrontPage(int usercount, int postcount)
           {
               var lastRatingJob = _context.UserRatingJobs
                               .Where(p => p.PostsCount > 0)
                               .OrderByDescending(t => t.DateTime)
                               .FirstOrDefault();
               if (lastRatingJob == null) return null;

               var topPosts = _context.PostRatings
                               .Where(p => p.PostRatingJobId == lastRatingJob.Id)
                               .Include(j => j.Post)
                               .Where(j => j.Post.SecurityStatus == (long)SecurityStatuses.Public)
                               .OrderBy(p => p.Id)
                               .Take(postcount);                           
          
               var topUsers = _context.UserRatings
                              .Where(u => u.UserRatingJobId == lastRatingJob.Id)
                              .OrderBy(b => b.Id)
                              .Take(usercount)
                              .Include(u => u.User);

               var result = new FrontPageViewModel { TopUsers = topUsers , TopPosts = topPosts};
               return result;

           }

           
          //======================================================
           public TopUsersViewModel GetUsersByCurrentTopRating(int perPage, int pageNumber)
           {//today top users

               var lastRatingJob = _context.UserRatingJobs
                               .Where(p => p.PostsCount > 0)
                               .OrderByDescending(t => t.DateTime);

               var lastRatingJobs = lastRatingJob.FirstOrDefault();
               var prevRatingJobs = lastRatingJob.Skip(1).FirstOrDefault();

               var tt = _context.UserRatings
                   .Where(u => u.UserRatingJobId == lastRatingJobs.Id).ToList();

               var ratings1 = _context.UserRatings
                   .Where(u => u.UserRatingJobId == lastRatingJobs.Id).ToList();
               var ratingsCount = ratings1.Count();

               var ratings = ratings1.GroupJoin(_context.UserRatings.Where(u => u.UserRatingJobId == prevRatingJobs.Id),
                             i1 => i1.ApplicationUserId, i2 => i2.ApplicationUserId,
                             (i1, i2) => new
                             {
                                 ApplicationUserId = i1.ApplicationUserId,
                                 TopOrder = i1.TopDenseRank,
                                 Rating = i1.Rating,
                                 OldRating = i2.Count() == 0 ? 0 : i2.FirstOrDefault().Rating,
                                 OldTopOrder = i2.Count() == 0 ? 0 : i2.FirstOrDefault().TopDenseRank,
                             })
                             .OrderBy(uu => uu.TopOrder).ThenBy(uu => uu.OldTopOrder)
                             .Skip(perPage * (pageNumber - 1))
                             .Take(perPage);

               var userRatings = ratings.Join(_context.Users, i1 => i1.ApplicationUserId, i2 => i2.Id,
                   (i1, i2) => new UserRatingViewModel
                   {
                       User = i2,
                       Rating = i1.Rating,
                       OldRating = i1.OldRating,
                       TopOrder = i1.TopOrder,
                       OldTopOrder = i1.OldTopOrder
                   }
                   ).ToList();

               var vm = new TopUsersViewModel();
               vm.UserRatings = userRatings;
               vm.TopUsersCount = ratingsCount;

               return vm;
           }



        //======================================================
           public IEnumerable<PostStatisticsViewModel> GetUsersRatingsChart(DateTime d1, DateTime d2, string username)
           {
             //test d1 = d1.AddMonths(-1); d2 = d2.AddMonths(-1);

               var topRatings = (_context.UserRatingJobs
                  .Where(v =>v.UsersCount !=0 && v.DateTime >= d1 && v.DateTime <= d2)
                  .GroupBy(v => v.DateTime.Day)
                  .Select(k => new { Day = k.Key, JobsCount = k.Count() }))

                  .Join(
                   (_context.UserRatings.Where(v => v.DateTime >= d1 && v.DateTime <= d2)
                           .GroupBy(s => s.DateTime.Day, s => s)
                           .Select(f => new
                           {
                               Day = f.Key,
                               Count3 = f.Where(v => v.TopDenseRank == 1).Sum(ff => ff.Rating), //topRating,
                               Count4 = f.GroupBy(ff => ff.DateTime).Select(g => g.Max(gg => gg.TopDenseRank)).Sum(ggg => ggg) // min top order

                           })),
                   i1 => i1.Day, i2 => i2.Day,
                (i1, i2) => new
                {
                    Day = i2.Day,
                    Count3 = i2.Count3 / i1.JobsCount, //rating,
                    Count4 = (int)Math.Ceiling((double)i2.Count4 / i1.JobsCount), //min order,
                    JobsCount = i1.JobsCount
                });



              var user = _context.Users.Where(p => username == p.UserName).FirstOrDefault();
              var ratings = topRatings

                  .GroupJoin(

              (_context.UserRatings.Where(v => user.Id == v.ApplicationUserId && v.DateTime >= d1 && v.DateTime <= d2)
                           .GroupBy(s => s.DateTime.Day, s => s)
                           .Select(f => new
                           {
                               Day = f.Key,
                               Count1 = f.Sum(ff => ff.Rating), //rating,
                               Count2 = f.Sum(ff => ff.TopDenseRank) // order
                           })),
               i1 => i1.Day, i2 => i2.Day,
               (i1, i2) => new PostStatisticsViewModel
               {

                   Day = i1.Day,
                   Count1 = i2.Count() == 0? 0 :i2.FirstOrDefault().Count1 / i1.JobsCount, //rating,
                   Count2 = (int)Math.Ceiling((double)(i2.Count() == 0 ? 0
                               : i2.FirstOrDefault().Count2) / i1.JobsCount), // order
                   Count3 = i1.Count3, // top ratings
                   Count4 = i1.Count4 // last top order
               }).ToList();

               return ratings;

           }
        //============================================================
           public RatingsViewModel GetPostRatingsMonthChart(DateTime d1, DateTime d2, int id)
           {   
               var post = _context.Posts.Where(p => id == p.Id)
                            .Include(p => p.ApplicationUser)     
                            .FirstOrDefault();

                var ratings = _context.UserRatingJobs
                     .Where(v =>v.DateTime >= d1 && v.DateTime <= d2)
                     .GroupJoin(_context.PostRatings
                                     .Where(v => v.DateTime >= d1 && v.DateTime <= d2),
                                     i1 => i1.DateTime, i2=> i2.DateTime,
                                     (i1, i2) =>  new 
                                       {
                                           DateTime = i1.DateTime,
                                           TopDenseRankFirst = i1.PostsCount  == 0 ? 0 : 1,
                                           TopDenseRankLast = i1.PostsCount == 0 ? 0 : i2.Max(d => d.TopDenseRank),
                                           RatingMax = i1.PostsCount == 0 ? 0 : i2.Max(d => d.Rating),
                                           RatingMin = i1.PostsCount == 0 ? 0 : i2.Min(d => d.Rating),
                                       }

                                )
                     .GroupJoin( _context.PostRatings
                                     .Where(v => id == v.PostId && v.DateTime >= d1 && v.DateTime <= d2),
                            
                        i1 => i1.DateTime, i2 => i2.DateTime,
                        (i1, i2) => new PostStatisticsViewModel
                       {

                           DateTime = i1.DateTime,
                              Count1 = i2.Count() == 0? 0 : i2.FirstOrDefault().Rating, //rating,
                              Count2 = i2.Count() == 0? 0 : i2.FirstOrDefault().TopDenseRank,
                              Count3 = i1.RatingMin, // top ratings Max
                              Count4 = i1.RatingMax, // last top order
                              Count5 = i1.TopDenseRankFirst,
                              Count6 = i1.TopDenseRankLast,
                }).ToList();

                RatingsViewModel vm = new RatingsViewModel();
                vm.Post = post;
                vm.RatingsInfo = ratings;

               return vm;

           }



        //============================================================
           public IEnumerable<PostStatisticsViewModel> GetUsersRatingsMonthChart(DateTime d1, DateTime d2, string username)
           {
               
               var user = _context.Users.Where(p => username == p.UserName).FirstOrDefault();

                var ratings = _context.UserRatingJobs
                     .Where(v =>v.DateTime >= d1 && v.DateTime <= d2)
                     .GroupJoin(_context.UserRatings
                                     .Where(v => v.DateTime >= d1 && v.DateTime <= d2),
                                     i1 => i1.DateTime, i2=> i2.DateTime,
                                     (i1, i2) =>  new 
                                       {
                                           DateTime = i1.DateTime,
                                           TopDenseRankFirst = i1.UsersCount  == 0 ? 0 : 1,
                                           TopDenseRankLast = i1.UsersCount == 0 ? 0 : i2.Max(d => d.TopDenseRank),
                                           RatingMax = i1.UsersCount == 0 ? 0 : i2.Max(d => d.Rating),
                                           RatingMin = i1.UsersCount == 0 ? 0 : i2.Min(d => d.Rating),
                                       }

                                )
                     .GroupJoin( _context.UserRatings
                                     .Where(v => user.Id == v.ApplicationUserId && v.DateTime >= d1 && v.DateTime <= d2),
                            
                        i1 => i1.DateTime, i2 => i2.DateTime,
                        (i1, i2) => new PostStatisticsViewModel
                       {

                           DateTime = i1.DateTime,
                              Count1 = i2.Count() == 0? 0 : i2.FirstOrDefault().Rating, //rating,
                              Count2 = i2.Count() == 0? 0 : i2.FirstOrDefault().TopDenseRank,
                              Count3 = i1.RatingMin, // top ratings Max
                              Count4 = i1.RatingMax, // last top order
                              Count5 = i1.TopDenseRankFirst,
                              Count6 = i1.TopDenseRankLast,
                }).ToList();

               return ratings;

           }



        //============================================================
         public IEnumerable<PostStatisticsViewModel> GetAllUsersRatingsMonthChart(DateTime d1, DateTime d2)
           {
                var ratings = _context.UserRatingJobs
                     .Where(v =>v.DateTime >= d1 && v.DateTime <= d2)
                     .GroupJoin(_context.UserRatings
                                     .Where(v => v.DateTime >= d1 && v.DateTime <= d2),
                                     i1 => i1.DateTime, i2=> i2.DateTime,
                                     (i1, i2) => new PostStatisticsViewModel
                                       {
                                           DateTime = i1.DateTime,
                                           Count3 = i1.UsersCount == 0 ? 0 : i2.Min(d => d.Rating),
                                           Count4 = i1.UsersCount == 0 ? 0 : i2.Max(d => d.Rating),
                                           Count5 = i1.UsersCount == 0 ? 0 : 1,
                                           Count6 = i1.UsersCount == 0 ? 0 : i2.Max(d => d.TopDenseRank),
                                       }
                                )
                     .ToList();

               return ratings;
           }

         //============================================================
         public DateTime GetAllUsersRatingsLastJob()
           {
               var date = _context.UserRatingJobs.Where(u => u.PostsCount > 0).Min(u => u.DateTime);
               return date;
           }

         //============================================================
           public IEnumerable<PostStatisticsViewModel> GetAllPostsRatingsMonthChart(DateTime d1, DateTime d2)
           {
               var ratings = _context.UserRatingJobs
                    .Where(v => v.DateTime >= d1 && v.DateTime <= d2  )
                    .GroupJoin(_context.PostRatings
                                    .Where(v => v.DateTime >= d1 && v.DateTime <= d2),
                                    i1 => i1.DateTime, i2 => i2.DateTime,
                                    (i1, i2) => new PostStatisticsViewModel
                                    {
                                        DateTime = i1.DateTime,
                                        Count3 = i1.UsersCount == 0 ? 0 : i2.Min(d => d.Rating),
                                        Count4 = i1.UsersCount == 0 ? 0 : i2.Max(d => d.Rating),
                                        Count5 = i1.UsersCount == 0 ? 0 : 1,
                                        Count6 = i1.UsersCount == 0 ? 0 : i2.Max(d => d.TopDenseRank),
                                    }
                               )
                    .ToList();

               return ratings;

           }
           //============================================================
           public ApplicationUser GetFriendsForListBoxes(string username)
           {
               var user = _context.Users.Where(u => u.UserName == username)
                          .Include(u => u.FriendsWith)
                          .Include(u => u.FriendGroups)
                          .FirstOrDefault();
                   return user;
           }


           //============================================================
           public ApplicationUser GetGroupForListBoxes(string username, int groupid)
           {
               var user = _context.Users.Where(u => u.UserName == username)
                          .Include(u => u.FriendsWith)
                          .Include(u => u.FriendGroups)
                          .FirstOrDefault();
               return user;
           }

      
        //===========================================================
           public void DeleteFriends(string username, IEnumerable<string> names)
           {
               var usersToDelete = _context.Users.Where(u => names.Any(n => n == u.UserName)).ToList();
               var user = _context.Users.Where(u => u.UserName == username)
                           .Include(u => u.FriendsWith)
                           .Include(u => u.FriendGroups)
                           .Include(p => p.FriendsWithArxiv)
                           .FirstOrDefault();

               List<FriendArxiv> fa= new List<FriendArxiv>();
               foreach (var u in usersToDelete)
               {
                   user.FriendsWith.Remove(user);
                   foreach (var gr in user.FriendGroups)
                   {
                       gr.Friends.Remove(u);
                   }
                   fa.Add(new FriendArxiv { Friend = u, User = user, Date = DateTime.Now, Action = (short)FriendAction.Deleted });
               };
               _context.FriendsWithArxiv.AddRange(fa);
               _context.SaveChanges();
               return ;
           }


           //===========================================================
           public int DeleteGroup(string username, int groupid )
           {
               var user = _context.Users.Where(u => u.UserName == username)
                           .Include(u => u.FriendGroups)
                           .FirstOrDefault();

               var group = user.FriendGroups.Where(g => g.Id == groupid).FirstOrDefault();
               if (group == null) return 0; 

               group = _context.Groups.Where(gr => gr.Id == groupid).Include(p =>p.Posts).FirstOrDefault();
               if (group == null) return 0;
               var postsCount = group.Posts.Count();
               if (postsCount > 0)
               {
                   return -postsCount;
               }

               _context.Entry(group).State = EntityState.Deleted;
               _context.SaveChanges();
               return group.Id;
           }


        //==============================================================
           public void AddOrEditGroup(string username,string groupname, int groupid, IEnumerable<string> names)
           {
               var usersToAdd = names == null || names.Count() == 0 ? null :_context.Users
                    .Where(u => names.Any(n => n == u.UserName)).ToList();

               var user = _context.Users
                   .Where(u => u.UserName == username)
                   .Include(u => u.FriendGroups)
                   .FirstOrDefault();

               if (groupid == 0)
               {                 
                   Group newGroup = new Group { Friends = usersToAdd, Name = groupname };
                   _context.Entry(newGroup).State = EntityState.Added;
                   user.FriendGroups.Add(newGroup);
               }
               else
               { 
                    var originalGroup = _context.Groups.Where(u => u.Id == groupid)
                           .Include(u => u.Friends)
                           .FirstOrDefault();
                   
                   var friendsToDelete = usersToAdd == null ? originalGroup.Friends.ToList() : originalGroup.Friends.Except(usersToAdd).ToList();
                   var friendsToAdd = usersToAdd == null ? null : usersToAdd.Except(originalGroup.Friends).ToList();

                   if (friendsToDelete != null)
                   {
                       foreach (var f in friendsToDelete)
                       {
                           originalGroup.Friends.Remove(f);
                       }
                   }

                   if (friendsToAdd != null)
                   {
                       foreach (var f in friendsToAdd)
                       {
                           originalGroup.Friends.Add(f);
                       }
                   }

                   if (originalGroup.Name != groupname)
                   {
                       originalGroup.Name = groupname;
                       _context.Entry(originalGroup).State = EntityState.Modified;
                   }
               }
               _context.SaveChanges();
               return ;
           }



        //===============================================================
           public void CreateProfile(ApplicationUser user)
           {
               user.DefaultUserImageId = 1;
               var newUser = _context.Users.Add(user);
              
               _context.SaveChanges();
           
           }


           //===============================================================
           public DateTime GetUserFirstPostDate(string username)
           {
              var startdate = _context.Posts.Where( u => u.ApplicationUserId == username)
                               .Min(pp => pp.PostedOn);
              return startdate;

           }

           //===============================================================
           public IEnumerable<string> GetPostCommentsDates(string userid)
           {
               var dates = _context.Posts.Where(u => u.ApplicationUserId == userid).Include(p =>p.Comments)
                                .FirstOrDefault().Comments
                                .GroupBy(p => new { Key1 = p.DateTime.Month, Key2 = p.DateTime.Year })
                                .Select(h => h.Key.Key1.ToString() + ":" + h.Key.Key2.ToString());
               return dates;

           }

           //===============================================================
           public DateTime GetPostCommentsStartDate(string userid)
           {
               var date = _context.Posts.Where(u => u.ApplicationUserId == userid).Include(p => p.Comments)
                                .FirstOrDefault().Comments.Min(p => p.DateTime);
                                
               return date;

           }

        //===============================================================
    }

}