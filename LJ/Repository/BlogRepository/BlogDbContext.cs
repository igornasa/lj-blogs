﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.Models.Visitor;

namespace LJ.Repository.BlogRepository
{
    public class BlogDbContext : IdentityDbContext<ApplicationUser>  
    {        
        public DbSet<ApplicationUserInfo> UserInfos { get; set; }
        public DbSet<ApplicationUserImage> UserImages { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<FriendArxiv> FriendsWithArxiv { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Reply> Replies { get; set; }
        public DbSet<PostLike> PostLikes { get; set; }
        public DbSet<CommentLike> CommentLikes { get; set; }
        public DbSet<ReplyLike> ReplyLikes { get; set; }       
        public DbSet<UserAd> UserAds { get; set; }
        public DbSet<ApplicationVisitor> Visitors { get; set; }
        public DbSet<ArxivVisitor> ArxivVisitors { get; set; }
        public DbSet<PostRating> PostRatings { get; set; }
        public DbSet<UserRating> UserRatings { get; set; }
        public DbSet<UserRatingJob> UserRatingJobs { get; set; }
        public DbSet<VisitorActionLog> VisitorActionLogs { get; set; }
        public DbSet<ArxiveVisitorLogForUser> ArxiveVisitorLogsForUser { get; set; }
        public DbSet<ArxiveVisitorLogForAdmin> ArxiveVisitorLogsForAdmin { get; set; }

      
      
        public BlogDbContext(): base("DefaultConnection") { }

        public static BlogDbContext Create()
        {
            return new BlogDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>()
                .ToTable("Users");
            modelBuilder.Entity<ApplicationUserInfo>()
                .ToTable("UserInfos");

             modelBuilder.Entity<ArxiveVisitorLogForUser>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ArxiveVisitorLogForAdmin>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<VisitorActionLog>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ArxivVisitor>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ApplicationUserInfo>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Group>()
               .Property(e => e.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<FriendArxiv>()
               .Property(e => e.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<PostLike>()
              .Property(e => e.Id)
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<CommentLike>()
              .Property(e => e.Id)
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ReplyLike>()
             .Property(e => e.Id)
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ApplicationUserImage>()
               .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Comment>()
              .Property(e => e.Id)
              .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Reply>()
               .Property(e => e.Id)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Tag>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Category>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Post>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<PostRating>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<UserRating>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<UserRatingJob>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
     
            modelBuilder.Entity<Post>()
              .HasMany<Tag>(post => post.Tags)
              .WithMany(tag => tag.Posts)
              .Map(pt =>
              {
                  pt.MapLeftKey("PostRefId");
                  pt.MapRightKey("TagRefId");
                  pt.ToTable("PostVsTag");
              });
        
            modelBuilder.Entity<ApplicationUser>()
              .HasMany<ApplicationUser>(u => u.FriendsFor)
              .WithMany(f => f.FriendsWith)
              .Map(pt =>
              {
                  pt.MapLeftKey("UserId");
                  pt.MapRightKey("FriendUserId");
                  pt.ToTable("UserVsFriendFor");
              });

            modelBuilder.Entity<ApplicationUser>()
             .HasMany<Group>(u => u.FriendGroups)
             .WithMany(f => f.Users)
             .Map(pt =>
             {
                 pt.MapLeftKey("UserId");
                 pt.MapRightKey("GroupId");
                 pt.ToTable("UserVsGroup");
             });
          
            modelBuilder.Entity<Group>()
            .HasMany<ApplicationUser>(u => u.Friends)
            .WithMany(f => f.FriendOfGroups)
            .Map(pt =>
            {
                pt.MapLeftKey("GroupId");
                pt.MapRightKey("UserId");
                pt.ToTable("GroupVsUserInGroup");
            });

            modelBuilder.Entity<Post>()
              .HasMany<Group>(u => u.Groups)
              .WithMany(f => f.Posts)
              .Map(pt =>
               {
                 pt.MapLeftKey("PostId");
                 pt.MapRightKey("GroupId");
                 pt.ToTable("PostVsGroup");
              });

            modelBuilder.Entity<Post>()   
               .HasRequired(c => c.ApplicationUser)
               .WithMany()
               .HasForeignKey(p => p.ApplicationUserId) 
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationVisitor>()   
               .HasOptional(c => c.User)
               .WithMany()
               .HasForeignKey(p => p.UserId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Comment>()
              .HasRequired(cmt => cmt.UserImage)
              .WithMany()
              .HasForeignKey(cmt => cmt.UserImageId)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<Reply>()
             .HasRequired(r => r.UserImage)
             .WithMany()
             .HasForeignKey(r => r.UserImageId)
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<Post>()
               .HasRequired(c => c.Category)
               .WithMany()
               .HasForeignKey(p => p.CategoryId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Comment>()
               .HasRequired(c => c.Post)
               .WithMany()
               .HasForeignKey(p => p.PostId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Reply>()
               .HasRequired(r => r.Post)
               .WithMany()
               .HasForeignKey(p => p.PostId)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Reply>()
               .HasRequired(r => r.Comment)
               .WithMany()
               .HasForeignKey(p => p.CommentId)
               .WillCascadeOnDelete(false);

            // one to many + self reference  
            modelBuilder.Entity<Reply>()
               .HasOptional(r => r.ParentReply)
               .WithMany()
               .HasForeignKey(p => p.ParentReplyId)
               .WillCascadeOnDelete(false);
        }
    }
}