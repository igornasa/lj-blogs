﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LJ
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                     "Category", "Category/{category}",
                     new { controller = "Blog", action = "GetCategory" }
           );
            routes.MapRoute(
                    "Tag", "Tag/{tag}",
                    new { controller = "Blog", action = "GetTag" }
          );

            routes.MapRoute(
                    "Search", "Search/{search}",
                    new { controller = "Blog", action = "GetSearch" }
          );

            routes.MapRoute(
                    "AddTag", "Admin/AddTag",
                    new { controller = "Admin", action = "EditTag" }
          );

            routes.MapRoute(
                   "AddPost", "Admin/AddPost",
                   new { controller = "Admin", action = "EditPost" }
         );

            routes.MapRoute(
                   "AddCategory", "Admin/AddCategory",
                   new { controller = "Admin", action = "EditCategory" }
         );

          routes.MapRoute(
                     "Post",  "Archive/{year}/{month}/{url}",
                     new { controller = "Blog", action = "GetPost" }
         );

          routes.MapRoute(
             "Manage", "Manage", new { controller = "Admin", action = "Manage" }
          );

          routes.MapRoute(
            "GetBloggers", "Home/Bloggers", new { controller = "User", action = "GetUsers" }
         );

          routes.MapRoute(
           " GetUsers", "Home/Bloggers", new { controller = "User", action = "GetUsers" }
         );

          routes.MapRoute(
            " GetPostsByUserName", "Blog/BloggerPosts", new { controller = "Home", action = "GetPostsByUserName" }
         );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",                  
                defaults: new { controller = "Blog", action = "GetTopPosts", id = UrlParameter.Optional }
               
            );
        }
    }
}
