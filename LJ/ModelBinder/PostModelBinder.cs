﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using LJ.Models.Blog;
using LJ.Repository.BlogRepository;
using LJ.Infrastructure;

namespace LJ.ModelBinder
{
    public class PostModelBinder : DefaultModelBinder
    {
       // private IBlogRepository<Post, int> _blogRepository;

        public PostModelBinder()
        {
           // IKernel kernel = new StandardKernel();
           // _blogRepository = kernel.Get<IBlogRepository<Post, int>>();
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
       
        {            
            Post post = (Post)base.BindModel(controllerContext, bindingContext);
            if (post.Tags == null || post.Tags.Count == 0)
            {
                var strTag = bindingContext.ValueProvider.GetValue("Tags");
                string[] tt = {};  
  
                if (strTag != null) tt = strTag.AttemptedValue.Split(',');
                
                if (tt.Length > 0)
                {
                    List<Tag> tags = new List<Tag>();
                     foreach ( var t in tt)
                    {
                    
                          tags.Add ( new Tag {Id= int.Parse(t.Trim())});
                    }

                     post.Tags = tags;
                }
            }
                   
            return post;
        }
    }
}