﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LJ.Models.Visitor;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using Hangfire;
using Hangfire.Dashboard;
using LJ.Controllers;


namespace LJ.Filters
{
    public class VisitorActionFilter : ActionFilterAttribute, IActionFilter
    {
        //Positional and named parameters for an attribute class are limited 
        //to the following attribute parameter types
        //https://msdn.microsoft.com/en-us/library/7kk5syz6(VS.90).aspx

        public string UserName = "";
        public string PostId = "";
        public int ActionType = 0;
       

      void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
      {
          
          int postId = 0;
          if (!String.IsNullOrEmpty(PostId) && filterContext.ActionParameters.ContainsKey(PostId))
          {
              int? id = filterContext.ActionParameters[PostId] as  Int32?;
              postId = id.GetValueOrDefault();
          }

          string userName = "";
          if (!String.IsNullOrEmpty(UserName) && filterContext.ActionParameters.ContainsKey(UserName))
          {
              userName = (filterContext.ActionParameters[UserName] as string) ?? "";
             
          }

          var categoryId = filterContext.ActionParameters.ContainsKey("categoryid")?
               (filterContext.ActionParameters["categoryid"] as Int32?).GetValueOrDefault() : 0;

          var tagId = filterContext.ActionParameters.ContainsKey("tagid") ?
               (filterContext.ActionParameters["tagid"] as Int32?).GetValueOrDefault() : 0;

          VisitorActionLog log = new VisitorActionLog()
          {
            Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
            Action = filterContext.ActionDescriptor.ActionName,
            IpAddress = filterContext.HttpContext.Request.UserHostAddress,
            DateTime = filterContext.HttpContext.Timestamp,
            VisitorUserName = filterContext.HttpContext.User.Identity.Name,
            Anonymous = filterContext.HttpContext.Request.AnonymousID,
            UserName = userName,
            ActionType = ActionType,
            PostId = postId,
            TagId = tagId,
            CategoryId = categoryId,
            SessionId = filterContext.HttpContext.Session.SessionID
         };
       
         BackgroundJob.Enqueue(() => VisitorController.JobSaveVisitorActionLog(log));
         base.OnActionExecuting(filterContext);
      }   
   }
}