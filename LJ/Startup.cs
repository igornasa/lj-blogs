﻿using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.Dashboard;
using LJ.Controllers;
using LJ.Filters;
using System;
using LJ.Repository.BlogRepository;


[assembly: OwinStartupAttribute(typeof(LJ.Startup))]
namespace LJ
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);          
            using (var context = new BlogDbContext())
            {
                context.Database.Initialize(false);
            }
           
           SetJobs(app);
        }



        private void SetJobs(IAppBuilder app)
        {
         GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");

           app.UseHangfireDashboard("/Hangfire", new DashboardOptions()
           {
               Authorization = new[] { new HangFireAuthorizationFilter() }
           });

           app.UseHangfireServer();
 
           RecurringJob.AddOrUpdate(() => VisitorController.JobRatings(), VisitorController.CRON_STRING_Rating);
           RecurringJob.AddOrUpdate(() => VisitorController.JobAddVisitorLogsToArxives(), VisitorController.CRON_STRING_VisitorLogsToArxives);
        
        }
    }
}
