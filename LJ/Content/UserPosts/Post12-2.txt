﻿В 1998 году книга "Старая леди, которая проглотила муху" с иллюстрациями Симмсa Табакa получила  премию "Caldecott Honor Book".  
<br/><br/>
<img src="http://farm8.staticflickr.com/7237/7160948695_dd97066164_n.jpg" width="320" alt="Untitled">
<br/><br/>

<br/><br/>


          
 Награды Калдекотта вручаются Ассоциацией американских библиотек и являются  одними из самых престижных премий в детской литературе в США.
<br/> 
<i>С 1938 года  премией Калдекотта ( Caldecott Honor или Caldecott Honor Book) награждаются "выдающиеся иллюстрированные детские произведения", a  медаль Калдекотта (Caldecott Medal)  получает "самое выдающееся произведениe".</i>  

  <br/><br/>           

<a href="http://farm8.staticflickr.com/7088/7345044666_0788f4c960_b.jpg"><img src="http://farm8.staticflickr.com/7088/7345044666_0788f4c960.jpg" width="500" height="414" alt="Untitled"></a>
<br/><br/>


<br/><br/><br/>

<h2><b>Scholastic Video Collection</b></h2> 
"Scholastic Video Collection" —  серия, состоящая из видео-адаптаций детских книг, которые были награждены различными призами. 
<br/><br/><br/>

Мультфильм, созданный   на основе книги Симмса Табака.
<br/><br/>

<iframe width="480" height="360" src="https://www.youtube.com/embed/sCaYt1KxZiY" frameborder="0" allowfullscreen></iframe>

<br/><br/>

<b><a href="http://video.nhptv.org/program/caldecott-literature-series">Здесь</a></b> можно посмотреть еще 27 лауреатов Калдекотта из "Scholastic Video Collection". 
<br/><br/><br/>


<h2><b>Cтихотворение</b></h2>

Kнига Симмса Табака иллюстрирует  стихотворение "There was an old lady who swallowed a fly", получившее известность  в США в 1940 годы. 
<br/>
<a href="http://farm8.staticflickr.com/7216/7160114721_8c42c59c4a_n_b.jpg"><img src="http://farm8.staticflickr.com/7216/7160114721_8c42c59c4a_n.jpg" width="258" height="320" alt="Untitled"></a>
<br/><br/><br/>

Несколько различных версий, собранные в штатах Джорджия, Колорадо и  Огайо, были в 1947 году опубликованы в "Hoosier Folklore". Подлинный автор произведения до сих пор не известен.
<br/><br/>
<div style="overflow: auto; width: 500px; height: 180px;"> 
<i>
There was an old lady who swallowed a fly.<br/>
I dunno why she swallowed that fly,<br/>
Perhaps she'll die.<br/><br/>

There was an old lady who swallowed a spider,<br/>
That wiggled and wiggled and tickled inside her.<br/>
She swallowed the spider to catch the fly.<br/>
But I dunno why she swallowed that fly -<br/>
Perhaps she'll die.<br/><br/>

There was an old lady who swallowed a bird;<br/>
How absurd, to swallow a bird!<br/>
She swallowed the bird to catch the spider<br/>
That wiggled and wiggled and tickled inside her.<br/>
She swallowed the spider to catch the fly.<br/>
But I dunno why she swallowed that fly -<br/>
Perhaps she'll die<br/><br/>

There was an old lady who swallowed a cat.<br/>
Imagine that, she swallowed a cat.<br/>
She swallowed the cat to catch the bird ...<br/>
She swallowed the bird to catch the spider<br/>
That wiggled and wiggled and tickled inside her.<br/>
She swallowed the spider to catch the fly.<br/>
But I dunno why she swallowed that fly<br/>
Perhaps she'll die<br/><br/>

There was an old lady who swallowed a dog.<br/>
What a hog! To swallow a dog!<br/>
She swallowed the dog to catch the cat...<br/>
She swallowed the cat to catch the bird ...<br/>
She swallowed the bird to catch the spider<br/>
That wiggled and wiggled and tickled inside her.<br/>
She swallowed the spider to catch the fly.<br/>
But I dunno why she swallowed that fly<br/>
Perhaps she'll die.<br/><br/>

There was an old lady who swallowed a goat.<br/>
Just opened her throat and swallowed a goat!<br/>
She swallowed the goat to catch the dog ...<br/>
She swallowed the dog to catch the cat.<br/>
She swallowed the cat to catch the bird ...<br/>
She swallowed the bird to catch the spider<br/>
That wiggled and wiggled and tickled inside her.<br/>
She swallowed the spider to catch the fly.<br/>
But I dunno why she swallowed that fly<br/>
Perhaps she'll die.<br/><br/>

There was an old lady who swallowed a cow.<br/>
I don't know how she swallowed a cow!<br/>
She swallowed the cow to catch the goat...<br/>
She swallowed the goat to catch the dog...<br/>
She swallowed the dog to catch the cat...<br/>
She swallowed the cat to catch the bird ...<br/>
She swallowed the bird to catch the spider<br/>
That wiggled and wiggled and tickled inside her.<br/>
She swallowed the spider to catch the fly.<br/>
But I dunno why she swallowed that fly<br/>
Perhaps she'll die.<br/><br/>

There was an old lady who swallowed a horse -<br/>
She's dead, of course.<br/></i>
</div>
<br/><br/>

 

Стихотворение рассказывает  о старой леди, которая проглотила муху,
<br/><br/>
<a href="http://farm8.staticflickr.com/7102/7351539330_1e3ace3a8a_b.jpg"><img src="http://farm8.staticflickr.com/7102/7351539330_1e3ace3a8a.jpg" width="500" height="215" alt=""></a>
<br/><br/><br/><br/><br/>

затем паука, чтoбы он поймал муху, 
<br/><br/>
<a href="http://farm9.staticflickr.com/8008/7159926415_ebf08b4e1f_b.jpg"><img src="http://farm9.staticflickr.com/8008/7159926415_ebf08b4e1f.jpg" width="500" height="216" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

потом  с той же целью  птичку, кошку, собаку, корову и ...
<br/><br/>
<a href="http://farm8.staticflickr.com/7077/7342193120_405c259f19_b.jpg"><img src="http://farm8.staticflickr.com/7077/7342193120_405c259f19.jpg" width="500" height="229" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

лошадь.
<br/>
<a href="http://farm8.staticflickr.com/7084/7342297886_c0777ddfc8_b.jpg"><img src="http://farm8.staticflickr.com/7084/7342297886_c0777ddfc8.jpg" width="500" height="230" alt="Untitled"></a>
<br/><br/><br/><br/><br/>


Венчает историю мораль в английcком стиле - "Никогда не глотайте лошадь".
<br/><br/>
<a href="http://farm8.staticflickr.com/7234/7337079336_32d92937d8_b.jpg"><img src="http://farm8.staticflickr.com/7234/7337079336_32d92937d8.jpg" width="500" height="367" alt="Untitled"></a>
<br/><br/><br/><br/><br/>





<h2><b>Главные действующие лица:</b></h2>

<h4><b>1) Старая леди</b></h4>

Before
<br/><br/>
<a href="http://farm9.staticflickr.com/8008/7337088032_ce77d5d6ed_b.jpg"><img src="http://farm9.staticflickr.com/8008/7337088032_ce77d5d6ed.jpg" width="500" height="395" alt="Untitled"></a>
<br/><br/><br/><br/><br/>



After
<br/><br/>
<a href="http://farm8.staticflickr.com/7133/7414248084_490017d7bd_b.jpg"><img src="http://farm8.staticflickr.com/7133/7414248084_490017d7bd.jpg" width="500" height="391" alt="Untitled"></a>
<br/><br/><br/><br/><br/><br/>









<h4><b>2) Mуха</b></h4>

Каждому претенденту на глотание посвящена  целая страница. 
<br/><br/>
<a href="http://farm8.staticflickr.com/7074/7344955032_4ac36f9d88_b.jpg"><img src="http://farm8.staticflickr.com/7074/7344955032_4ac36f9d88.jpg" width="500" height="219" alt="DSC_0036"></a>
<br/><br/><br/><br/><br/>

Несекомые - от ползающих до летающих.
<br/><br/>
<a href="http://farm8.staticflickr.com/7221/7337083012_7ac357e1cb_b.jpg"><img src="http://farm8.staticflickr.com/7221/7337083012_7ac357e1cb.jpg" width="500" height="412" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

Заголовок журнала "Таймс" - <i>"Старая леди  проглотила муху"</i>.
<br/><br/>
<a href="http://farm6.staticflickr.com/5152/7409099582_a119453dee_b.jpg"><img src="http://farm6.staticflickr.com/5152/7409099582_a119453dee.jpg" width="500" height="325" alt="Untitled"></a>
<br/><br/><br/><br/><br/><br/>


<h4><b>2) Паук</b></h4>
Кулинар-новатор и ингредиенты его коронного блюда.
<br/><br/>
<a href="http://farm8.staticflickr.com/7217/7337081640_7243915f26_b.jpg"><img src="http://farm8.staticflickr.com/7217/7337081640_7243915f26.jpg" width="500" height="409" alt="Untitled"></a>
<br/><br/><br/><br/><br/>
Суп паука<br/>
Рецепт на ужин:<br/>
 1 муха, 1 шершень, 2 осы и 1/2 гусеницы. Жарить  в  соке червяка до полной готовности ...

<br/><br/> 
<a href="http://farm9.staticflickr.com/8006/7409049004_39ac9bb943_b.jpg"><img src="http://farm9.staticflickr.com/8006/7409049004_39ac9bb943.jpg" width="500" height="318" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

<h4><b>3) Птица</b></h4>
Ветвь  древа эволюции  (по Симмсу Табаку)
<br/><br/>
<a href="http://farm9.staticflickr.com/8156/7337080488_8f1757598a_b.jpg"><img src="http://farm9.staticflickr.com/8156/7337080488_8f1757598a.jpg" width="500" height="405" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

Срочное письмо. Адресат - старая леди.
<br/><br/>
<a href="http://farm6.staticflickr.com/5151/7409100340_53674499f6_b.jpg"><img src="http://farm6.staticflickr.com/5151/7409100340_53674499f6.jpg" width="500" height="314" alt="Untitled"></a>
<br/><br/><br/><br/><br/><br/>


<h4><b>4) Кот</b></h4>
Блеск и нищита котов
<br/><br/>
<a href="http://farm9.staticflickr.com/8166/7337078610_a151769b6c_b.jpg"><img src="http://farm9.staticflickr.com/8166/7337078610_a151769b6c.jpg" width="500" height="399" alt="Untitled"></a>
<br/><br/><br/><br/><br/>




Заголовки газеты "Нью-Йорк пост":<br/>
<i>
"Совершено нападение на кота", "Прeступник опознан","Кот в шляпе потерял шляпу" <sup><b>[ 1 ]</b></sup>,"У кота осталось 8 жизней" <sup><b>[ 2 ]</b></sup></i>.
<br/><br/>

<a href="http://farm8.staticflickr.com/7279/7409048044_bb25f9dcff_b.jpg"><img src="http://farm8.staticflickr.com/7279/7409048044_bb25f9dcff.jpg" width="500" height="456" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

<sup><b>[ 1 ]</b></sup> - <i>"Кот в шляпе потерял шляпу" (Cat in  Hat loses hat)</i>
<br/>   
Подразумевается знаменитый персонаж Доктора Сьюза.
<br/><br/>
<img alt="Seuss-cat-hat.gif" src="//upload.wikimedia.org/wikipedia/en/thumb/b/b5/Seuss-cat-hat.gif/200px-Seuss-cat-hat.gif" width="200" height="289">
<br/><br/><br/><br/>


<sup><b>[ 2 ]</b></sup> - <i>"У кота осталось 8 жизней" (Cat has 8 lives left)</i>
<br/>
<i>"8 lives left"</i> - чудом избежать опасности. Выражение произошло от идиомы <i>"A cat has 9 lives"</i> (перевод - у кошки 9 жизней, русский аналог - живуч, как кошка).
<br/><br/>
<img src="http://farm9.staticflickr.com/8447/7746944936_554d5fbfa6.jpg" width="309" height="264" alt="Untitled">
<br/>
<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(Отойди. У меня 9 жизней)</i>
<br/><br/><br/><br/><br/><br/>


<h4><b>5) Собака</b></h4>
Диалекты собачьего лая,  собранные  на территории  штата Нью-Йорк. 
<br/><br/>
<a href="http://farm8.staticflickr.com/7091/7337084418_35ded975b0_b.jpg"><img src="http://farm8.staticflickr.com/7091/7337084418_35ded975b0.jpg" width="500" height="401" alt="Untitled"></a>
<br/><br/><br/><br/><br/>



Загoловок таблоида супермаркетов "National Enquirer" - <i>"Леди проглотила собаку"</i>.
<br/><br/>
<a href="http://farm6.staticflickr.com/5456/7409050644_080775cd13_b.jpg"><img src="http://farm6.staticflickr.com/5456/7409050644_080775cd13.jpg" width="500" height="366" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

Слева -  популярная новелла Эрикa Найта "Лесси возвращается домой",
справа - объявление  <i>"Пропала собака. Cмешанный лабрадор. Размер средний. Очень дружелюбна. Последний раз  видели со старой леди."</i>.
<br/><br/>
<a href="http://farm8.staticflickr.com/7138/7411479768_05cb5e0ba0_b.jpg"><img src="http://farm8.staticflickr.com/7138/7411479768_05cb5e0ba0.jpg" width="500" height="402" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

 Cлева - календарь с "собачьими днями" <sup><b>[ 3 ]</b></sup>, в центре - предмет личной гигиены - средство против блох.
<br/><br/>
<a href="http://farm8.staticflickr.com/7258/7409140722_efd1d3ce0b_b.jpg"><img src="http://farm8.staticflickr.com/7258/7409140722_efd1d3ce0b.jpg" width="500" height="437" alt="Untitled"></a>
<br/><br/>
<sup><b>[ 3 ]</b></sup> <i> - <b><a href="http://club-sovetov.ru/dom-i-semya/dom-i-kottedzh/pochemu-tak-zharko-prishli-sobachi-dni-leta.htm">Все говорят</a></b> о «Собачей жаре», но мало кто знает, что означает это выражение. Некоторые говорят, что это означает жаркие, знойные дни, которые не подходят для собак. Другие говорят, это погода, при которой собаки сходят с ума. Но на самом деле «Собачьи дни» определяются как период с 3 июля по 11 августа, когда звезда Сириус (созвездие Большого Пса), поднимается вместе (или почти вместе) с Солнцем.
<br/>
В результате, некоторые люди считают, что комбинация из самых ярких светил – днем Солнце, а ночью самая яркая звезда ночного времени Сириус – является ответственной за сильную жару, которая часто возникает в середине лета. Другим эффектом, который давало это сочетание, по мнению древних, была засуха, эпидемии и безумие.</i>
<br/><br/><br/><br/><br/><br/>




<h4><b>6) Корова</b></h4>
Корова и ее продукция.
<br/><br/>
<a href="http://farm8.staticflickr.com/7216/7337085728_69bc9c4c7e_b.jpg"><img src="http://farm8.staticflickr.com/7216/7337085728_69bc9c4c7e.jpg" width="500" height="411" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

Заголовок местной газеты  "Вудстoк <sup><b>[ 4 ]</b></sup> Таймс"  - <i>"Съедена целая корова".</i>

<br/><br/>
<a href="http://farm9.staticflickr.com/8157/7409101346_e713f25159_b.jpg"><img src="http://farm9.staticflickr.com/8157/7409101346_e713f25159.jpg" width="500" height="420" alt="Untitled"></a>
<br/><br/>
<sup><b>[ 4 ]</b></sup> - Городoк Вудстoк в Катскильских горах знаменит тем, что дал название  Вудстокскому фестивалю.
<br/><br/><br/><br/><br/>

<h4><b>7) Лошадь</b></h4>

Лошадь с фермы "Тайная долина"
<br/><br/>
<a href="http://farm8.staticflickr.com/7233/7337087020_edfdea6861_b.jpg"><img src="http://farm8.staticflickr.com/7233/7337087020_edfdea6861.jpg" width="500" height="410" alt="Untitled"></a>
<br/><br/><br/><br/><br/>


Заголовки газеты "Уолл-стрит джорнел":<br/>
<i>"Леди съела на обед лошадь","Рынок акций отреагировал!"</i>
<br/><br/>
<a href="http://farm8.staticflickr.com/7279/7409102366_ff3065ee63_b.jpg"><img src="http://farm8.staticflickr.com/7279/7409102366_ff3065ee63.jpg" width="500" height="355" alt="Untitled"></a>
<br/><br/><br/><br/><br/>



Название  "Тайная долина" (Hidden Valley) выглядит как пародия на  американскую фирму  "Био-долина"  (Organic Valley) <sup><b>[ 5 ]</b></sup>.
<br/><br/>
<a href="http://www.flickr.com/photos/72635554@N08/7409172780/" title="Untitled by elefant5959, on Flickr"><img src="http://farm6.staticflickr.com/5343/7409172780_f43b41d9db.jpg" width="500" height="442" alt="Untitled"></a>
<br/><br/><br/><br/><br/>

<sup><b>[ 5 ]</b></sup> - Лейбл "Organic Valley" можно встретить в любом супермаркете США.
<br/><br/>
<a href="http://farm6.staticflickr.com/5118/7414641568_2bb2a9aeb8_b.jpg"><img src="http://farm6.staticflickr.com/5118/7414641568_2bb2a9aeb8.jpg" width="320" height="230" alt="Untitled"></a>
<br/>