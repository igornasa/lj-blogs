﻿"Carrying On", 2003 Janet Zweig, Edward del Rosario <br/>
Мозаика: <i>"Miotto Mosaic Art Studios", NY</i><br/>
Станция: <i>"Prince Street"</i> линии <i>N, R, W</i> (Манхэттен) 
<br/><br/>
Круг интересов  <a href="http://www.janetzweig.com"><b>Джанет Цвейг</b></a> охватывает   скульптуру, инсталляцию и <a href="http://ru.wikipedia.org/wiki/%D0%9A%D0%BD%D0%B8%D0%B3%D0%B0_%D1%85%D1%83%D0%B4%D0%BE%D0%B6%D0%BD%D0%B8%D0%BA%D0%B0"><b>книгу художника</b></a>  (book-art).
<br/><br/>
<img src="http://farm7.static.flickr.com/6109/6346206075_3f26bea0c4_m.jpg" width="133" height="154" alt="">
<br/>
<br/><br/>
<br/>



Oсновная тема  <a href="http://edwarddelrosario.com/index.htm"><b>живописи</b></a> Эдварда Дель Росарио - психологически-провокационные ситуации. <br/>
Он входит в число авторов популярных вагонных <a href="http://elefant59.livejournal.com/5507.html#Игра"><b>постеров</b></a> подземки.
<br/>
<br/>
<a href="http://www.flickr.com/photos/68171840@N06/6346206053/"><img src="http://farm7.static.flickr.com/6108/6346206053_c419bc0403_m.jpg" width="133" height="128"></a>


 
<br/>
<br/>


Одной из самых оригинальных станций  нью-йоркского  метро считается "Prince Street", которую можно назвать  энциклопедией уличной жизни Манхэттена. На стенах 2-х платформ станции  инсталлированы мозаичные силуэты людей в движении.
<br/>
<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4918073308/" title="b2 by elefant59, on Flickr"><img src="http://farm5.staticflickr.com/4093/4918073308_f374ce9da5.jpg" width="500" height="375" alt="b2"></a>
<br/><br/><br/><br/>




Персонажи  мозаик  были взяты с фотографий нью-йоркцев, идущих по улицам Манхэттена. Из более чем 2000 изображений были выбраны 194 как наиболее визуально читабельныe.
<br/> 
<br/>
<a href="http://www.flickr.com/photos/tarkastad/4270989658/" title="Prince Street by Jean (tarkastad), on Flickr"><img src="http://farm5.static.flickr.com/4006/4270989658_15ed5bf56c.jpg" width="500" height="330" alt="Prince Street"></a>
<br/><br/><br/><br/>

Было бы очень любопытно посмотреть на оригинальные снимки. К сожалению, я не смогла их найти,
но благодаря посту известного жж-блогера <lj user="samsebeskazal">  можно <s>пройти по следам бременских музыкантов</s> получить представление об <a href="http://samsebeskazal.livejournal.com/260286.html"><b>обычных жителях Нью-Йорка</b></a>.
<br/> <br/>
<a href="http://www.flickr.com/photos/50463938@N04/4918073192/" title="b1 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4094/4918073192_6bc5537a85.jpg" width="500" height="336" alt="b1" /></a>
<br/><br/><br/><br/>
<a href="http://www.flickr.com/photos/50463938@N04/4918852424/" title="aIMG_3321 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4143/4918852424_0acde1f2d2.jpg" width="500" height="375" alt="aIMG_3321" /></a>
<br/><br/><br/><br/> 



Название  инсталляции <a href="http://www.janetzweig.com/public/06.html"><b><i>"Carrying On"</b></a></i> - игра смыслов: 
<br/><br/>

<i>Carrying something - переносить что-то</i><br/>
Люди на улицах Нью-Йорка почти всегда что-то несут в руках.
<br/><br/> 

<i>Carry on with their lives - продолжать жить дальше</i><br/>
Работа над мозаиками на станции была  начата  перед роковой датой 11 сентября  2001 года и закончилась лишь три года спустя.<br/><br/>

<i>Carry on (вести себя несдерженно, беситься, рвать, метать и т.д.)</i><br/>
Динамичные жители столицы мира действительно <i>"carry on"</i>. 
<br/> <br/> 









 




<a href="http://www.flickr.com/photos/50463938@N04/4918137604/" title="co9 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4094/4918137604_7965b0fe41.jpg" width="500" height="337" alt="co9" /></a>

<br/><br/><br/>


<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4917419543/" title="co3 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4097/4917419543_52bd5e4d93.jpg" width="500" height="336" alt="co3" /></a>
<br/><br/><br/>


<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4918019388/" title="co6 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4101/4918019388_def4cb9fba.jpg" width="500" height="338" alt="co6" /></a>
<br/><br/><br/>

<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4918017602/" title="co4 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4115/4918017602_1e942ec430.jpg" width="500" height="341" alt="co4" /></a>
<br/><br/><br/>

<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4918018048/" title="co5 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4093/4918018048_0935a5c7ba.jpg" width="500" height="337" alt="co5" /></a>
<br/><br/><br/>

<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4918252855/" title="co12 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4094/4918252855_d0849e83fd.jpg" width="500" height="344" alt="co12" /></a>
<br/><br/><br/>

<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4917418815/" title="co1 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4136/4917418815_082e898783.jpg" width="500" height="334" alt="co1" /></a>
<br/><br/><br/>

<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4918018682/" title="co2 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4079/4918018682_d90f56763a.jpg" width="500" height="337" alt="co2" /></a>
<br/><br/><br/><br/><br/>

Мозаики выполнены из стали, шиферa и терраззо 
<br/><br/>

<iframe width="500" height="281" src="http://www.youtube.com/embed/Vresm66w8Kg?wmode=opaque" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

<br/><br/><br/><br/><br/>

Видео с сайта Джанет Цвейг 
<br/><br/>

<iframe width="500" height="375" src="http://www.youtube.com/embed/Y1XRRVBtqQs?wmode=opaque" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
