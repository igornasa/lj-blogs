﻿<h4><b>8) "Моя малышка с Кони-Айленда",  2004 <a href="http://www.robertwilson.com">Роберт Уилсон</a></b></h4>

Станция метро: "Coney Island - Stillwell Avenue"  (Бруклин) линии D, F, N, Q
<br/>
<br/>
Многие изображения на стеклянной стене (~ 110м х 5м),  связанные с историей Кони-Айленда,  взяты с архивных открыток, фотографий, постеров.
На название инсталляции  претендуют две совершенно разные песни "Coney Island Baby" ("Малышка с Кони-Айленда") <a href="http://www.youtube.com/watch?v=Xp-5V3PJ90E">Лу Рида</a> и <a href="
http://www.youtube.com/watch?v=U1UwvzFlwOE">Тома Уэйтса</a>.
<br/>
<br/>


Роберт Уилсон  – театральный и оперный режиссёр, скульптор, фото-художник, драматург и сценограф, один из крупнейших представителей театрального авангарда, внес элемент театральности и в этот проект.
<br/>
<a href="http://www.flickr.com/photos/38300656@N05/3549697377/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm4.static.flickr.com/3380/3549697377_f310503604.jpg" width="500" height="375" alt="Coney Island" /></a>
<br/><br/><br/>
 
Ночью при искусственом освещении лица в окнах выглядят несколько мистически.<br/>
<a href="http://www.flickr.com/photos/38300656@N05/3550507278/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm4.static.flickr.com/3579/3550507278_31d47254eb.jpg" width="500" height="375" alt="Coney Island" /></a>
<br/><br/><br/><br/>

<a href="http://www.flickr.com/photos/ifong/3408473546/" title="Detail, &quot;My Coney Island Baby,&quot; Coney Island/Stillwell Ave Subway Station by i mei, on Flickr"><img src="http://farm4.static.flickr.com/3622/3408473546_fc60dbaf16.jpg" width="500" height="336" alt="Detail, &quot;My Coney Island Baby,&quot; Coney Island/Stillwell Ave Subway Station" /></a>
<br/><br/><br/><br/>

<a href="http://www.flickr.com/photos/38300656@N05/3550506184/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm4.static.flickr.com/3599/3550506184_73b7b7b9c8.jpg" width="500" height="375" alt="Coney Island" /></a>
<br/><br/><br/><br/>

<a href="http://www.flickr.com/photos/38300656@N05/3549695729/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm4.static.flickr.com/3383/3549695729_aa82c8cff8.jpg" width="500" height="375" alt="Coney Island" /></a>
<br/><br/><br/><br/>

<a href="http://www.flickr.com/photos/38300656@N05/3549699685/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm4.static.flickr.com/3324/3549699685_cee1a37dec.jpg" width="500" height="375" alt="Coney Island" /></a>
<br/><br/><br/>

"Человек-черепаха"<br/>
<a href="http://www.flickr.com/photos/38300656@N05/3550503748/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm4.static.flickr.com/3567/3550503748_b33d08c081.jpg" width="500" height="375" alt="Coney Island" /></a>
<br/><br/><br/>

<s>Кислотный</s> "Токсичный мститель"  - герой американского культового фильма. <br/>
<a href="http://www.flickr.com/photos/51644460@N06/4996297224/" title="k7 by ign2009, on Flickr"><img src="http://farm5.static.flickr.com/4103/4996297224_85c6d66037.jpg" width="500" height="369" alt="k7" /></a>
<br/><br/><br/><br/>

<a href="http://www.flickr.com/photos/38300656@N05/3549689535/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm4.static.flickr.com/3632/3549689535_dc615ac1ef.jpg" width="500" height="375" alt="Coney Island" /></a>
<br/><br/><br/><br/>

<a href="http://www.flickr.com/photos/38300656@N05/3550495390/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm3.static.flickr.com/2443/3550495390_ffd5ae2280.jpg" width="500" height="375" alt="Coney Island" /></a>
<br/><br/><br/>

Огромный хот-дог - ассоциация с расположенной на Кони Айленде закусочной "Nathan’s Hot Dogs" (1916), где ежегодo проводятся всемирные чемпионаты по поеданию хот-догов.<br/> 
<a href="http://www.flickr.com/photos/38300656@N05/3549687143/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm4.static.flickr.com/3571/3549687143_c49057e0be.jpg" height="375" alt="Coney Island" /></a>
<br/><br/><br/>

<s>"Дворец советов"</s> "Башня-глобус Кони Айленда"  Самуэл Фрайд (1906)<br/>
<a href="http://www.flickr.com/photos/38300656@N05/3550496588/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm4.static.flickr.com/3347/3550496588_d3d4d1fbfb.jpg" width="500" height="375" alt="Coney Island" /></a>
<br/><br/><br/>

"Gravesend Race Track" (1886). Иппподром был закрыт в 1913 году.
<br/>
<a href="http://www.flickr.com/photos/ifong/3408474492/" title="Detail, Coney Island/Stillwell Ave Subway Station (Artist: Robert Wilson) by i mei, on Flickr"><img src="http://farm4.static.flickr.com/3302/3408474492_c3ff0554c9.jpg" width="500" height="341" alt="Detail, Coney Island/Stillwell Ave Subway Station (Artist: Robert Wilson)" /></a>
<br/><br/><br/>

Колесо обозрения "Wonder Wheel" (1918) занесено в  национальный реестр исторических мест.<br/>
<a href="http://www.flickr.com/photos/51644460@N06/4995689705/" title="k2 by ign2009, on Flickr"><img src="http://farm5.static.flickr.com/4154/4995689705_1d09192034.jpg" width="426" height="500" alt="k2" /></a>
<br/><br/><br/>

Горки "Cyclone"  (1927) занесены в  национальный реестр исторических мест.<br/>
<a href="http://www.flickr.com/photos/51644460@N06/4995690021/" title="k3 by ign2009, on Flickr"><img src="http://farm5.static.flickr.com/4145/4995690021_5f9c50d64e.jpg" width="500" height="328" alt="k3" /></a>
<br/><br/><br/>

Горки "Cyclone"  на Кони Айленде (1927)<br/>
<a href="http://www.flickr.com/photos/51644460@N06/4996296070/" title="k1 by ign2009, on Flickr"><img src="http://farm5.static.flickr.com/4144/4996296070_43e65e7d5a.jpg" width="500" height="384" alt="k1" /></a>
<br/><br/><br/>

Дети на пляже - архивная фотография.<br/>
<a href="http://www.flickr.com/photos/51644460@N06/4995690163/" title="k4 by ign2009, on Flickr"><img src="http://farm5.static.flickr.com/4111/4995690163_1d7ceaa76e.jpg" width="500" height="375" alt="k4" /></a>
<br/><br/><br/>

Сцена на бордволке - архивная открытка<br/>
<a href="http://www.flickr.com/photos/38300656@N05/3550497098/" title="Coney Island by nycsubwayart, on Flickr"><img src="http://farm3.static.flickr.com/2449/3550497098_81e8938a80.jpg" width="500" height="375" alt="Coney Island" /></a>

<br/>
