# **LJ Blogs**
 **[multiblogs-001-site1.ftempurl.com](http://multiblogs-001-site1.ftempurl.com)** 
 
 * About project
 * Website terms and icons
 * Screenshots
 ________________________

# **About project**

________________________


Multi-blogging website, where each user has a blog, friends list and profile.   
Posts and blogs ratings for top lines are calculated every 12 hours as a background job.   
Statistics reports are available at public, admin, and blog levels.



### Used software: ###

**Languages**

* C#, JavaScript

**Database**

* MS SQL Server 2014

**ORM**

* Entity Framework 6 (Code First)

**Background processing**

* [Hangfire](http://hangfire.io/)

**Internet**

* ASP.NET MVC 4, JavaScript, jQuery, Templates, Ajax, CSS

**Responsive web design**

* front-end web framework - Bootstrap 3

**Plugins**

* [tinyMCE](https://www.tinymce.com), advanced HTML editor
* [imgAreaSelect](http://odyniec.net/projects/imgareaselect), an image cropping tool
* [ddSlick](http://designwithpc.com/Plugins/ddSlick), a custom drop down with images
* [highCharts](http://www.highcharts.com/demo), a charting library
* [highMaps](http://www.highcharts.com/maps/demo), a map library

**API**

* [freeGeoIP](https://freegeoip.net/), public HTTP API to search the geolocation of IP addresses



___________________________________________________

# **Website terms and icons** 

___________________________________________________________________________

![B29.png](https://bitbucket.org/repo/dabgxnq/images/1606902779-B29.png)

___________________________________________________________________________


# **Screenshots** 
___________________________________________________

**Demo** 

No  passwords needed. Login/re-login as a website user at  **["help"](http://multiblogs-001-site1.ftempurl.com/Home/Help)** 


![B45.jpg](https://bitbucket.org/repo/dabgxnq/images/140878280-B45.jpg)

________________________



## **Home**

*Top blogs and top posts lines are calculated every 12 hours*
________________________


![B238.jpg](https://bitbucket.org/repo/dabgxnq/images/2565008804-B238.jpg)

________________________


## **Top posts ** 

![K5.jpg](https://bitbucket.org/repo/dabgxnq/images/2860603497-K5.jpg)
___________________________________________________

![B45.png](https://bitbucket.org/repo/dabgxnq/images/2555759429-B45.png)



___________________________________________________


## **Posts  ratings  (public report)**

*Post  ratings are calculated  as a background job with help of [Hangfire](http://hangfire.io).*

**Used plugins:**  
*[highCharts](http://www.highcharts.com/demo) - a charting library*  

___________________________________________________

![B34.png](https://bitbucket.org/repo/dabgxnq/images/1204854662-B34.png)

___________________________________________________


## **Top bloggers **

![K4.jpg](https://bitbucket.org/repo/dabgxnq/images/2592324532-K4.jpg)

___________________________________________________________________________


![d6.jpg](https://bitbucket.org/repo/dabgxnq/images/2209197901-d6.jpg)

___________________________________________________________________________

## **User profile**
*
Public profile contains personal information, userpics, used by blog tags and categories, relationships*

**Relationships:**   
*� friend - user added  another user to his/her friends list  
� friend of - user added by another user to his/her friends list  
� mutual friend - user is both a friend and a friend of*

*A user can be added or deleted from friend list ( +/- button next to main userpic)
*
 _______________________________________

![B.png](https://bitbucket.org/repo/dabgxnq/images/1834417675-B.png)

_______________________________________



## **Edit profile**

**Used plugins:**  
*[imgAreaSelect](http://odyniec.net/projects/imgareaselect) - an image cropping tool*
_______________________________________


![![b6.png](https://bitbucket.org/repo/dabgxnq/images/3359488344-b6.png)](https://bitbucket.org/repo/dabgxnq/images/811781007-B54.png)

___________________________________________________

## **Edit post**

**Used plugins:**  
*� [tinyMCE](https://www.tinymce.com) - advanced HTML editor  
� [ddSlick](http://designwithpc.com/Plugins/ddSlick) - a custom drop down with images* 

_______________________________________



![B1.png](https://bitbucket.org/repo/dabgxnq/images/380854932-B1.png)

________________________

## **User posts**


  ![K2.jpg](https://bitbucket.org/repo/dabgxnq/images/1463936568-K2.jpg)

___________________________________________________

![B55.png](https://bitbucket.org/repo/dabgxnq/images/2098641219-B55.png)

_______________________________________

## **Comments**



**Used plugins:**  
*� [tinyMCE](https://www.tinymce.com) - advanced HTML editor  
� [ddSlick](http://designwithpc.com/Plugins/ddSlick) - a custom drop down with images* 


_______________________________________

![B92.png](https://bitbucket.org/repo/dabgxnq/images/3559512492-B92.png)
_______________________________________

## **Comments chart (blog report)**

**Used plugins:**  
 *[highCharts](http://www.highcharts.com/demo) - a charting library*  


___________________________________________________________________________

![report1[1].jpg](https://bitbucket.org/repo/dabgxnq/images/1861748253-report1%5B1%5D.jpg)

___________________________________________________________________________

## **Friends list chart (blog report)**


**Used plugins:**  
 *[highCharts](http://www.highcharts.com/demo) - a charting library*  



___________________________________________________________________________

![B4.jpg](https://bitbucket.org/repo/dabgxnq/images/3909682435-B4.jpg)

___________________________________________________________________________





## **Manage friends list**

![K1.jpg](https://bitbucket.org/repo/dabgxnq/images/2238213844-K1.jpg)

___________________________________________________________________________

![d5.jpg](https://bitbucket.org/repo/dabgxnq/images/2557651877-d5.jpg)

___________________________________________________________________________


## **Manage friends groups**

*Multiple friend groups can be created from friends list*

___________________________________________________________________________


![d33.jpg](https://bitbucket.org/repo/dabgxnq/images/1494560416-d33.jpg)

___________________________________________________________________________

##  **Online visitors map (public report)**

**Used plugins:**  
 [highMaps](http://www.highcharts.com/maps/demo) - a map library  

**Used API:**  
*[freeGeoIP](https://freegeoip.net/) - public HTTP API to search the geolocation of IP addresses*
___________________________________________________________________________

![M2.jpg](https://bitbucket.org/repo/dabgxnq/images/3740216119-M2.jpg)

___________________________________________________________________________


##  **History visitors map (public report)**


**Used plugins:**  
 [highMaps](http://www.highcharts.com/maps/demo) - a map library  

**Used API:**  
*[freeGeoIP](https://freegeoip.net/) - public HTTP API to search the geolocation of IP addresses*


___________________________________________________________________________


![report3[1].jpg](https://bitbucket.org/repo/dabgxnq/images/4236017340-report3%5B1%5D.jpg)


___________________________________________________________________________

##  **History visitors charts  (public report)**

**Used plugins:**  
*[highCharts](http://www.highcharts.com/demo) - a charting library*  

___________________________________________________________________________

![B47.png](https://bitbucket.org/repo/dabgxnq/images/2439717835-B47.png)

___________________________________________________________________________


##  **Visitor reports (private reports) **

*Reports for visited, bounce, and exit pages  are available for admin and blogs.*




**Visited pages (blog report)**
___________________________________________________

![M1.jpg](https://bitbucket.org/repo/dabgxnq/images/3378482000-M1.jpg)

________________________

**Exit pages (admin report)**
___________________________________________________

![M2.jpg](https://bitbucket.org/repo/dabgxnq/images/714396238-M2.jpg)

________________________

## **Manage background jobs (admin)**

**[Hangfire](http://hangfire.io)** used to perform background processing.  
 

___________________________________________________


![H2.jpg](https://bitbucket.org/repo/dabgxnq/images/1173722195-H2.jpg)

___________________________________________________

**[Job types:](https://www.hangfire.io/)** 

**Fire-and-forget job** (fire only once and almost immediately after creation):  
*�  page hits saved on every request* 

**Recurring job** (fire many times on the specified CRON schedule):   
*�  blog and posts ratings calculated every 12 hours    
�  visitors history calculated every 24 hours with  3 days offset*


___________________________________________________


![H1.jpg](https://bitbucket.org/repo/dabgxnq/images/1321133339-H1.jpg)

________________________

## **Manage post filters (admin)**

**Used plugins:**  
 *jQuery Grid*

___________________________________________________

![B61.jpg](https://bitbucket.org/repo/dabgxnq/images/2540426803-B61.jpg)

________________________

# **Content** 

___________________________________________________________________________

![B98.png](https://bitbucket.org/repo/dabgxnq/images/2385427224-B98.png)

________________________

